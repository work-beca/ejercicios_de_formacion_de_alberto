��          �      ,      �  3   �     �     �     �  
   �  	   �  !   �       !   '     I     g  !   �     �  +   �     �  '   �  �  !  .   �     �     �     �       	   6  "   @     c  ,   o  )   �     �  "   �     	  "   )     L      ^     
                 	                                                        Congratulations, you have entered the raffle. Luck! Email address Enter email Enter last name Enter name Last name Plugin to participate in a raffle Register Sorry, your nonce did not verify. The check field is incomplete The email field is incomplete The last name field is incomplete The name field is incomplete The user already participates in the raffle Username Yes, I want to paticipate in the raffle Project-Id-Version: yith-am-raffle 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/yith-am-raffle
PO-Revision-Date: 2020-11-04 11:09+0000
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Domain: yith-am-raffle
Plural-Forms: nplurals=2; plural=(n != 1);
 Felicidades, has entrado en la rifa. ¡Suerte! Dirección de correo Introduce tu correo Introduce tus apellidos Introduce tu nombre de usuario Apellidos Plugin para participar en una rifa Suscribirse Lo siento, tu "nonce" no ha sido verificado. El campo de verificación esta incompleto El campo correo esta incompleto El campo apellidos esta incompleto El campo nombre esta incompleto El usuario ya participa en la rifa Nombre de usuario Si, quiero participar en la rifa 