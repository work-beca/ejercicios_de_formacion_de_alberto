<?php
/**
 * YITH raffle users form
 *
 * @package WordPress
 */
?>

<div class = 'content-participants' >

	<h3>Últimos 5 participantes</h3>
	<table class="table">
	<thead>
		<tr>
		<th scope="col">Name</th>
		<th scope="col">Last Name</th>
		<th scope="col">email</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ( $args as $participant ) { ?>
			<tr>
			<td><?php echo esc_html( $participant->name ); ?></td>
			<td><?php echo esc_html( $participant->surnames ); ?></td>
			<td><?php echo esc_html( $participant->email ); ?></td>
			</tr>
		<?php } ?>
	</tbody>
	</table>

</div>
