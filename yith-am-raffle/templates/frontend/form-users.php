<?php
/**
 * YITH raffle users form
 *
 * @package WordPress
 */

if ( is_user_logged_in() ) { ?>
	<?php if ( isset( $_GET['errormsg'] ) ) { ?>
		<div class="div-user-exist">
			<p class="text-user-exist"><?php echo esc_html( sanitize_text_field( wp_unslash( $_GET['errormsg'] ) ) ); ?> </p>
		</div>
	<?php } ?>
	<?php if ( isset( $_GET['successmsg'] ) ) { ?>
		<div class="div-user-no-exist">
			<p class="text-user-no-exist"><?php echo esc_html( sanitize_text_field( wp_unslash( $_GET['successmsg'] ) ) ); ?> </p>
		</div>
	<?php } ?>
	<form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
		<div class = "form-content">
			<div class="form-check">
				<input type="checkbox" class="form-check-input" id="Check" name="Check" required>
				<label class="form-check-label" for="Check"><?php echo esc_html_e( 'Yes, I want to paticipate in the raffle', 'yith-am-raffle' ); ?></label>
			</div>
			<button type="submit" class="btn btn-primary"><?php echo esc_html_e( 'Register', 'yith-am-raffle' ); ?></button>
			<input type="hidden" name="action" value="raffleform">
			<?php wp_nonce_field( 'action_raffle_form', 'raffle_form' ); ?>
		</div>
	</form>
<?php } else { ?>
	<?php if ( isset( $_GET['errormsg'] ) ) { ?>
		<div class="div-user-exist">
			<p class="text-user-exist"><?php echo esc_html( sanitize_text_field( wp_unslash( $_GET['errormsg'] ) ) ); ?> </p>
		</div>
	<?php } ?>
	<?php if ( isset( $_GET['successmsg'] ) ) { ?>
		<div class="div-user-no-exist">
			<p class="text-user-no-exist"><?php echo esc_html( sanitize_text_field( wp_unslash( $_GET['successmsg'] ) ) ); ?> </p>
		</div>
	<?php } ?>
	<form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
		<div class = "form-content">
			<div class="form-group">
				<label for="InputUser"> <?php echo esc_html_e( 'Username', 'yith-am-raffle' ); ?></label>
				<input type="text" class="form-control" id="InputUser" name="InputUser" placeholder="<?php echo esc_html_e( 'Enter name', 'yith-am-raffle' ); ?>" required>
			</div>
			<div class="form-group">
				<label for="InputLastname"><?php echo esc_html_e( 'Last name', 'yith-am-raffle' ); ?></label>
				<input type="text" class="form-control" id="InputLastname" name="InputLastname" placeholder="<?php echo esc_html_e( 'Enter last name', 'yith-am-raffle' ); ?>" required>
			</div>
			<div class="form-group">
				<label for="InputEmail"><?php echo esc_html_e( 'Email address', 'yith-am-raffle' ); ?></label>
				<input type="email" class="form-control" id="InputEmail" name="InputEmail" aria-describedby="emailHelp" placeholder="<?php echo esc_html_e( 'Enter email', 'yith-am-raffle' ); ?>" required>
			</div>
			<div class="form-check">
				<input type="checkbox" class="form-check-input" id="Check" name="Check" required>
				<label class="form-check-label" for="Check"><?php echo esc_html_e( 'Yes, I want to paticipate in the raffle', 'yith-am-raffle' ); ?></label>
			</div>
			<button type="submit" class="btn btn-primary"><?php echo esc_html_e( 'Register', 'yith-am-raffle' ); ?></button>
			<input type="hidden" name="action" value="raffleform">
			<?php wp_nonce_field( 'action_raffle_form', 'raffle_form' ); ?>
		</div>
	</form>
	<?php }
?>
