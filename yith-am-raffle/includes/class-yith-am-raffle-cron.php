<?php
/**
 * File class raffle cron
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_RAFFLE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_RAFFLE_Cron' ) ) {
	/**
	 * YITH_AM_RAFFLE_Admin
	 */
	class YITH_AM_RAFFLE_Cron {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_RAFFLE_Cron
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_RAFFLE_Cron
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_filter( 'cron_schedules', array( $this, 'my_cron_schedules' ) );
			if ( ! wp_next_scheduled( 'task_send_email_raffle_participants' ) ) {
				wp_schedule_event( time(), '5min', 'task_send_email_raffle_participants' );
			}
			// add_action( 'task_send_email_raffle_participants', array( $this, 'task_send_email_raffle_participants_cb' ) );
		}
		/**
		 * My_cron_schedules
		 *
		 * @param  mixed $schedules comment.
		 * @return $schedules
		 */
		public static function my_cron_schedules( $schedules ) {
			if ( ! isset( $schedules['5min'] ) ) {
				$schedules['5min'] = array(
					'interval' => 5 * 60,
					'display'  => __( 'Once every five minutes' ),
				);
			}

			return $schedules;
		}
		/**
		 * Task_send_email_raffle_participants
		 *
		 * @return void
		 */
		public static function task_send_email_raffle_participants_cb() {

			$to      = 'alberto.mrtn.nu@gmail.com';
			$subject = 'wp_mail';
			$message = 'This is a test';
			$headers = '';

			$sent_message = wp_mail( $to, $subject, $message, $headers );

			// if ( $sent_message ) {

			// 	error_log( print_r( 'The test message was sent. Check your email inbox.', true ) );
			// } else {

			// 	error_log( print_r( 'The message was not sent!', true ) );

			// }
		}


	}
}
