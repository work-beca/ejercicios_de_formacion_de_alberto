<?php
/**
 * File main class
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_RAFFLE_VERSION' ) ) {
	exit( 'Direct acces forbidden' );
}

if ( ! class_exists( 'YITH_AM_RAFFLE_Plugin' ) ) {

	/**
	 * YITH_AM_RAFFLE_Plugin
	 *
	 * @category
	 * @author
	 * @package
	 * @license
	 */
	class YITH_AM_RAFFLE_Plugin {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_RAFFLE_Plugin
		 */
		private static $instance;
		/**
		 * Admin
		 *
		 * @var YITH_AM_RAFFLE_Plugin
		 */
		public $admin = null;
		/**
		 * Frontend
		 *
		 * @var YITH_AM_RAFFLE_Plugin
		 */
		public $frontend = null;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_RAFFLE_Plugin
		 */
		public static function get_instance() {

			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/***
		 *
		 * YITH_AM_RAFFLE_Plugin constructor
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_am_raffle_require_class',
				array(

					'common'   => array(
						'includes/class-yith-am-raffle-users-db.php',
						'includes/functions.php',
						'includes/class-yith-am-raffle-shortcodes.php',
						'includes/save-db-form.php',
						'includes/class-yith-am-raffle-transients.php',
						'includes/class-yith-am-raffle-cron.php',
						'includes/class-yith-am-raffle-list-participants.php',
					),
					'admin'    => array(
						'includes/class-yith-am-raffle-admin.php',

					),
					'frontend' => array(
						'includes/class-yith-am-raffle-frontend.php',
					),
				)
			);

			$this->require_( $require );
			$this->init_classes();

			/**
			 * Here set any other hoooks(actions o filters you'll use on this class)
			 */
			$this->init();

		}
		/**
		 * _require
		 *
		 * @param  mixed $main_classes Require diferent sections.
		 * @return void
		 */
		protected function require_( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' === $section && is_admin() ) && file_exists( YITH_AM_RAFFLE_DIR_PATH . $class ) ) {
						require_once YITH_AM_RAFFLE_DIR_PATH . $class;
					}
				}
			}
		}
		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {
			YITH_AM_RAFFLE_USERS_DB::get_instance();
			YITH_AM_RAFFLE_Shortcode::get_instance();
			YITH_AM_RAFFLE_Transientparticipants::get_instance();
			YITH_AM_RAFFLE_Cron::get_instance();
			YITH_AM_RAFFLE_List_Participants::get_instance();
		}
		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_AM_RAFFLE_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_AM_RAFFLE_Frontend::get_instance();
			}
		}
	}
}

if ( ! function_exists( 'yith_am_raffle_plugin' ) ) {
	/**
	 * Yith_am_raffle_plugin
	 *
	 * @return YITH_AM_RAFFLE_Plugin
	 */
	function yith_am_raffle_plugin() {
		return YITH_AM_RAFFLE_Plugin::get_instance();
	}
}
