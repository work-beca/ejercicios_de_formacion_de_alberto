<?php
/**
 * YITH functions
 *
 * @package WordPress
 */

if ( ! function_exists( 'yith_am_ruffle_get_template' ) ) {
	/**
	 * Yith_amtestimonials_get_template
	 *
	 * @param  mixed $file_name comment.
	 * @param  mixed $args comment.
	 * @return void
	 */
	function yith_am_raffle_get_template( $file_name, $args = array() ) {
		$full_path = YITH_AM_RAFFLE_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}
