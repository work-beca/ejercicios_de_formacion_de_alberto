<?php
/**
 * File class list raffle participants
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_RAFFLE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'WP_List_Table' ) ) {

	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
	// require_once ABSPATH . 'wp-admin/includes/class-wp-screen.php';// added
	// require_once ABSPATH . 'wp-admin/includes/screen.php';// added
	// require_once ABSPATH . 'wp-admin/includes/template.php';
}

if ( ! class_exists( 'YITH_AM_RAFFLE_List_Participants' ) ) {
	/**
	 * YITH_AM_RAFFLE_List_Participants
	 */
	class YITH_AM_RAFFLE_List_Participants extends WP_List_Table {

		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_RAFFLE_List_Participants
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_AM_RAFFLE_List_Participants
		 */
		public static function get_instance() {

			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_action( 'admin_menu', array( $this, 'list_raffle_participants' ) );
		}
		/**
		 * List_raffle_participants
		 *
		 * @return void
		 */
		public function list_raffle_participants() {
			add_menu_page(
				esc_html__( 'Raffle participants', 'yith-am-raffle' ),
				esc_html__( 'Raffle participants', 'yith-am-raffle' ),
				'activate_plugins',
				'plugin_raffle_list',
				array( $this, 'list_participants_cb' ),
				'dashicons-tickets-alt',
				40
			);
		}
		/**
		 * Get_data_table
		 *
		 * @return datatable
		 */
		public function get_data_table() {
			global $wpdb;

			$datatable = $wpdb->get_results( 'SELECT name, surnames, email FROM wp_yith_raffle_users ' );

			return $datatable;
		}
		/**
		 * Get_columns
		 *
		 * @return array
		 */
		public function get_columns() {
			$columns = array(
				'name'     => ' Name',
				'surnames' => 'Surnames',
				'email'    => 'Email',
				'action'   => 'Actions',
			);

			return $columns;
		}
		/**
		 * Prepare_items
		 *
		 * @return void
		 */
		public function prepare_items() {

			$this->screen = get_current_screen();
			$columns      = $this->get_columns();
			$hidden       = array();
			$sortable     = array();

			$this->_column_headers = array( $columns, $hidden, $sortable );
			$this->process_bulk_action();
			$this->items = $this->get_data_table();

		}
		/**
		 * Column_default
		 *
		 * @param  mixed $item comment.
		 * @param  mixed $column_name comment.
		 * @return  item
		 */
		public function column_default( $item, $column_name ) {
			switch ( $column_name ) {
				case 'name':
				case 'surnames':
				case 'email':
					return $item->$column_name;
				default:
					return print_r( $item, true ); // Mostramos todo el arreglo para resolver problemas
			}
		}
		/**
		 * Column_action
		 *
		 * @param  mixed $item comment.
		 * @return button
		 */
		public function column_action( $item ) {
			$actions = array(
				'delete' => sprintf( '<button type="button"><a href="?page=plugin_raffle_list&action=%s&participant=%s">Delete</a></button>', 'delete', $item->name ),
			);
			return sprintf(
				'' . $this->row_actions( $actions ) . ''
			);
		}
		/**
		 * Process_bulk_action
		 *
		 * @return void
		 */
		public function process_bulk_action() {
			if ( 'delete' === $this->current_action() ) {
				$this->delete_participant( sanitize_text_field( wp_unslash( $_REQUEST['participant'] ) ) );
			}
		}
		/**
		 * Delete_participant
		 *
		 * @param  mixed $name_participant comment.
		 * @return void
		 */
		public function delete_participant( $name_participant ) {
			global $wpdb;

			$result = $wpdb->delete( 'wp_yith_raffle_users', array( 'name' => $name_participant ) );

		}
		/**
		 * List_participants_cb
		 *
		 * @return void
		 */
		public function list_participants_cb() {

			$listparticipants = new YITH_AM_RAFFLE_List_Participants();
			echo '<div class = "wrap" > <h2> My list Raffle Participants </h2>';
			$listparticipants->prepare_items();
			$listparticipants->display();
			echo '</div>';

		}
	}
}
