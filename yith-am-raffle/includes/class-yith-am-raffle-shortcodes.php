<?php
/**
 * YITH class shortcodes raffle
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_RAFFLE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_RAFFLE_Shortcode' ) ) {
	/**
	 * YITH_AM_RAFFLE_Shortcode
	 */
	class YITH_AM_RAFFLE_Shortcode {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_RAFFLE_Shortcode
		 */
		private static $instance;
		/**
		 * Getinstance
		 *
		 * @return YITH_AM_RAFFLE_Shortcode
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_action( 'init', array( $this, 'ready_for_show_shortcodes' ) );

		}
		/**
		 * Ready_for_show_form_users
		 *
		 * @return void
		 */
		public function ready_for_show_shortcodes() {
			add_shortcode( 'yith_show_form_users', array( $this, 'yith_show_form_users_cb' ) );
			add_shortcode( 'yith_show_last_participants', array( $this, 'yith_show_last_participants_cb' ) );
		}

		/**
		 * Yith_show_form_users_cb
		 *
		 * @return code
		 */
		public function yith_show_form_users_cb( $atts = array() ) {
			ob_start();
			yith_am_raffle_get_template( '/frontend/form-users.php', $atts );
			return ob_get_clean();

		}
		/**
		 * Yith_show_last_participants_cb
		 *
		 * @return template
		 */
		public function yith_show_last_participants_cb() {
			ob_start();
			$listparticipants = YITH_AM_RAFFLE_Transientparticipants::get_user_list_transient();
			yith_am_raffle_get_template( '/frontend/list-participants.php', $listparticipants );
			return ob_get_clean();
		}


	}
}
