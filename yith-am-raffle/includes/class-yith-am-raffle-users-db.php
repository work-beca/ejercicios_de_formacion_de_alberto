<?php
/**
 * YITH raffle users Databases
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_RAFFLE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_RAFFLE_USERS_DB' ) ) {

	/**
	 * YITH_RAFFLE_USERS_DB
	 */
	class YITH_AM_RAFFLE_USERS_DB {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_RAFFLE_USERS_DB
		 */
		private static $instance;
		/**
		 * A static variable
		 *
		 * @static
		 * @var string
		 */
		public static $version = '1.0.0';
		/**
		 * A static variable
		 *
		 * @static
		 * @var string
		 */
		public static $raffle_user_table = 'yith_raffle_users';

		/**
		 * Get_instance
		 *
		 * @return YITH_AM_RAFFLE_USERS_DB
		 */
		public static function get_instance() {

			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

		}
		/**
		 * Install
		 *
		 * @return void
		 */
		public static function install() {
			self::create_db_table();
		}
		/**
		 * Create_db_table
		 *
		 * @param  mixed $force comment.
		 * @return void
		 */
		public static function create_db_table( $force = false ) {
			global $wpdb;

			$table_name = $wpdb->prefix . self::$raffle_user_table;

			$sql = "CREATE TABLE IF NOT EXISTS $table_name (
                `id` bigint(20) NOT NULL AUTO_INCREMENT,
                `user_id` bigint(20) NOT NULL,
                `name` VARCHAR(50) NOT NULL,
                `surnames` VARCHAR(50) NOT NULL,
                `email` VARCHAR(100) NOT NULL,
                PRIMARY KEY (id)

			)";
			if ( ! function_exists( 'dbDelta' ) ) {
				require_once ABSPATH . 'wp-admin/includes/upgrade.php';
			}

			dbDelta( $sql );

		}
	}
}
