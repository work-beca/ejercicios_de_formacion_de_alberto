<?php
/**
 * File class frontend
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_RAFFLE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_RAFFLE_Frontend' ) ) {

	/**
	 * YITH_AM_RAFFLE_Frontend
	 */
	class YITH_AM_RAFFLE_Frontend {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_RAFFLE_Frontend
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_RAFFLE_Frontend
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * Construct YITH_AMTESTIMONIALS_Frontend
		 */
		private function __construct() {

			add_action( 'wp_enqueue_scripts', array( $this, 'front_end_scripts_raffle' ) );
		}

		/**
		 * Front_end_scripts_raffle
		 *
		 * @return void
		 */
		public function front_end_scripts_raffle() {

			wp_register_style( 'style_raffle', YITH_AM_RAFFLE_DIR_ASSETS_CSS_URL . '/style.css', false );
			wp_enqueue_style( 'style_raffle' );

		}
	}
}
