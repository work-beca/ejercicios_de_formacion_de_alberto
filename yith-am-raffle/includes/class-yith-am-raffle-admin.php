<?php
/**
 * File class admin
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_RAFFLE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}



if ( ! class_exists( 'YITH_AM_RAFFLE_Admin' ) ) {
	/**
	 * YITH_AM_RAFFLE_Admin
	 */
	class YITH_AM_RAFFLE_Admin {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_RAFFLE_Admin
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_RAFFLE_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
		}
	}
}
