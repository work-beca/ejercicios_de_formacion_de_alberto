<?php
/**
 * File class transients
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_RAFFLE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_RAFFLE_Transientparticipants' ) ) {

	/**
	 * YITH_AM_RAFFLE_Transient_participants
	 */
	class YITH_AM_RAFFLE_Transientparticipants {

		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_RAFFLE_Transientparticipants
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_AM_RAFFLE_Transientparticipants
		 */
		public static function get_instance() {

			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

		}
		/**
		 * Get_db_list_participants
		 *
		 * @return $datatable
		 */
		public static function get_db_list_participants() {
			global $wpdb;

			$datatable = $wpdb->get_results( 'SELECT name, surnames, email FROM wp_yith_raffle_users ORDER BY id DESC LIMIT 5 '  );

			return $datatable;
		}
		/**
		 * Get_user_list_transient
		 *
		 * @return array $listparticipants
		 */
		public static function get_user_list_transient() {

			$listparticipants = get_transient( 'yith_am_list_participants' );

			if ( ! $listparticipants ) {
				$listparticipants = self::get_db_list_participants();
				set_transient( 'yith_am_list_participants', $listparticipants, 5 * MINUTE_IN_SECONDS );
			}

			return $listparticipants;
		}


	}
}
