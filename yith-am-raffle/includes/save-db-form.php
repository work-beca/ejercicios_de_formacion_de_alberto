<?php
/**
 * YITH save params to datatable
 *
 * @package WordPress
 */

/**
 * Value_form_participants_no_logged
 *
 * @return void
 */
function value_form_participants_no_logged() {
	if ( isset( $_POST['raffle_form'] ) && wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['raffle_form'] ) ), 'action_raffle_form' ) ) {

		if ( ! isset( $_POST['InputUser'] ) || empty( $_POST['InputUser'] ) ) {
			wp_safe_redirect( add_query_arg( array( 'errormsg' => __( 'The name field is incomplete', 'yith-am-raffle' ) ), get_home_url() . 'raffle-participants' ) );
			exit;
		}
		if ( ! isset( $_POST['InputLastname'] ) || empty( $_POST['InputLastname'] ) ) {
			wp_safe_redirect( add_query_arg( array( 'errormsg' => __( 'The last name field is incomplete', 'yith-am-raffle' ) ), get_home_url() . 'raffle-participants' ) );
			exit;
		}
		if ( ! isset( $_POST['InputEmail'] ) || empty( $_POST['InputEmail'] ) ) {
			wp_safe_redirect( add_query_arg( array( 'errormsg' => __( 'The email field is incomplete', 'yith-am-raffle' ) ), get_home_url() . 'raffle-participants' ) );
			exit;
		}
		if ( ! isset( $_POST['Check'] ) || empty( $_POST['Check'] ) ) {
			wp_safe_redirect( add_query_arg( array( 'errormsg' => __( 'The check field is incomplete', 'yith-am-raffle' ) ), get_home_url() . 'raffle-participants' ) );
			exit;
		}

		$username = sanitize_text_field( wp_unslash( $_POST['InputUser'] ) );
		$lastname = sanitize_text_field( wp_unslash( $_POST['InputLastname'] ) );
		$email    = sanitize_email( wp_unslash( $_POST['InputEmail'] ) );

		global $wpdb;
		$userid_exist = $wpdb->get_results( $wpdb->prepare( 'SELECT name FROM wp_yith_raffle_users  WHERE email = %s', $email ) ); // db call ok; no-cache ok.

		if ( $userid_exist ) {
			wp_safe_redirect( add_query_arg( array( 'errormsg' => __( 'The user already participates in the raffle', 'yith-am-raffle' ) ), get_home_url() . '/raffle-participants' ) );
			exit;
		} else {

			$wpdb->insert(
				'wp_yith_raffle_users',
				array(
					'name'     => $username,
					'surnames' => $lastname,
					'email'    => $email,
				)
			); // db call ok.
		}
		wp_safe_redirect( add_query_arg( array( 'successmsg' => __( 'Congratulations, you have entered the raffle. Luck!', 'yith-am-raffle' ) ), get_home_url() . '/raffle-participants' ) );
			exit;

	} else {
		print esc_html__( 'Sorry, your nonce did not verify.', 'yith-am-raffle' );
		exit;
	}

}

/**
 * Value_form_participants_logged
 *
 * @return void
 */
function value_form_participants_logged() {

	if ( isset( $_POST['raffle_form'] ) && wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['raffle_form'] ) ), 'action_raffle_form' ) ) {
		global $wpdb;

		$userid       = get_current_user_id();
		$userid_exist = $wpdb->get_results( $wpdb->prepare( 'SELECT user_id FROM wp_yith_raffle_users WHERE user_id = %s', $userid ) );
		if ( $userid_exist ) {
			wp_safe_redirect( add_query_arg( array( 'errormsg' => __( 'The user already participates in the raffle', 'yith-am-raffle' ) ), get_home_url() . '/raffle-participants' ) );
			exit;
		} else {
			$userinfo = get_userdata( $userid );

			$wpdb->insert(
				'wp_yith_raffle_users',
				array(
					'user_id'  => $userid,
					'name'     => $userinfo->first_name,
					'surnames' => $userinfo->last_name,
					'email'    => $userinfo->user_email,
				)
			); // db call ok.
			wp_safe_redirect( add_query_arg( array( 'successmsg' => __( 'Congratulations, you have entered the raffle. Luck!', 'yith-am-raffle' ) ), get_home_url() . '/raffle-participants' ) );
			exit;
		}
	} else {
		print esc_html__( 'Sorry, your nonce did not verify.', 'yith-am-raffle' );
		exit;
	}

}
add_action( 'admin_post_nopriv_raffleform', 'value_form_participants_no_logged' );
add_action( 'admin_post_raffleform', 'value_form_participants_logged' );
