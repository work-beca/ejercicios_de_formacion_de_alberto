<?php
/** File colorpicker input
 *
 * @author Alberto Martín Núñez
 * @package WordPress
 */

global $post;

if ( ! is_object( $post ) ) {

	$value_get_options = get_option( 'border_note_box_name' );
	$value_input_style = empty( $value_get_options['style'] ) ? esc_html__( $args['value'] ) : esc_html__( $value_get_options['style'] );
}
?>


<p class='form-field'>
	<label>
		<?php echo esc_html__( $args['label'], 'yith-am-product-purchase-note' ); ?>
	</label>
	<select 
	name='<?php echo esc_html__( $args['name'] ); ?>'>
		<?php foreach ( $args['option'] as $option ) { ?>
			<option <?php $selected = $option === $value_input_style ? 'selected' : ' ';
					echo esc_html__( $selected );?>>
				<?php
				echo esc_html__( $option );
				?>
			</option>
			<?php
		}
		?>
	</select>
</p>
