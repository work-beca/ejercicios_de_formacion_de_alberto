<?php

global $post;
$value_get_post_meta  = get_post_meta( $post->ID, $args['name'] );
$value_input_textarea = ! empty( $value_get_post_meta ) ? esc_html__( $value_get_post_meta[0] ) : '';

?>


<p class='form-field <?php echo esc_html__( $args['div_class'] ); ?>'>
		<label> <?php echo esc_html__( $args['label'], 'yith-am-product-purchase-note' ); ?></label>
		<textarea  
			rows='3'
			name ='<?php echo esc_html__( $args['name'] ); ?>'
			class ='<?php echo esc_html__( $args['class'] ); ?>'
			placeholder = '<?php echo esc_html__( $args['placeholder'] ); ?>'	
		><?php echo esc_html( $value_input_textarea ); ?></textarea>
</p>
