<?php
/** File number input
 *
 * @author Alberto Martín Núñez
 * @package WordPress
 */

global $post;

if ( is_object( $post ) ) {

	$value_get_post_meta = get_post_meta( $post->ID, $args['name'] );
	$value_input_number  = empty( $value_get_post_meta[0] ) ? esc_html__( $args['value'] ) : esc_html__( $value_get_post_meta[0] );

} else {
	$value_get_options_border  = get_option( 'border_note_box_name' );
	if ( $args['id'] === 'yitham_NBC_border_weight_id' ) {
		$value_input_number = empty( $value_get_options_border['weight'] ) ? esc_html__( $args['value'] ) : esc_html__( $value_get_options_border['weight'] );
	} else {
		$value_input_number = empty( $value_get_options_border['radius'] ) ? esc_html__( $args['value'] ) : esc_html__( $value_get_options_border['radius'] );

	}


	$value_get_options_padding  = get_option( 'padding_note_box_name' );
	if ( $args['id'] === 'yitham_text_NBC_padding_top_id' ) {
		$value_input_number = empty( $value_get_options_padding['top'] ) ? esc_html__( $args['value'] ) : esc_html__( $value_get_options_padding['top'] );
	}
	if ( $args['id'] === 'yitham_text_NBC_padding_right_id' ) {
		$value_input_number = empty( $value_get_options_padding['top'] ) ? esc_html__( $args['value'] ) : esc_html__( $value_get_options_padding['right'] );
	}
	if ( $args['id'] === 'yitham_text_NBC_padding_bottom_id' ) {
		$value_input_number = empty( $value_get_options_padding['top'] ) ? esc_html__( $args['value'] ) : esc_html__( $value_get_options_padding['bottom'] );
	}
	if ( $args['id'] === 'yitham_text_NBC_padding_left_id' ) {
		$value_input_number = empty( $value_get_options_padding['top'] ) ? esc_html__( $args['value'] ) : esc_html__( $value_get_options_padding['left'] );
	}
}

?>

<p class='form-field <?php echo esc_html__( $args['div_class'] ); ?>'>
		<label><?php echo esc_html__( $args['label'] ); ?> </label>
		<input 
			type='number' 
			id='<?php echo esc_html__( $args['id'] ); ?>' 
			name='<?php echo esc_html__( $args['name'] ); ?>'
			min='<?php echo esc_html__( $args['min'] ); ?>'
			value='<?php echo esc_html__( $value_input_number ); ?>'
			>
</p>
