<?php
/** File number input
 *
 * @author Alberto Martín Núñez
 * @package WordPress
 */

global $post;
$checked = $args['check'];
if ( is_object( $post ) ) {

	$value_get_post_meta = get_post_meta( $post->ID, $args['name'] );
	if ( ! empty( $value_get_post_meta ) ) {
		$checked = $value_get_post_meta[0] === $args['value'] ? 'checked' : '';
	}
} else {

	if ( $args['name'] === 'position_product_note_box_name' ) {
		$value_get_option = get_option( 'position_product_note_box_name' );
		$checked          = $args['value'] === $value_get_option ? 'checked' : $checked;
	}


	if ( $args['name'] === 'position_shop_note_box_name' ) {
		$value_get_option = get_option( 'position_shop_note_box_name' );
		$checked          = $args['value'] === $value_get_option ? 'checked' : $checked;
	}
}

?>
<p class='form-field <?php echo esc_html__( $args['div_class'] ); ?>'>
	<label>
		<?php echo esc_html__( $args['label'], 'yith-am-product-purchase-note' ); ?>
	</label>	
	<input 
		type='radio' 
		name='<?php echo esc_html__( $args['name'] ); ?>'  
		value='<?php echo esc_html__( $args['value'] ); ?>' 
		id='<?php echo esc_html__( $args['id'] ); ?>'
		<?php echo esc_html__( $checked ); ?>
	>
</p>
