<?php
/**
 * File purchase note tab
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_PRODUCT_PURCHASE_NOTE_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

$purchase_note_product = wc_get_product( $post );
$post_id_              = '';

if ( $purchase_note_product instanceof WC_Product ) {
	$post_id_ = $purchase_note_product->get_id();

} else {
	if ( ! empty( $_GET['product_id'] ) ) {
		$purchase_note_product = wc_get_product( $_GET['product_id'] );
		$post_id_              = ( isset( $purchase_note_product ) && $purchase_note_product instanceof WC_Product ) ? $purchase_note_product->get_id() : '';
	}
}

?>
<div>
	<h3 class="yith-am-title-purchase-note-tab"><?php echo esc_html__( 'Purchase Note Config', 'yith-am-product-purchase-note' ); ?></h3>
</div>

<?php

do_action( 'yith_ampn_before_purchase_note_tab', $post_id_ );
