<?php

global $post;
$value_get_post_meta = get_post_meta( $post->ID, $args['name'] );
$value_input_text    = empty( $value_get_post_meta ) ? esc_html__( $args['value'] ) : esc_html__( $value_get_post_meta[0] );

?>

<p class='form-field <?php echo esc_html__( $args['div_class'] ); ?>'>
	<label> <?php echo esc_html__( $args['label'] ); ?> </label>
	<input 
		type='text' 
		name='<?php echo esc_html__( $args['name'] ); ?>'
		placeholder='<?php echo esc_html__( $args['placeholder'] ); ?>'
		value = '<?php echo esc_html__( $value_input_text ); ?>'
		>
</p>
