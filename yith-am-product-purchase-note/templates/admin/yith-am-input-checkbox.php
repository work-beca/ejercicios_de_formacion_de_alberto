<?php

global $post;
$value_get_post_meta[0] = '';
if ( ! empty( get_post_meta( $post->ID, $args['name'] ) ) ) {
	$value_get_post_meta = get_post_meta( $post->ID, $args['name'] );
}

$checked = $value_get_post_meta[0] === 'on' ? 'checked' : '';
?>

<p class='form-field'>
	<label><?php echo esc_html__( $args['label'], 'yith-am-product-purchase-note' ); ?></label>
		<input 
			type='checkbox' 
			name='<?php echo esc_html__( $args['name'] ); ?>'
			class='<?php echo esc_html__( $args['class'] ); ?>' 
			id='<?php echo esc_html__( $args['id'] ); ?>'  
			<?php echo esc_attr( $checked ); ?>
			>
</p>
