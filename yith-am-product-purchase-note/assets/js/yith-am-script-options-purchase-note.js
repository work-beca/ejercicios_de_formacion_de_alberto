jQuery(document).ready(function ($) {

    /***** COMPROBACIONES LA PRIMERA VEZ QUE SE CARGA ****/

    // Comprobamos el onoff del note antes de  hacer click
    if ('yes' == $('.yampn-purchase-note-onoff > input').val()) {
        $('.group-note-purchase').show();
    } else {
        $('.group-note-purchase').hide();
    }
    // Comprobamos el onoff del badge antes de  hacer click
    if ('yes' == $('.yampn-purchase-badge-onoff > input').val()) {
        $('.group-badge-purchase').show();
    } else {
        $('.group-badge-purchase').hide();
    }
    // Comprobamos el select price
    if ('Free' == $('#_yith_ampn_input_type_price').val()) {
        $('._yith_ampn_text_price').hide();
        $('._yith_ampn_number_free').hide();
    } else {
        $('._yith_ampn_text_price').show();
        $('._yith_ampn_number_free').show();
    }

    /***** FIN DE LAS COMPROBACIONES  INICIALES *****/
    /** ------------------------------------------------------------------ */

    /** COMPROBACIONES AL CAMBIAR O HACER CLICK EN ALGUN CAMPO */

    // Comprobamos cuando se hace click en el onoff para el note
    $('.yampn-purchase-note-onoff').on('click', function () {

        if ('yes' == $('.yampn-purchase-note-onoff > input').val()) {
            $('.group-note-purchase').hide();
        } else {
            $('.group-note-purchase').show();
        }
        if ('Free' == $('#_yith_ampn_input_type_price').val()) {
            $('._yith_ampn_text_price').hide();
            $('._yith_ampn_number_free').hide();
        }
    });

    // Comprobamos cuando se hace click en el onoff para el badge
    $('.yampn-purchase-badge-onoff').on('click', function () {

        $('.group-badge-purchase').toggle("slow", function () {
            // Animation complete.
        });
    });
    // Comprobamos cuando se cambiar el valor del radio type priceç

    $('#_yith_ampn_input_type_price').on('change', function () {

        if ('Free' == $(this).val()) {
            $('._yith_ampn_text_price').hide();
            $('._yith_ampn_number_free').hide();
        } else {
            $('._yith_ampn_text_price').show();
            $('._yith_ampn_number_free').show();
        }
    });

});