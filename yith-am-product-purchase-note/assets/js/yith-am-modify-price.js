jQuery(document).ready(function ($) {

    let values_note = Object.values(price_note_purchase);
    let array_text = values_note[0].toString().split(' ');
    let type_price = values_note[2].toString();

    let price_value_note = array_text[0];
    let number_character = values_note[1];

    let tam_word = 0;
    let price = 0.00;

    $('.total-price').val('0.00');

    $('.yith-am-input-text-note-purchase').on('input', function () {

        if (type_price == 'Fixed') {
            tam_word = $('.yith-am-input-text-note-purchase').val().length;

            if (tam_word > number_character) {
                $('.total-price').val(price_value_note + '.00');
            } else {
                $('.total-price').val('0.00');
            }

        }

        if (type_price == 'Per Character') {
            tam_word = $('.yith-am-input-text-note-purchase').val().length;

            if (tam_word > number_character) {
                price = (tam_word - number_character) * price_value_note;
                $('.total-price').val(price + '.00');
            } else {
                $('.total-price').val('0.00');
            }
        }
    });

});