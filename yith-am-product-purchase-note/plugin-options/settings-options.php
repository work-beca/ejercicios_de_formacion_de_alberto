<?php
/**
 * File settings options
 *
 * @package WordPress
 */

$settings = array(
	'settings' => array(
		'padding-options'                               => array(
			'title' => __( 'Padding Options', 'yith-am-product-purchase-note' ),
			'type'  => 'title',
		),
		'yith-ampt-number-padding-top'                  => array(
			'id'        => 'yith-ampt-number-padding-top',
			'name'      => __( 'Padding Top', 'yith-am-product-purchase-note' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'std'       => 20,
			'desc'      => __( 'Select your padding top', 'yith-am-product-purchase-note' ),
		),
		'yith-ampt-number-padding-right'                => array(
			'id'        => 'yith-ampt-number-padding-right',
			'name'      => __( 'Padding right', 'yith-am-product-purchase-note' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'std'       => 25,
			'desc'      => __( 'Select your padding right', 'yith-am-product-purchase-note' ),
		),
		'yith-ampt-number-padding-bottom'               => array(
			'id'        => 'yith-ampt-number-padding-bottom',
			'name'      => __( 'Padding bottom', 'yith-am-product-purchase-note' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'std'       => 25,
			'desc'      => __( 'Select your padding bottom', 'yith-am-product-purchase-note' ),
		),
		'yith-ampt-number-padding-left'                 => array(
			'id'        => 'yith-ampt-number-padding-left',
			'name'      => __( 'Padding left', 'yith-am-product-purchase-note' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'std'       => 25,
			'desc'      => __( 'Select your padding left', 'yith-am-product-purchase-note' ),
		),
		'padding-options-end'                           => array(
			'type' => 'sectionend',
			'id'   => 'yith-ampn-padding-options',
		),

		// ¡New Section
		'border-options'                                => array(
			'title' => __( 'Border Options', 'yith-am-product-purchase-note' ),
			'type'  => 'title',
		),
		'yith-ampt-number-weight'                       => array(
			'id'        => 'yith-ampt-number-weight',
			'name'      => __( 'Border weight', 'yith-am-product-purchase-note' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'default'   => 3,
			'desc'      => __( 'Select your border weight', 'yith-am-product-purchase-note' ),
		),
		'yith-ampt-select-border-style'                 => array(
			'id'        => 'yith-ampt-select-border-style',
			'name'      => __( 'Border style', 'yith-am-product-purchase-note' ),
			'type'      => 'yith-field',
			'yith-type' => 'select',
			'options'   => array(
				'groove' => 'groove',
				'solid'  => 'solid',
				'double' => 'double',
			),
			'desc'      => __( 'Select your padding right', 'yith-am-product-purchase-note' ),
		),
		'yith-ampt-colorpicker-background-color-border' => array(
			'id'        => 'yith-ampt-colorpicker-background-color-border',
			'name'      => __( 'Border color', 'yith-am-product-purchase-note' ),
			'type'      => 'yith-field',
			'yith-type' => 'colorpicker',
			'default'   => '#d8d8d8',
			'desc'      => __( 'Select your padding bottom', 'yith-am-product-purchase-note' ),
		),
		'yith-ampt-number-border-radius'                => array(
			'id'        => 'yith-ampt-number-border-radius',
			'name'      => __( 'Border radius', 'yith-am-product-purchase-note' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'default'   => 7,
			'desc'      => __( 'Select your border weight', 'yith-am-product-purchase-note' ),
		),
		'border-options-end'                            => array(
			'type' => 'sectionend',
			'id'   => 'yith-ampn-border-options',
		),

		// ¡New Section
		'position-product-options'                      => array(
			'title' => __( 'Position Product', 'yith-am-product-purchase-note' ),
			'type'  => 'title',
		),
		'yith-ampt-radio-position-product'              => array(
			'id'        => 'yith-ampt-radio-position-product',
			'name'      => __( 'Badge position in product', 'yith-am-product-purchase-note' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'value'     => 'Top-left',
			'options'   => array(
				'top-left-option'  => 'Top-left',
				'top-right-option' => 'Top-right',
			),
			'desc'      => __( 'Select your badge position in product', 'yith-am-product-purchase-note' ),
		),
		'position-product-options-end'                  => array(
			'type' => 'sectionend',
			'id'   => 'yith-ampn-position-product-options',
		),

		// ¡New Section
		'position-shop-options'                         => array(
			'title' => __( 'Position Shop', 'yith-am-product-purchase-note' ),
			'type'  => 'title',
		),
		'yith-ampt-radio-position-shop'                 => array(
			'id'        => 'yith-ampt-radio-position-shop',
			'name'      => __( 'Badge position in shop', 'yith-am-product-purchase-note' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'value'     => 'Top-left',
			'options'   => array(
				'top-left-option'  => 'Top-left',
				'top-right-option' => 'Top-right',
			),
			'desc'      => __( 'Select your badge position in shop', 'yith-am-product-purchase-note' ),
		),
		'position-shop-options-end'                  => array(
			'type' => 'sectionend',
			'id'   => 'yith-ampn-position-product-options',
		),
	),
);

return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
