<?php
/**
 * YITH functions
 *
 * @package WordPress
 */

if ( ! function_exists( 'yith_am_product_purchase_note_get_template' ) ) {
	/**
	 * Yith_am_product_purchase_note_get_template
	 *
	 * @param  mixed $file_name comment.
	 * @param  mixed $args comment.
	 * @return void
	 */
	function yith_am_product_purchase_note_get_template( $file_name, $args = array() ) {
		$full_path = YITH_AM_PRODUCT_PURCHASE_NOTE_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_am_product_purchase_note_get_inputs' ) ) {
	/**
	 * Yith_am_product_purchase_note_get_template
	 *
	 * @param  mixed $file_name comment.
	 * @param  mixed $args comment.
	 * @return void
	 */
	function yith_am_product_purchase_note_get_inputs( $file_name, $args = array() ) {
		$full_path = YITH_AM_PRODUCT_PURCHASE_NOTE_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_ampn_product_metabox_form_field' ) ) {
	/**
	 * Print a form field for product metabox
	 *
	 * @since 2.0.0
	 */
	function yith_ampn_product_metabox_form_field( $field ) {

		$defaults = array(
			'class'     => '',
			'title'     => '',
			'label_for' => '',
			'desc'      => '',
			'data'      => array(),
			'fields'    => array(),
		);
		$field    = apply_filters( 'yith_wcact_product_metabox_form_field_args', wp_parse_args( $field, $defaults ), $field );
		/**
		 * @var string $class
		 * @var string $title
		 * @var string $label_for
		 * @var string $desc
		 * @var array  $data
		 * @var array  $fields
		 */
		extract( $field );

		if ( ! $label_for && $fields ) {
			$first_field = current( $fields );
			if ( isset( $first_field['id'] ) ) {
				$label_for = $first_field['id'];
			}
		}

		$data_html = '';
		foreach ( $data as $key => $value ) {
			$data_html .= "data-{$key}='{$value}' ";
		}

		$html  = '';
		$html .= "<div class='yith-ampn-form-field {$class}' {$data_html}>";
		$html .= "<label class='yith-wcact-form-field__label' for='{$label_for}'>{$title}</label>";

		$html .= "<div class='yith-ampn-form-field__container'>";
		ob_start();
		yith_plugin_fw_get_field( $fields, true ); // Print field using plugin-fw
		$html .= ob_get_clean();
		$html .= '</div><!-- yith-ampn-form-field__container -->';

		if ( $desc ) {
			$html .= "<div class='yith-ampn-form-field__description'>{$desc}</div>";
		}

		$html .= '</div><!-- yith-ampn-form-field -->';

		echo apply_filters( 'yith_wcact_product_metabox_form_field_html', $html, $field );
	}
}
