<?php
/**
 * File class inputs
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_PRODUCT_PURCHASE_NOTE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_PRODUCT_PURCHASE_NOTE_Inputs' ) ) {

	/**
	 * YITH_AM_PRODUCT_PURCHASE_NOTE_Frontend
	 */
	class YITH_AM_PRODUCT_PURCHASE_NOTE_Inputs {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_PRODUCT_PURCHASE_NOTE_Inputs
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_PRODUCT_PURCHASE_NOTE_Inputs
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

		}


		/**
		 * Input_params
		 *
		 * @return $args
		 */
		public static function show_input( $fields ) {

			$fields_default = array(
				'type_input'         => '',
				'id'                 => '',
				'name'               => '',
				'class'              => '',
				'div_class'          => '',
				'label'              => '',
				'placeholder'        => '',
				'value'              => '',
				'check'              => '',
				'min'                => '',
				'data_default_color' => '',
				'option'             => '',

			);

			$fields_merge = array_merge( $fields_default, $fields );
			return yith_am_product_purchase_note_get_inputs( '/admin/yith-am-input-' . $fields_merge['type_input'] . '.php', $fields_merge );

		}

	}
}
