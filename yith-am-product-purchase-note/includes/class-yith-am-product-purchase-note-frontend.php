<?php
/**
 * File class frontend
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_PRODUCT_PURCHASE_NOTE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_PRODUCT_PURCHASE_NOTE_Frontend' ) ) {

	/**
	 * YITH_AM_PRODUCT_PURCHASE_NOTE_Frontend
	 */
	class YITH_AM_PRODUCT_PURCHASE_NOTE_Frontend {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_PRODUCT_PURCHASE_NOTE_Frontend
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_PRODUCT_PURCHASE_NOTE_Frontend
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'yith_am_add_note_box' ) );
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'front_end_product_scripts_price' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'front_end_product_scripts_purchase_note' ) );

			add_action( 'woocommerce_product_thumbnails', array( $this, 'yith_am_custom_badge' ) );

			add_action( 'wp_enqueue_scripts', array( $this, 'front_end_product_scripts_price_register' ) );

			add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'note_purchase_add_to_cart_validation' ), 10, 4 );
			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'note_purchase_add_cart_item_data' ), 10, 3 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'yith_am_note_purchase_get_item_data' ), 10, 2 );

			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'yith_am_note_purchase_checkout_create_order_line_item' ), 10, 4 );

			add_action( 'woocommerce_before_calculate_totals', array( $this, 'yith_am_note_purchase_add_price' ), 99 );

			add_filter( 'woocommerce_sale_flash', array( $this, 'yith_am_lw_hide_sale_flash' ) );
		}

		/**
		 * Lw_hide_sale_flash: Usamos esta funcion para eliminar el badge "SALE" que aparece por defecto en caso de que no tengamos el custom activated
		 *
		 * @return void
		 */
		public function yith_am_lw_hide_sale_flash() {
			return false;
		}

		/**
		 * Yith_am_note_purchase_add_price
		 *
		 * @return void
		 */
		public function yith_am_note_purchase_add_price( $cart_object ) {
			foreach ( $cart_object->cart_contents as $key => $value ) {

				if ( isset( $value['yith_am_input_price_note_purchase'] ) ) {

					$additionalPrice = $value['yith_am_input_price_note_purchase'];

					$orgPrice = floatval( $value['data']->get_price() );

					$discPrice = $orgPrice + $additionalPrice;
					$value['data']->set_price( $discPrice );
				}
			}
		}

		public function yith_am_note_purchase_get_item_data( $item_data, $cart_item_data ) {

			if ( isset( $cart_item_data['yith_am_input_text_note_purchase'] ) ) {
				$item_data[] = array(
					'key'   => __( 'Purchase note', 'yith-am-product-purchase-note' ),
					'value' => wc_clean( $cart_item_data ['yith_am_input_text_note_purchase'] ),
				);

				$item_data[] = array(
					'key'   => __( 'Note price added', 'yith-am-product-purchase-note' ),
					'value' => wc_clean( $cart_item_data ['yith_am_input_price_note_purchase'] ) . ' $',
				);

			}

				return $item_data;
		}

		/**
		 * Yith_am_note_purchase_checkout_create_order_line_item
		 *
		 * @return void
		 */
		public function yith_am_note_purchase_checkout_create_order_line_item( $item, $cart_item_key, $values, $order ) {

			if ( isset( $values['yith_am_input_text_note_purchase'] ) && isset( $values['yith_am_input_price_note_purchase'] ) ) {
				$item->update_meta_data(
					__( 'Purchase note', 'yith-am-product-purchase-note' ),
					$values['yith_am_input_text_note_purchase'],
					true
				);
				$item->update_meta_data(
					__( 'Price', 'yith-am-product-purchase-note' ),
					$values['yith_am_input_price_note_purchase'],
					true
				);
			}
		}



		/**
		 * Note_purchase_add_to_cart_validation
		 *
		 * @return bool
		 */
		public function note_purchase_add_to_cart_validation( $passed, $product_id, $quantity, $variation_id = null ) {

			if ( empty( $_POST['yith-am-input-text-note-purchase'] ) ) {
				$passed = false;
				wc_add_notice( __( 'Your field is a required field.', 'yith-am-product-purchase-note' ), 'error' );
			}
			return $passed;

		}

		/**
		 * note_purchase_add_cart_item_data
		 *
		 * @param  mixed $cart_item_data
		 * @param  mixed $product_id
		 * @param  mixed $variaci
		 * @return $card_item_data
		 */
		public function note_purchase_add_cart_item_data( $cart_item_data, $product_id, $variación_id ) {

			if ( isset( $_POST ['yith-am-input-text-note-purchase'] ) ) {
				$cart_item_data ['yith_am_input_text_note_purchase']  = sanitize_text_field( $_POST ['yith-am-input-text-note-purchase'] );
				$cart_item_data ['yith_am_input_price_note_purchase'] = sanitize_text_field( $_POST ['yith-am-input-price-note-purchase'] );
			}
			return $cart_item_data;
		}
		/**
		 * Front_end_product_scrits_price
		 *
		 * @return void
		 */
		public function front_end_product_scripts_price_register() {

			wp_register_script( 'yith_am_modify_price_purchase', YITH_AM_PRODUCT_PURCHASE_NOTE_DIR_ASSETS_JS_URL . '/yith-am-modify-price.js', array( 'jquery' ), false, true );

		}

		/**
		 * Front_end_product_scripts_price
		 *
		 * @return void
		 */
		public function front_end_product_scripts_price() {
			global $product;
			$id_product_current = $product->get_id();

			$price_note            = ! empty( get_post_meta( $id_product_current, '_yith_ampn_text_price' ) ) ? get_post_meta( $id_product_current, '_yith_ampn_text_price' ) : '';
			$number_character_free = ! empty( get_post_meta( $id_product_current, '_yith_ampn_number_free' ) ) ? get_post_meta( $id_product_current, '_yith_ampn_number_free' ) : '';

			$radio_price_setting = ! empty( get_post_meta( $id_product_current, '_yith_ampn_input_type_price' ) ) ? get_post_meta( $id_product_current, '_yith_ampn_input_type_price' ) : '';

			$value_price_note_purchase = array(
				'price'                 => $price_note,
				'number_character_free' => $number_character_free,
				'price_setting'         => $radio_price_setting,
			);

			wp_localize_script( 'yith_am_modify_price_purchase', 'price_note_purchase', $value_price_note_purchase );

			wp_enqueue_script( 'yith_am_modify_price_purchase' );

			$custom_css_price = '
					.price-note-purchase{
						display: flex;
						justify-content:flex-end;
						margin-right:20px;
						
					}';

				wp_add_inline_style( 'style_product_purchase_note', $custom_css_price );
				wp_enqueue_style( 'style_product_purchase_note' );
		}

		/**
		 * My_custom_sale_flash
		 *
		 * @param  mixed $text comment.
		 * @return html
		 */
		public function yith_am_custom_badge() {
			global $product;
			$id_product_current = $product->get_id();
			$check_badge        = get_post_meta( $id_product_current, '_yith_ampn_onoff_show_badge' );

			if ( ! empty( $check_badge[0] ) && 'yes' === $check_badge[0] ) {

				$text_badge             = ! empty( get_post_meta( $id_product_current, '_yith_ampn_text_badge' ) ) ? get_post_meta( $id_product_current, '_yith_ampn_text_badge' ) : '';
				$color_badge            = ! empty( get_post_meta( $id_product_current, '_yith_ampn_color_picker_textcolor' ) ) ? get_post_meta( $id_product_current, '_yith_ampn_color_picker_textcolor' ) : '';
				$background_color_badge = ! empty( get_post_meta( $id_product_current, '_yith_ampn_color_picker_backgroundcolor' ) ) ? get_post_meta( $id_product_current, '_yith_ampn_color_picker_backgroundcolor' ) : '';

				$position_badge = ( get_option( 'yith-ampt-radio-position-product' ) ) === 'top-right-option' ? 'left:70%;' : 'left: 0;';

				$custom_css_badge = '
					.yith-am_badge{
						position: absolute;
						top: 0;' .
						$position_badge . '
						display: inline-block;
						background: #cd2653;
						color: #fff;
						font-family: -apple-system,blinkmacsystemfont,"Helvetica Neue",helvetica,sans-serif;
						font-size: 1.7rem;
						font-weight: 700;
						letter-spacing: -.02em;
						line-height: 1.2;
						padding: 1.5rem;
						text-transform: uppercase;
						z-index: 1;
					}
					.onsale {display:none}
					';

				wp_add_inline_style( 'style_product_purchase_note', $custom_css_badge );
				wp_enqueue_style( 'style_product_purchase_note' );?>

				<span class="yith-am_badge"><?php echo esc_html( $text_badge[0] ); ?> </span>

				<?php
			}
		}

		/**
		 * Front_end_product_scripts_purchase_note
		 *
		 * @return void
		 */
		public function front_end_product_scripts_purchase_note() {

			wp_register_style( 'style_product_purchase_note', YITH_AM_PRODUCT_PURCHASE_NOTE_DIR_ASSETS_CSS_URL . '/yith-am-style-frontend-product.css', false );

			$custom_css = '
                            .yith-am-div-note-box{
								width: 500px;
								height: auto;
								border: ' . get_option( 'yith-ampt-number-weight' ) . 'px ' . get_option( 'yith-ampt-select-border-style' ) . ' ' . get_option( 'yith-ampt-colorpicker-background-color-border' ) . ';
                                border-radius: ' . get_option( 'yith-ampt-number-border-radius' ) . 'px;
							}
							.yith-am-padding-note-purchase{
								padding-top: ' . get_option( 'yith-ampt-number-padding-top' ) . 'px;
								padding-right: ' . get_option( 'yith-ampt-number-padding-right' ) . 'px;
								padding-bottom: ' . get_option( 'yith-ampt-number-padding-bottom' ) . 'px;
								padding-left: ' . get_option( 'yith-ampt-number-padding-left' ) . 'px;
							}
							.yith-am-input-text-note-purchase{	
								width: 400px !important;
								height: auto;
							}';

			wp_add_inline_style( 'style_product_purchase_note', $custom_css );
		}
		/**
		 * Yith_am_add_note_box
		 *
		 * @return void
		 */
		public function yith_am_add_note_box() {
			global $product;
			$id_product_current = $product->get_id();
			$check              = get_post_meta( $id_product_current, '_yith_ampn_onoff_show_note' );
			if ( ! empty( $check[0] ) && 'yes' === $check[0] ) {
				$check_title               = get_post_meta( $id_product_current, '_yith_ampn_textnote' );
				$check_description         = get_post_meta( $id_product_current, '_yith_ampn_textarea' );
				$type_input                = get_post_meta( $id_product_current, '_yith_ampn_input_type' );
				$title_purchase_note       = ! empty( $check_title[0] ) ? $check_title[0] : '';
				$description_purchase_note = ! empty( $check_description[0] ) ? $check_description[0] : '';

				wp_enqueue_style( 'style_product_purchase_note' );
				?>
				<div class = 'yith-am-div-note-box' >
					<div class = 'yith-am-padding-note-purchase'><strong><?php echo esc_html( $title_purchase_note ); ?></strong></div>
					<div class = 'yith-am-padding-note-purchase'><?php echo esc_html( $description_purchase_note ); ?></div>
					<div class = 'yith-am-padding-note-purchase'>
					<?php
					if ( 'Text-Option' === $type_input[0] ) {
						?>

						<input class = 'yith-am-input-text-note-purchase' type = 'text' value = '' name='yith-am-input-text-note-purchase' >
					<?php	} else { ?>
						<textarea class = 'yith-am-inputarea-text-note-purchase' rows="4" cols="50" value = '' name='yith-am-input-textarea-note-purchase' ></textarea>
						<?php
					}
					?>
						
					</div>	
					<div class= 'price-note-purchase'>
						<input  class='total-price' name = 'yith-am-input-price-note-purchase' readonly='readonly'><?php echo get_woocommerce_currency_symbol(); ?>
					</div>
				</div><br>
				<?php
			}

		}

	}
}
