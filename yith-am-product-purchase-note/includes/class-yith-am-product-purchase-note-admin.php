<?php
/**
 * File class admin
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_PRODUCT_PURCHASE_NOTE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}



if ( ! class_exists( 'YITH_AM_PRODUCT_PURCHASE_NOTE_Admin' ) ) {
	/**
	 * YITH_AM_PRODUCT_PURCHASE_NOTE_Admin
	 */
	class YITH_AM_PRODUCT_PURCHASE_NOTE_Admin {

		/* @var YIT_Plugin_Panel_WooCommerce $_panel the panel */
		private $_panel; //phpcs:ignore
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_PRODUCT_PURCHASE_NOTE_Admin
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_PRODUCT_PURCHASE_NOTE_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_filter( 'plugin_action_links_' . plugin_basename( YITH_AM_PRODUCT_PURCHASE_NOTE_DIR_PATH . '/' . basename( YITH_AM_PRODUCT_PURCHASE_NOTE_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );

			// Add tabs for product auction.
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yiht_ampn_purchase_note_tab' ) );
			// Add options to general product data tab.
			add_action( 'woocommerce_product_data_panels', array( $this, 'yith_ampn_add_product_data_panels' ) );

			add_action( 'yith_ampn_before_purchase_note_tab', array( $this, 'yith_ampn_before_purchase_note_tab' ) );

			add_action( 'woocommerce_process_product_meta', array( $this, 'save_purchase_note_options' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'purchase_note_options_style_icon' ) );
			// Enqueue Scripts.
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

			// Create YITH menu !
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
		}
		/**
		 * Enqueue_scripts
		 *
		 * @return void
		 */
		public function enqueue_scripts() {
			$screen                        = get_current_screen();
			$is_product                    = 'product' === $screen->id;
			$settings_section_dependencies = $is_product ? array( 'jquery', 'yith-plugin-fw-fields' ) : array( 'jquery' );

			wp_register_style( 'yith-ampn-admin-css', YITH_AM_PRODUCT_PURCHASE_NOTE_DIR_ASSETS_URL . '/css/yith-am-admin-purchase-note.css', array( 'yith-plugin-fw-fields' ), YITH_AM_PRODUCT_PURCHASE_NOTE_VERSION );
			wp_enqueue_style( 'yith-ampn-admin-css' );

			wp_register_script( 'yith-ampn-admin-settings-sections', YITH_AM_PRODUCT_PURCHASE_NOTE_DIR_ASSETS_URL . '/js/yith-am-admin-settings-sections.js', $settings_section_dependencies, YITH_AM_PRODUCT_PURCHASE_NOTE_VERSION, true );
			wp_enqueue_script( 'yith-ampn-admin-settings-sections' );

			wp_register_style( 'admin-styles', YITH_AM_PRODUCT_PURCHASE_NOTE_DIR_ASSETS_CSS_URL . '/yith-am-style-admin.css', false ); // phpcs:ignore
			wp_enqueue_style( 'admin-styles' );

			wp_register_script( 'yitham-purchase-note-options', YITH_AM_PRODUCT_PURCHASE_NOTE_DIR_ASSETS_JS_URL . '/yith-am-script-options-purchase-note.js', array( 'jquery' ), '1', true );
			wp_enqueue_script( 'yitham-purchase-note-options' );
		}

		/**
		 * Yith_before_purchase_note_tab
		 *
		 * @param  mixed $post_id .
		 * @return void
		 */
		public function yith_ampn_before_purchase_note_tab( $post_id ) {

			$product = wc_get_product( $post_id );
			// !Get note values
			$value_checkbox   = get_post_meta( $post_id, '_yith_ampn_onoff_show_note', true );
			$value_text_note  = get_post_meta( $post_id, '_yith_ampn_textnote', true );
			$value_input_type = get_post_meta( $post_id, '_yith_ampn_input_type', true );

			$value_textarea    = get_post_meta( $post_id, '_yith_ampn_textarea', true );
			$value_type_price  = get_post_meta( $post_id, '_yith_ampn_input_type_price', true );
			$value_text_price  = get_post_meta( $post_id, '_yith_ampn_text_price', true );
			$value_number_free = get_post_meta( $post_id, '_yith_ampn_number_free', true );

			// !Get badge values
			$value_checkbox_badge        = get_post_meta( $post_id, '_yith_ampn_onoff_show_badge', true );
			$value_text_badge            = get_post_meta( $post_id, '_yith_ampn_text_badge', true );
			$value_backgroundcolor_badge = get_post_meta( $post_id, '_yith_ampn_color_picker_backgroundcolor', true );
			$value_textcolor_badge       = get_post_meta( $post_id, '_yith_ampn_color_picker_textcolor', true );

			yith_ampn_product_metabox_form_field(
				array(
					'class'  => 'form-field wc_auction_field yith-plugin-ui',
					'title'  => esc_html__( 'Enable/Disable Note', 'yith-am-product-purchase-note' ),
					'desc'   => esc_html__( 'Enable or disable note', 'yith-am-product-purchase-note' ),
					'fields' => array(
						'class' => 'yampn-purchase-note-onoff',
						'type'  => 'onoff',
						'id'    => '_yith_ampn_onoff_show_note',
						'name'  => '_yith_ampn_onoff_show_note',
						'value' => $product && ! empty( $value_checkbox ) && isset( $value_checkbox ) ? 'yes' : 'no',
						'std'   => 'yes',
					),
				)
			);
			yith_ampn_product_metabox_form_field(
				array(
					'class'  => 'form-field wc_auction_field yith-plugin-ui group-note-purchase',
					'title'  => esc_html__( 'Note', 'yith-am-product-purchase-note' ),
					'desc'   => esc_html__( 'Enter the text note ', 'yith-am-product-purchase-note' ),
					'fields' => array(
						'class' => '',
						'type'  => 'text',
						'value' => $product && ! empty( $value_text_note ) && isset( $value_text_note ) ? $value_text_note : 'Note',
						'id'    => '_yith_ampn_textnote',
						'name'  => '_yith_ampn_textnote',
					),
				)
			);
			yith_ampn_product_metabox_form_field(
				array(

					'class'  => 'form-field wc_auction_field yith-plugin-ui group-note-purchase',
					'title'  => esc_html__( 'Text', 'yith-am-product-purchase-note' ),
					'desc'   => esc_html__( 'Select type', 'yith-am-product-purchase-note' ),
					'fields' => array(
						'id'      => '_yith_ampn_input_type',
						'name'    => '_yith_ampn_input_type',
						'class'   => '',
						'type'    => 'radio',
						'value'   => $product && ! empty( $value_input_type ) && isset( $value_input_type ) ? $value_input_type : 'Text-Option',
						'options' => array(
							'Text-Option'     => 'Text Option',
							'Textarea-Option' => 'Textarea Option',
						),
					),
				)
			);
			yith_ampn_product_metabox_form_field(
				array(
					'class'  => 'form-field wc_auction_field yith-plugin-ui group-note-purchase',
					'title'  => esc_html__( 'Note description', 'yith-am-product-purchase-note' ),
					'desc'   => esc_html__( 'Write the note description ', 'yith-am-product-purchase-note' ),
					'fields' => array(
						'class' => 'textarea-note',
						'type'  => 'textarea',
						'id'    => '_yith_ampn_textarea',
						'name'  => '_yith_ampn_textarea',
						'value' => $product && ! empty( $value_textarea ) && isset( $value_textarea ) ? $value_textarea : '',
					),
				)
			);
			yith_ampn_product_metabox_form_field(
				array(

					'class'  => 'form-field wc_auction_field yith-plugin-ui group-note-purchase',
					'title'  => esc_html__( 'Type price', 'yith-am-product-purchase-note' ),
					'desc'   => esc_html__( 'Select type price', 'yith-am-product-purchase-note' ),
					'fields' => array(
						'id'      => '_yith_ampn_input_type_price',
						'name'    => '_yith_ampn_input_type_price',
						'class'   => '',
						'type'    => 'radio',
						'value'   => $product && ! empty( $value_type_price ) && isset( $value_type_price ) ? $value_type_price : 'Free',
						'options' => array(
							'Free'          => 'Free',
							'Fixed'         => 'Fixed',
							'Per Character' => 'Character',
						),
					),
				)
			);
			yith_ampn_product_metabox_form_field(
				array(
					'class'  => 'form-field wc_auction_field yith-plugin-ui _yith_ampn_text_price group-note-purchase',
					'title'  => esc_html__( 'Price', 'yith-am-product-purchase-note' ),
					'desc'   => esc_html__( 'Write the price', 'yith-am-product-purchase-note' ),
					'fields' => array(
						'class' => '',
						'type'  => 'text',
						'id'    => '_yith_ampn_text_price',
						'name'  => '_yith_ampn_text_price',
						'value' => $product && ! empty( $value_text_price ) && isset( $value_text_price ) ? $value_text_price : '',
					),
				)
			);
			yith_ampn_product_metabox_form_field(
				array(
					'class'  => 'form-field wc_auction_field yith-plugin-ui _yith_ampn_number_free group-note-purchase',
					'title'  => esc_html__( 'Number of free characters', 'yith-am-product-purchase-note' ),
					'desc'   => esc_html__( 'Add the number of free characters', 'yith-am-product-purchase-note' ),
					'fields' => array(
						'class' => '',
						'type'  => 'number',
						'id'    => '_yith_ampn_number_free',
						'name'  => '_yith_ampn_number_free',
						'min'   => 0,
						'step'  => 1,
						'std'   => 0,
						'value' => $product && ! empty( $value_number_free ) && isset( $value_number_free ) ? $value_number_free : 0,
					),
				)
			);
			yith_ampn_product_metabox_form_field(
				array(
					'class'  => 'form-field wc_auction_field yith-plugin-ui',
					'title'  => esc_html__( 'Show Badge', 'yith-am-product-purchase-note' ),
					'desc'   => esc_html__( 'Enable or disable badge', 'yith-am-product-purchase-note' ),
					'fields' => array(
						'class' => 'yampn-purchase-badge-onoff',
						'type'  => 'onoff',
						'id'    => '_yith_ampn_onoff_show_badge',
						'name'  => '_yith_ampn_onoff_show_badge',
						'value' => $product && ! empty( $value_checkbox_badge ) && isset( $value_checkbox_badge ) ? 'yes' : 'no',
						'std'   => 'yes',
					),
				)
			);
			yith_ampn_product_metabox_form_field(
				array(
					'class'  => 'form-field wc_auction_field yith-plugin-ui group-badge-purchase',
					'title'  => esc_html__( 'Text badge', 'yith-am-product-purchase-note' ),
					'desc'   => esc_html__( 'Write the badge text', 'yith-am-product-purchase-note' ),
					'fields' => array(
						'class' => '',
						'type'  => 'text',
						'id'    => '_yith_ampn_text_badge',
						'name'  => '_yith_ampn_text_badge',
						'value' => $product && ! empty( $value_text_badge ) && isset( $value_text_badge ) ? $value_text_badge : '',
					),
				)
			);
			yith_ampn_product_metabox_form_field(
				array(
					'class'  => 'form-field wc_auction_field yith-plugin-ui group-badge-purchase',
					'title'  => esc_html__( 'Background color badge', 'yith-am-product-purchase-note' ),
					'desc'   => esc_html__( 'Select background color', 'yith-am-product-purchase-note' ),
					'fields' => array(
						'class'   => 'select-background-color yith-plugin-fw-colorpicker color-picker',
						'type'    => 'colorpicker',
						'id'      => '_yith_ampn_color_picker_backgroundcolor',
						'name'    => '_yith_ampn_color_picker_backgroundcolor',
						'default' => '#007694',
						'value'   => $product && ! empty( $value_backgroundcolor_badge ) && isset( $value_backgroundcolor_badge ) ? $value_backgroundcolor_badge : '#007694',
					),
				)
			);
			yith_ampn_product_metabox_form_field(
				array(
					'class'  => 'form-field wc_auction_field yith-plugin-ui group-badge-purchase',
					'title'  => esc_html__( 'Text color badge', 'yith-am-product-purchase-note' ),
					'desc'   => esc_html__( 'Select text color', 'yith-am-product-purchase-note' ),
					'fields' => array(
						'class'   => 'select-background-color yith-plugin-fw-colorpicker color-picker',
						'type'    => 'colorpicker',
						'id'      => '_yith_ampn_color_picker_textcolor',
						'name'    => '_yith_ampn_color_picker_textcolor',
						'default' => '#ffffff',
						'value'   => $product && ! empty( $value_textcolor_badge ) && isset( $value_textcolor_badge ) ? $value_textcolor_badge : '#ffffff',
					),
				)
			);
		}

		/**
		 * Yiht_ampn_purchase_note_tab
		 *
		 * @param  mixed $tabs .
		 * @return array
		 */
		public function yiht_ampn_purchase_note_tab( $tabs ) {

			$new_tabs = array(
				'yith_ampn_purchase_note' => array(
					'label'    => esc_html__( 'Purchase Note', 'yith-am-product-purchase-note' ),
					'target'   => 'yith_ampn_purchase_note_settings',
					// 'class'    => array( 'show_if_auction active' ),
					'priority' => 15,
				),
			);
			$tabs     = array_merge( $new_tabs, $tabs );

			return $tabs;
		}

		/**
		 * Add_product_data_panels
		 *
		 * @return void
		 */
		public function yith_ampn_add_product_data_panels() {
			global $post;
			$tabs = array(
				'purchase_note' => 'yith_ampn_purchase_note_settings',
			);
			foreach ( $tabs as $key => $tab_id ) {
				echo "<div id='{$tab_id}' class='panel woocommerce_options_panel'>";
				include YITH_AM_PRODUCT_PURCHASE_NOTE_DIR_TEMPLATES_PATH . '/admin/' . $key . '-tab.php';
				echo '</div>';
			}
		}

		/**
		 * Register_panel
		 *
		 * @return void
		 */
		public function register_panel() {
			if ( ! empty( $this->_panel ) ) {
				return;
			}

			$admin_tabs = array(
				'settings' => __( 'General Settings', 'yith-am-product-purchase-note' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH Purchase Note', // ¡this text MUST be NOT translatable
				'menu_title'         => 'YITH Purchase Note Config', // ¡this text MUST be NOT translatable
				'plugin_description' => __( 'General config of purchase note plugin', 'yith-am-product-purchase-note' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => 'yith-am-product-purchase-note',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => 'yith_test_plugin_panel',
				'admin-tabs'         => $admin_tabs,
				'plugin-url'         => YITH_AM_PRODUCT_PURCHASE_NOTE_DIR_PATH,
				'options-path'       => YITH_AM_PRODUCT_PURCHASE_NOTE_DIR_PATH . 'plugin-options',
			);

			$this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );
		}
		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, YITH_AM_PRODUCT_PURCHASE_NOTE_SLUG );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_AM_PRODUCT_PURCHASE_NOTE_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_AM_PRODUCT_PURCHASE_NOTE_SLUG;
				$row_meta_args['is_premium'] = true;
			}

			return $row_meta_args;
		}


		/**
		 * Save_purchase_note_options
		 *
		 * @param  mixed $post_id .
		 * @return void
		 */
		public function save_purchase_note_options( $post_id ) {

			$field_name_options = array( '_yith_ampn_textnote', '_yith_ampn_input_type', '_yith_ampn_textarea', '_yith_ampn_input_type_price', '_yith_ampn_text_price', '_yith_ampn_text_free', '_yith_ampn_number_free', '_yith_ampn_text_badge', '_yith_ampn_color_picker_backgroundcolor', '_yith_ampn_color_picker_textcolor' );

			foreach ( $field_name_options as $field_options ) {

				if ( ! empty( $_POST[ $field_options ] ) && isset( $_POST[ $field_options ] ) ) { //phpcs:ignore
					$value_field = $_POST[ $field_options ]; //phpcs:ignore
					update_post_meta( $post_id, $field_options, esc_attr( $value_field ) );
				}
			}

			if ( ! empty( $_POST['_yith_ampn_onoff_show_note'] ) ) { // phpcs:ignore
				update_post_meta( $post_id, '_yith_ampn_onoff_show_note', esc_attr( $_POST['_yith_ampn_onoff_show_note'] ) ); //phpcs:ignore
			} else {
				update_post_meta( $post_id, '_yith_ampn_onoff_show_note', '' );
			}

			if ( ! empty( $_POST['_yith_ampn_onoff_show_badge'] ) ) { // phpcs:ignore
				update_post_meta( $post_id, '_yith_ampn_onoff_show_badge', esc_attr( $_POST['_yith_ampn_onoff_show_badge'] ) ); //phpcs:ignore
			} else {
				update_post_meta( $post_id, '_yith_ampn_onoff_show_badge', '' );
			}

		}


		/**
		 * Wcpp_custom_style
		 *
		 * @return void
		 */
		public function purchase_note_options_style_icon() {

			?>
			<style>
				#woocommerce-product-data .inside > .panel-wrap > ul > .yith_ampn_purchase_note_options > a:before { font-family: dashicons; content: '\f119'; }
			</style>
			<?php

		}
	}

}
