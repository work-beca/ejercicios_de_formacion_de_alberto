<?php

/*
Plugin Name: yith-am-testimonials
Plugin URI: https://yith-am-testimonials.es
Description: Plugin that add diferent testimonials
Version: 1.0
Author: Alberto
Author URI: https://alberto.es
License: GPL2
Text Domain: yith-am-testimonials
Domain Path: /languages/
*/

function front_end_scripts_testimonials(){
 
    wp_register_style('style_testimonials', plugin_dir_url(__FILE__)."assets/css/style.css" , false);
    wp_enqueue_style('style_testimonials');

}
add_action('wp_enqueue_scripts', 'front_end_scripts_testimonials');

function admin_style_testimonials() {
   
    wp_register_style('admin-styles', plugin_dir_url(__FILE__)."assets/css/style_admin.css" , false);
    wp_enqueue_style('admin-styles');
   
  }
add_action('admin_enqueue_scripts', 'admin_style_testimonials');



function mw_enqueue_color_picker( $hook_suffix ) {
    // first check that $hook_suffix is appropriate for your admin page
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'my-script-handle', plugins_url('assets/js/my-script.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
}
add_action( 'admin_enqueue_scripts', 'mw_enqueue_color_picker' );




function yith_cpt_testimonials(){


    $labels = array(
        'name' => __('Testimonials', 'yith-am-testimonials'),
        'singular_name' => __('Last testimonials','yith-am-testimonials'),
        'add_new' => __('New testimonial','yith-am-testimonials'),
        'edit_item' => __('Edit testimonial','yith-am-testimonials'),
        'view_item' => __('View testimonial','yith-am-testimonials'), 
        'search_items' => __('Search testimonials','yith-am-testimonials'),
        'not_found' => __('Sorry, no testimonials','yith-am-testimonials'),
        'set_featured_image' => __('Set featured testimonial image','yith-am-testimonials'),
        'remove_featured_image' => __('Remove featured testimonial image','yith-am-testimonials'),
        'use_featured_image' => __('Choose your image to identify yourself', 'yith-am-testimonials')
    );

    $args = array(

        'labels' => $labels,
        'description' => 'Create, edit and show testimonials',
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'menu_icon' => 'dashicons-testimonial',
        'capability_type' => 'post',
        'has_archive' => true,
        'supports' => array('title', 'editor', 'thumbnail')
    );



    register_post_type('yith_testimonial', $args);

   // flush_rewrite_rules();
}
add_action('init', 'yith_cpt_testimonials');


/**
 * Formacion internationalize plugin
 * 
 */

function yith_load_text_domain_ctp_testimonials(){

    load_plugin_textdomain( 'yith-am-testimonials', $deprecated = false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
add_action('plugins_loaded', 'yith_load_text_domain_ctp_testimonials');


/**
 * 
 * Remove media buttons
 * 
 *  
 * */

function check_post_type_and_remove_media_buttons() {
	global $current_screen;
        // use 'post', 'page' or 'custom-post-type-name'
	if( 'yith_testimonial' == $current_screen->post_type ){
        remove_action('media_buttons', 'media_buttons');
    } 
}
add_action('admin_head','check_post_type_and_remove_media_buttons');


/**
 * 
 * Añadiendo shortcode para mostrar los testimonios
 * 
 */

function ready_for_show_testimonials(){
    add_shortcode('yith_show_testimonials', 'yith_show_testimonials_cb');
}
add_action('init', 'ready_for_show_testimonials');


function yith_show_testimonials_cb($atts=[]){


   if(empty($atts['num_posts']) and empty($atts['ids'])){

        $args = array(
            'post_type' => 'yith_testimonial',
            'numberposts' => 6
        );
   }
   
   if (!empty($atts['num_posts']) && !empty($atts['ids'])) {
        $array_ids = explode(",", $atts['ids']);

        $args = array(
            'post_type' => 'yith_testimonial',
            'post__in' => $array_ids
        );
   } 
   
   if(empty($atts['num_posts']) and !empty($atts['ids'])){
        $array_ids = explode(",", $atts['ids']);

        $args = array(
            'post_type' => 'yith_testimonial',
            'post__in' => $array_ids
        );
    }

    if(!empty($atts['num_posts']) and empty($atts['ids'])){

        $num_posts = $atts['num_posts'];
        $args = array(
            'post_type' => 'yith_testimonial',
            'numberposts' => $num_posts
        );
    }


    $posts_ = get_posts($args);

    $content = "";
    $content.="<div class = 'content-testimonials'>";

    
    foreach($posts_ as $post_){

            $vip_post_class = (get_post_meta($post_->ID, 'on_off_testimonials', true) == 1) ? "vip_on" : "vip_off";
            

            if(!empty($atts['hover_effect']) && $atts['hover_effect'] == 'yes'){
                $content.= "<div class = 'testimonials-card hover-effect-testimonials ".$vip_post_class."' >";    
            }else{
                $content.= "<div class = 'testimonials-card ".$vip_post_class."' >";
            }
                
            if(get_post_meta($post_->ID, 'on_off_badge_testimonials', true) == "1"){
                $content.= "<div class = 'title-badge-testimonials'><p class='title_badge title_background'>".get_post_meta($post_->ID, 'title_badge_testimonials', true)."</p></div>";
             echo '<style>
                    .title_badge{
                        
                        background-color:'.get_post_meta($post_->ID, 'background_color_badge', true).';
                        color: white;
                        height: 80%;
                        border-radius: 4px;
                        margin-top: 5px;
                        font-size: 15px;
                    }
                </style>';
            
            }
                
            
            $content.= "<div class= 'group-testimonials'>";
                if((!empty($atts['img']) and $atts['img'] == 'yes') or (empty($atts['img']))){
                    $content.= "<div id='img_testimonials' class= 'img-testimonials'>".get_the_post_thumbnail($post_->ID)."</div>";              
                }
                    $content.= "<div class = 'title-testimonials'>".$post_->post_title."</div>";
                    
                   
                $content.="</div>";
                $content.= "<div class = 'company-testimonials'>" .__('Player of').  "<a target='_blank' class='link-company' href=".get_post_meta($post_->ID, 'url_company_testimonials', true).">".get_post_meta($post_->ID, 'company_testimonials', true)."</a></div>"; 
                $content.= "<div class = 'email-testimonials'>".get_post_meta($post_->ID, 'email_testimonials', true)."</div>"; 
                $star_testimonials = get_post_meta($post_->ID, 'estrellas', true);

                $content .= "<p class='clasificacion'>";
                    $value = 5;
                    for( $i = 0; $i < $value; $i++ ){
                        $checked = $i <= ($star_testimonials -1) ? "checked" : "unchecked";
                        $content.="<input ";
                        $content.= $checked;
                        $content.= " type='radio' name='estrellas' value='".$i."' disabled>";
                        $content.= "<label for='radio".$i."' class='".$checked."'>★</label> ";               
                    }
                $content.="</p>"; 

                $content.= " <div class='testimonial-text'>".$post_->post_content."</div>";
            $content.= "</div>";

    }

    $content.="</div>";

   

return $content;



}


/**
 * 
 * Add metaboxes version 1.1.0
 * 
 * 
 */


function aditional_testimonials_metabox() {
	add_meta_box( 'info-testimonials', 'Información adicional de los testimonios', 'testimonials_meta_box_content', 'yith_testimonial', 'normal', 'high' );
}
add_action( 'add_meta_boxes', 'aditional_testimonials_metabox' );


function testimonials_meta_box_content( $post ){

    $values    = get_post_custom( $post->ID );
    $role      = isset( $values['role_testimonials'] ) ? esc_attr( $values['role_testimonials'][0] ) : '';
    $company    = isset( $values['company_testimonials'] ) ? esc_attr( $values['company_testimonials'][0] ) : '';
    $url_company    = isset( $values['url_company_testimonials'] ) ? esc_attr( $values['url_company_testimonials'][0] ) : '';
    $email_company = isset( $values['email_testimonials'] ) ? esc_attr( $values['email_testimonials'][0] ) : '';
    $star = isset( $values['estrellas'] ) ? esc_attr( $values['estrellas'][0] ) : '';
    $on_off_vip = isset( $values['on_off_testimonials'] ) ? esc_attr( $values['on_off_testimonials'][0] ) : '';
    $on_off_badge = isset( $values['on_off_badge_testimonials'] ) ? esc_attr( $values['on_off_badge_testimonials'][0] ) : '';
    $title_badge = isset( $values['title_badge_testimonials'] ) ? esc_attr( $values['title_badge_testimonials'][0] ) : '';
    $background_color_badge = isset( $values['background_color_badge'] ) ? esc_attr( $values['background_color_badge'][0] ) : '';

    ?>
        <p>
            <label for="role_testimonials">Role: </label>
            <input placeholder="add role" type="text" name="role_testimonials" id="role_testimonials" value="<?php echo esc_html( $role ); ?>" />
        </p>
        <p>
            <label for="company_testimonials">Company: </label>
            <input placeholder="add company" type="text" name="company_testimonials" id="company_testimonials" value="<?php echo esc_html( $company ); ?>" />
        </p>
        <p>
            <label for="url_company_testimonials">URL Company: </label>
            <input placeholder="add url of company" type="url" name="url_company_testimonials" id="url_company_testimonials" value="<?php echo esc_html( $url_company ); ?>" />
        </p>
        <p>
            <label for="email_testimonials">Email Player: </label>
            <input placeholder="add email of player" type="email" name="email_testimonials" id="email_testimonials" value="<?php echo esc_html( $email_company ); ?>" />
        </p>
        <p class="clasificacion">
            <input <?php if($star == "5"){?> checked<?php }?> id="radio1" type="radio" name="estrellas" value="5">
            <label for="radio1">★</label>
            <input <?php if($star == "4"){?> checked<?php }?> id="radio2" type="radio" name="estrellas" value="4">
            <label for="radio2">★</label>
            <input <?php if($star == "3"){?> checked<?php }?> id="radio3" type="radio" name="estrellas" value="3">
            <label for="radio3">★</label>
            <input <?php if($star == "2"){?> checked<?php }?> id="radio4" type="radio" name="estrellas" value="2">
            <label for="radio4">★</label>
            <input <?php if($star == "1"){?> checked<?php }?> id="radio5" type="radio" name="estrellas" value="1">
            <label for="radio5">★</label>
        </p>
        
        <p>
            <!-- Rectangular switch -->
            <p>VIP </p>
            <label class="switch">
                <input <?php if($on_off_vip == "1"){?> checked<?php }?> type="checkbox" id="on_off_testimonials" name="on_off_testimonials" value="1">
                <span class="slider"></span>
            </label>
        </p>

        <p>
            <!-- Rectangular switch -->
            <p> Activate badge </p>
            <label class="switch">
                <input <?php if($on_off_badge == "1"){?> checked<?php }?> type="checkbox" id="on_off_badge_testimonials" name="on_off_badge_testimonials" value="1">          
                <span class="slider"></span>
            </label>
        </p>

        <?php if($on_off_badge == "1"){?>
            <p>
                <label for="title_badge">Title: </label>
                <input type= "text" placeholder= "Add title" name="title_badge_testimonials" id = " title_badge_testimonials " value= "<?php echo esc_html( $title_badge ); ?>" />
            </p>
            <p>
                <label for="title_badge">Background color: </label>
                <input type="text" id="background_color_badge" value="<?php echo esc_html( $background_color_badge ); ?>" name="background_color_badge" class="my-color-field" data-default-color="#13327a"/>
            </p>
            
        <?php } ?>
  
        <?php

        
}


add_action( 'save_post', 'info_testimonials_metabox_save' );
function info_testimonials_metabox_save($post_id){

    //ignoramos los auto guardados
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
        return;
    }

    // Si no está el nonce declarado antes o no podemos verificarlo no seguimos.
    /*if(!isset($_POST['bf_metabox_nonce']) || !wp_verify_nonce($_POST['bf_metabox_nonce'], 'dariobs_metabox_nonce')){
        return;
    }*/

    // Si el usuario actual no puede editar entradas no debería estar aquí.
    if ( ! current_user_can( 'edit_post' ) ) {
        return;
    }

    //**********Save info *********** */

    // Nos aseguramos de que hay información que guardar.
    if ( isset( $_POST['role_testimonials'] ) ) {
        update_post_meta( $post_id, 'role_testimonials', $_POST['role_testimonials'] );
    }
    if ( isset( $_POST['company_testimonials'] ) ) {
        update_post_meta( $post_id, 'company_testimonials', $_POST['company_testimonials'] );
    }
    if ( isset( $_POST['url_company_testimonials'] ) ) {
        update_post_meta( $post_id, 'url_company_testimonials', $_POST['url_company_testimonials'] );
    }
    if ( isset( $_POST['email_testimonials'] ) ) {
        update_post_meta( $post_id, 'email_testimonials', $_POST['email_testimonials'] );
    }
    if ( isset( $_POST['estrellas'] ) ) {
        update_post_meta( $post_id, 'estrellas', $_POST['estrellas'] );
    }
    if ( isset( $_POST['on_off_testimonials'] ) ) {
        update_post_meta( $post_id, 'on_off_testimonials', $_POST['on_off_testimonials'] );
    }else{
        update_post_meta( $post_id, 'on_off_testimonials', 0);
    }
    if ( isset( $_POST['on_off_badge_testimonials'] ) ) {
        update_post_meta( $post_id, 'on_off_badge_testimonials', $_POST['on_off_badge_testimonials'] );
    }else{
        update_post_meta( $post_id, 'on_off_badge_testimonials', 0);
    }
    if ( isset( $_POST['title_badge_testimonials'] ) ) {
        update_post_meta( $post_id, 'title_badge_testimonials', $_POST['title_badge_testimonials'] );
    }
    if ( isset( $_POST['background_color_badge'] ) ) {
        update_post_meta( $post_id, 'background_color_badge', $_POST['background_color_badge'] );
    }
}