jQuery(document).ready(function ($) {

  // Esto es porque el primer event ticket no funciona
  // if (Cookies.get('Quantity') === 1) {
  //   Cookies.set('Quantity', $('.quantity > input').val());
  // }


  // Reestablecemos el valor de Quantity a 1
  // ya que por algun motivo me guardaba el ultimo valor 


  //Añadimos la propiedad de required
  $('.cart .quantity > input').prop('required', true);


  $('.yith-amet-event-ticket-name > strong').text(product_title + ' #1');

  var index = 1;
  var index_title = 2;
  $('.yith_amet_inputs_event > span > input').attr("required", true);
  addNewInputsEventTicket = function () {

    var totalContainer = $('.container-event-ticket');
    var addOnsContainer = totalContainer.find('.inputs-event-ticket:first');

    var newInputsEventTicket = $(addOnsContainer.html().replaceAll('yith-amet-event-ticket[0]', 'yith-amet-event-ticket[' + index + ']'));

    totalContainer.append("<hr/><div class = 'inputs-event-ticket'><span class = 'yith-amet-event-ticket-name'></span></div>");
    $('.inputs-event-ticket:last').append(newInputsEventTicket);
    $('.yith-amet-event-ticket-name > strong:last').replaceWith("<strong>" + product_title + "#" + index_title + "</strong>");
    $('.inputs-event-ticket:last > input').val(index);

    $('.yith_amet_inputs_event > span > input').attr("required", true);

    index++;
    index_title++;
  }



  /** Aqui lo nuevo */

  /** Comprobamos si ya se ha puesto algun event ticket con sus campos y los datos correspondientes */

  let quantity_value = Cookies.get('Quantity');

  if (quantity_value >= 1) {

    for (var i = 0; i < quantity_value - 1; i++) {
      addNewInputsEventTicket();
    }

    $('.quantity >input').val(quantity_value++);


    $('.inputs-event-ticket').each(function () {

      let value_index = $(this).find('input').val();

      $(this).find('p > span > .yith-amet-input-name').val(Cookies.get('name_' + value_index));
      $(this).find('p > span > .yith-amet-input-surname').val(Cookies.get('surname_' + value_index));
    });

  }



  $(document).on('keyup', '.quantity > input', () => {


    var value_previus = parseInt($('.inputs-event-ticket:last > input').val()) + 1;
    var value_quantity = $('.quantity > input').val();


    if (value_quantity > value_previus) {
      let dif_value = value_quantity - value_previus;
      for (var i = 0; i < dif_value; i++) {
        addNewInputsEventTicket();
      }

    } else {

      var num_inputs_event_ticket = $('.inputs-event-ticket').length;
      // Al dismunir la cantidad elminamos un bloque de inputs( name and surname)
      // solo si hay más de un bloque
      if (num_inputs_event_ticket > 1) {
        if (value_quantity === '') {
          value_quantity++;
        }
        for (var i = num_inputs_event_ticket; i > value_quantity; i--) {

          $('.inputs-event-ticket:last').remove();
          index--;
          index_title--;
          $('hr:last').remove();
        }
      }

    }
    Cookies.set('Quantity', $('.quantity > input').val());

  });


  $(document).on('click', '.quantity > input', () => {

    var value_previus = parseInt($('.inputs-event-ticket:last > input').val()) + 1;
    var value_quantity = $('.quantity > input').val();

    if (value_quantity > value_previus) {
      addNewInputsEventTicket();
    } else {

      var num_inputs_event_ticket = $('.inputs-event-ticket').length;
      // Al dismunir la cantidad elminamos un bloque de inputs( name and surname)
      // solo si hay más de un bloque
      if (num_inputs_event_ticket > 1) {

        $('.inputs-event-ticket:last').remove();
        index--;
        index_title--;
        $('hr:last').remove();
      }
    }
    
    Cookies.set('Quantity', $('.quantity > input').val());

  });
  $('body').on('focusout', '.inputs-event-ticket > p > span > input', function () {
    //$(".inputs-event-ticket > p > span > input").focusout(function () {

    let inputs_event_ticket = $(this).parent().parent().parent();
    let label_parent = $(this).parent().parent();
    let text_label = label_parent.find('label').text();

    let input_label = text_label.substr(0, text_label.length - 1).trim();

    let event_ticket_index = inputs_event_ticket.find('input').val();
    let cookie_name = input_label + '_' + event_ticket_index;

    Cookies.set(cookie_name, $(this).val());

  });


});
