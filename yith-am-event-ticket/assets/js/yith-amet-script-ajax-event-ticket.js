jQuery(document).ready(function ($) {

    $('.yith-amet-check-event-ticket-button').on('click', function () {

        //aqui meter pop up

        var opcion = confirm('El ticket cambiará de estado a CHECKED, ¿Quiere continuar?');
        if (opcion == true) {

            let item_html = $(this).parent().parent();

            let post_id_event_ticket_val = item_html.find('.Ticket span .yith-amet-post-id').text();
            //La llamada AJAX
            //console.log(post_id_event_ticket_val);
            $.ajax({

                type: "post",
                url: wp_ajax_tets_vars.ajax_url, // Pon aquí tu URL
                data: {
                    action: "event_ticket_checked_click",
                    post_id_event_ticket: parseInt(post_id_event_ticket_val)
                },
                error: function (response) {
                    console.log(response);
                },
                success: function (response) {
                    // Actualiza el mensaje con la respuesta
                    //console.log('Success mmessage ajax');
                    if (item_html.find('.Check > span').hasClass('dashicons-update-alt')) {
                        item_html.find('.Check > span').removeClass('dashicons-update-alt');
                        item_html.find('.Check > span').addClass('dashicons-thumbs-up');
                    }

                    item_html.find('.Actions > span').remove();
                }
            })

        }

    });

});