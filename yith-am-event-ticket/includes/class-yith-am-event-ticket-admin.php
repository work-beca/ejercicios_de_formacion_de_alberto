<?php
/**
 * File class admin
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_EVENT_TICKET_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_EVENT_TICKET_Admin' ) ) {
	/**
	 * YITH_AM_EVENT_TICKET_Admin
	 */
	class YITH_AM_EVENT_TICKET_Admin {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_EVENT_TICKET_Admin
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_EVENT_TICKET_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yith_am_event_ticket_product_tab' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'yith_amet_style_icon' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'demo_product_tab_product_tab_content' ) );
			add_action( 'woocommerce_admin_process_product_object', array( $this, 'yith_amet_save_options_event_ticket' ) );

			add_action( 'admin_enqueue_scripts', array( $this, 'yith_amet_style_panel_admin' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'yith_amet_script_ajax_check_event_ticket' ) );

			add_action( 'woocommerce_order_status_processing', array( $this, 'yith_amet_event_ticket_create_order_id' ) );
			add_action( 'woocommerce_order_status_completed', array( $this, 'yith_amet_event_ticket_create_order_id' ) );

			// ¡See definition on class-wp-post-list-table.php  manage_{$post_type}_posts_columns;
			add_filter( 'manage_yith_am_event_ticket_posts_columns', array( $this, 'yith_amet_add_post_type_columns' ) );
			// ¡See definition on class-wp-post-list-table.php manage_{$post->post_type}_posts_custom_column
			add_action( 'manage_yith_am_event_ticket_posts_custom_column', array( $this, 'yith_amet_display_post_type_custom_column' ), 10, 2 );

			// ¡Add capabilities to role Administrator
			add_action( 'admin_init', array( $this, 'yith_amet_add_capabilities_event_ticket' ) );

			// ¡Create custom status
			// add_action( 'init', array( $this, 'yith_amet_custom_post_status' ) );

			// ¡Hook para usuarios logueados
			add_action( 'wp_ajax_event_ticket_checked_click', array( $this, 'yith_amet_event_ticket_checked_click' ) );

		}


		/**
		 * Yith_amet_event_ticket_checked_click
		 *
		 * @return void
		 */
		public function yith_amet_event_ticket_checked_click() {

			$post_id = isset( $_POST['post_id_event_ticket'] ) ? $_POST['post_id_event_ticket'] : false; //phpcs:ignore
			if ( $post_id ) {
				if ( 'pending' === get_post_status( $post_id ) ) {
					wp_update_post(
						array(
							'ID'          => $post_id,
							'post_status' => 'publish',
						)
					);
				} else {
					wp_update_post(
						array(
							'ID'          => $post_id,
							'post_status' => 'pending',
						)
					);
				}
			}
		}

		/**
		 * Yith_ampa_script_ajax_check_event_ticket
		 *
		 * @return void
		 */
		public function yith_amet_script_ajax_check_event_ticket() {

			wp_register_script( 'yith-amet-script-ajax-event-ticket', YITH_AM_EVENT_TICKET_DIR_ASSETS_JS_URL . '/yith-amet-script-ajax-event-ticket.js', array( 'jquery' ), '1', true );
			wp_register_script( 'cookie-js-script', YITH_AM_EVENT_TICKET_DIR_ASSETS_JS_URL . '/js.cookie.min.js', array( 'jquery' ), '1', true );
			wp_localize_script(
				'yith-amet-script-ajax-event-ticket',
				'wp_ajax_tets_vars',
				array(
					'ajax_url' => admin_url( 'admin-ajax.php' ),
				)
			);
			wp_enqueue_script( 'yith-amet-script-ajax-event-ticket' );
			wp_enqueue_script( 'cookie-js-script' );
		}
		/**
		 * Yith_amet_custom_post_status
		 *
		 * @return void
		 */
		// public function yith_amet_custom_post_status() {

		// register_post_status(
		// 'yith_amet_pending_status',
		// array(
		// 'label'                     => __( 'Pending Status', 'yith-am-event-ticket' ),
		// 'public'                    => true,
		// 'exclude_from_search'       => false,
		// 'show_in_admin_all_list'    => true,
		// 'show_in_admin_status_list' => true,
		// 'label_count'               => _n_noop( 'Pending Status (%s)', 'Pending Status (%s)', 'yith_am_event_ticket' ), //phpcs:ignore
		// )
		// );
		// register_post_status(
		// 'yith_amet_checked_status',
		// array(
		// 'label'                     => __( 'Checked Status', 'yith-am-event-ticket' ),
		// 'public'                    => true,
		// 'exclude_from_search'       => false,
		// 'show_in_admin_all_list'    => true,
		// 'show_in_admin_status_list' => true,
		// 'label_count'               => _n_noop( 'Checked Status (%s)', 'Checked Status (%s)', 'yith_am_event_ticket' ), //phpcs:ignore
		// )
		// );

		// }

		/**
		 * Yith_amet_add_capabilities_event_ticket
		 *
		 * @return void
		 */
		public function yith_amet_add_capabilities_event_ticket() {

			// ¡Gets the administrator role
			$admin = get_role( 'administrator' );

			if ( $admin ) {

				$event_ticket_caps_admin = array(
					'edit_event_ticket',
					'edit_event_tickets',
					'publish_event_tickets',
					'read_event_ticket',
					'read_private_event_tickets',
					'delete_event_ticket',
					'delete_event_tickets',

				);

				foreach ( $event_ticket_caps_admin as $cap ) {

					if ( ! $admin->has_cap( $cap ) ) {
						$admin->add_cap( $cap );
					}
				}
			}

		}

		/**
		 * Yith_amet_display_post_type_custom_column
		 *
		 * @param  mixed $column_name .
		 * @param  mixed $post_id .
		 * @return void
		 */
		public function yith_amet_display_post_type_custom_column( $column_name, $post_id ) {

			if ( get_post_meta( $post_id, 'yith_amet_event_ticket_order_id' ) ) {
				$order_id = get_post_meta( $post_id, 'yith_amet_event_ticket_order_id' );

				$order             = wc_get_order( $order_id[0] );
				$order_data        = $order->get_data(); // ! The data $order
				$order_data_name   = $order_data['billing']['first_name'];
				$order_data_email  = $order_data['billing']['email'];
				$order_data_status = $order_data['status'];

				$event_ticket_data = get_post( $post_id );

				switch ( $column_name ) {

					case 'Check':
						if ( 'pending' === get_post_status( $post_id ) ) {
							echo '<span class="yith-amet-icon dashicons dashicons-update-alt"></span>';
						}
						if ( 'publish' === get_post_status( $post_id ) ) {
							echo '<span class="yith-amet-icon dashicons dashicons-thumbs-up"></span>';
						}

						break;
					case 'Ticket':
						echo '<span>#<span class="yith-amet-post-id">' . esc_attr( $event_ticket_data->ID ) . '</span> , ' . esc_attr( $event_ticket_data->post_title ) . '</span>';

						break;
					case 'Order':
						echo '<span>#' . esc_attr( $order_id[0] ) . esc_html__( ' by ', 'yith-am-event-ticket' ) . esc_attr( $order_data_name ) . ' ' . esc_attr( $order_data_email ) . '</span>';

						break;
					case 'Purchase_status':
						echo '<span>' . esc_attr__( ucfirst( $order_data_status ), 'yith-am-event-ticket' ) . esc_html__( ' on ', 'yith-am-event-ticket' ) . esc_attr( get_the_date( 'F j, Y', $event_ticket_data->ID ) ) . '</span>'; //phpcs:ignore

						break;

					case 'Actions':
						if ( 'pending' === get_post_status( $post_id ) ) {
							echo '<a id="yith-amet-dashicon-eye" class="dashicons dashicons-visibility" href="' . esc_html( get_edit_post_link( $post_id ) ) . '"></a><span class="dashicons dashicons-thumbs-up yith-amet-check-event-ticket-button"></span>';
						}
						if ( 'publish' === get_post_status( $post_id ) ) {
							echo '<a id="yith-amet-dashicon-eye" class="dashicons dashicons-visibility" href="' . esc_html( get_edit_post_link( $post_id ) ) . '"></a>';
						}

						break;
					default:
						break;
				}
				?>
				<?php
			}
		}

		/**
		 * Yith_amet_add_post_type_columns
		 *
		 * @param array $post_columns .
		 * @return $post_columns .
		 */
		public function yith_amet_add_post_type_columns( $post_columns ) {

			unset(
				$post_columns['title'],
				$post_columns['date']
			);
			$new_columns = apply_filters(
				'yith_amet_custom_columns',
				array(
					'Check'           => '<span class="dashicons dashicons-pets"></span>',
					'Ticket'          => esc_html__( 'Ticket', 'yith-am-event-ticket' ),
					'Order'           => esc_html__( 'Order', 'yith-am-event-ticket' ),
					'Purchase_status' => esc_html__( 'Purchase status', 'yith-am-event-ticket' ),
					'Actions'         => esc_html__( 'Actions', 'yith-am-event-ticket' ),
				)
			);

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
		}

		/**
		 * Yith_amet_event_ticket_order_id_processing
		 *
		 * @param  mixed $order_id comment .
		 * @return void
		 */
		public function yith_amet_event_ticket_create_order_id( $order_id ) {

			// ¡Cambiarlo por get_posts
			global $wpdb;
			$results = $wpdb->get_results( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = 'yith_amet_event_ticket_order_id' AND meta_value =%d ", $order_id ) );

			$post_id_event_ticket_array = isset( $results ) && ! empty( $results ) ? $results : 0;

			$order           = wc_get_order( $order_id );
			$order_data      = $order->get_data(); // ! The data $order
			$order_data_name = $order_data['billing']['first_name'];
			$index           = 0;
			foreach ( $order->get_items() as $item_id => $item ) {

				$product_name = $item['name'];
				$product_id   = $item->get_product_id();
				$product      = wc_get_product( $product_id );

				$meta_data     = $item->get_formatted_meta_data( '_', true );
				$field_name    = '';
				$field_surname = '';

				foreach ( $meta_data as $field ) {

					if ( 'Name' === $field->key ) {
						$field_name = $field->value;
					}
					if ( 'Surname' === $field->key ) {
						$field_surname = $field->value;
					}
				}

				$insert_post_id = isset( $post_id_event_ticket_array[ $index ]->post_id ) ? $post_id_event_ticket_array[ $index ]->post_id : 0;

				if ( 'event_ticket' === $product->get_type() ) {
					$new_post = array(
						'ID'          => $insert_post_id,
						'post_title'  => $product_name,
						'post_status' => 'pending',
						'post_type'   => 'yith_am_event_ticket',
						'meta_input'  => array(
							'yith_amet_event_ticket_order_id' => $order_id,
							'yith_amet_event_ticket_name' => $field_name,
							'yith_amet_event_ticket_surname' => $field_surname,
							'yith_amet_event_ticket_product_id' => $product_id,
							'yith_amet_event_ticket_user_name_order' => $order_data_name,

						),
					);
					if ( ! get_post( $insert_post_id ) || 0 === $insert_post_id ) {
						$post_id_created = wp_insert_post( $new_post );
					}

					$item_data_event_id = wc_get_order_item_meta( $item_id, '_event_id', true );

					if ( empty( $item_data_event_id ) ) {

						wc_add_order_item_meta( $item_id, '_event_id', $post_id_created );
					}

					$index++;
				}
			}

		}


		/**
		 * Yith_ampa_addons_options_style_icon
		 *
		 * @return void
		 */
		public function yith_amet_style_icon() {
			?>
			<style>
				#woocommerce-product-data ul.wc-tabs li.ticket_tab a:before { font-family: dashicons; content: '\f327'; }
			</style>
			<?php
		}

		/**
		 * Yith_am_event_ticket_product_tab
		 *
		 * @param  mixed $tabs comment.
		 * @return $tabs
		 */
		public function yith_am_event_ticket_product_tab( $tabs ) {

			$tabs['Event ticket'] = array(
				'label'  => __( 'Fields Event Ticket', 'yith-am-event-ticket' ),
				'target' => 'event_ticket_product_options',
				'class'  => 'show_if_event_ticket',
			);
			return $tabs;
		}

		/**
		 * Demo_product_tab_product_tab_content
		 *
		 * @return void
		 */
		public function demo_product_tab_product_tab_content() {
			global $post;
			$current_product_id = $post->ID;

			if ( get_post_meta( $current_product_id, 'input_name_event_ticket' ) ) {
				$input_name = get_post_meta( $current_product_id, 'input_name_event_ticket' );
			} else {
				$input_name = 'name';
			}

			if ( get_post_meta( $current_product_id, 'input_surname_event_ticket' ) ) {
				$input_surname = get_post_meta( $current_product_id, 'input_surname_event_ticket' );
			} else {
				$input_surname = 'surname';
			}

			?>
			<div id='event_ticket_product_options' class='panel woocommerce_options_panel marketplace-suggestions-container '>
				<div class=''>
				<table class="widefat yith-amet-table-panel" >
					<tr>
						<th class = 'yith-amet-header-table-panel'>Name</th>
						<th class = 'yith-amet-header-table-panel-required'>Required</th>
					</tr>
					<tr>
						<td>
					<?php
						woocommerce_wp_text_input(
							array(
								'id'          => 'input_name_event_ticket',
								'name'        => 'input_name_event_ticket',
								'label'       => __( 'Input name', 'yith-am-event-ticket' ),
								'class'       => '',
								'placeholder' => '',
								'desc_tip'    => 'false',
								'type'        => 'text',
								'value'       => 'name',
							)
						);
					?>
						</td>
						<td>					
						<?php
						woocommerce_wp_checkbox(
							array(
								'label'             => '',
								'class'             => '',
								'style'             => '',
								'wrapper_class'     => '',
								'value'             => '',
								'id'                => '',
								'name'              => '',
								'cbvalue'           => '',
								'desc_tip'          => '',
								'custom_attributes' => array( 'readonly' => 'readonly' ),
								'description'       => '',
							)
						);
						?>
						</td>
					</tr>

					<tr>
						<td>				
						<?php
						woocommerce_wp_text_input(
							array(
								'id'          => 'input_surname_event_ticket',
								'name'        => 'input_surname_event_ticket',
								'label'       => __( 'Input surname', 'yith-am-event-ticket' ),
								'class'       => '',
								'placeholder' => '',
								'desc_tip'    => 'false',
								'type'        => 'text',
								'value'       => 'surname',
							)
						);
						?>
						</td>
						<td>				
						<?php

						woocommerce_wp_checkbox(
							array(
								'label'             => '',
								'class'             => '',
								'style'             => '',
								'wrapper_class'     => '',
								'value'             => '',
								'id'                => '',
								'name'              => '',
								'cbvalue'           => '',
								'desc_tip'          => '',
								'custom_attributes' => array( 'readonly' => 'readonly' ),
								'description'       => '',
							)
						);
						?>
						</td>
					</tr>
				</table>
			</div>
				<?php
		}

		/**
		 * Yith_amet_style_panel_admin
		 *
		 * @return void
		 */
		public function yith_amet_style_panel_admin() {
			wp_register_style( 'yith-amet-panel-style', YITH_AM_EVENT_TICKET_DIR_ASSETS_CSS_URL . '/yith-amet-panel-style-admin.css', false ); //phpcs:ignore
			wp_enqueue_style( 'yith-amet-panel-style' );
		}

		/**
		 * Yith_amet_save_options_event_ticket
		 *
		 * @param  mixed $product .
		 * @return void
		 */
		public function yith_amet_save_options_event_ticket( $product ) {
			$input_name_event_ticket = wp_unslash( $_POST['input_name_event_ticket'] ); //phpcs:ignore

			if ( ! empty( $input_name_event_ticket ) ) {

				$product->update_meta_data( 'input_name_event_ticket', $input_name_event_ticket );
				$product->save();
			}
		}

	}
}
