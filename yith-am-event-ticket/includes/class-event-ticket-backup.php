<?php
/**
 * File class admin
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_EVENT_TICKET_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'WC_Product_Event_Ticket' ) ) {
	/**
	 * WC_Product_Event_Ticket
	 */
	class WC_Product_Event_Ticket extends WC_Product {
		/**
		 * A static variable
		 *
		 * @static
		 * @var WC_Product_Event_Ticket
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return WC_Product_Event_Ticket
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		public function __construct() {
			$this->product_type = 'Event ticket';
			parent::__construct( $this );

			add_filter( 'product_type_selector', array( $this, 'yith_am_add_event_ticket_product_type' ) );
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yith_am_event_ticket_product_tab' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'demo_product_tab_product_tab_content' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'yith_amet_style_icon' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'yith_amet_style_panel_admin' ) );
		}



		/**
		 * Yith_ampa_addons_options_style_icon
		 *
		 * @return void
		 */
		public function yith_amet_style_icon() {
			?>
			<style>
				#woocommerce-product-data ul.wc-tabs li.ticket_tab a:before { font-family: dashicons; content: '\f327'; }
			</style>
			<?php
		}


		/**
		 * Add_event_ticket_product_type
		 *
		 * @param  mixed $types .
		 * @return $types
		 */
		public function yith_am_add_event_ticket_product_type( $types ) {
			$types['Event ticket'] = __( 'Event ticket', 'yith-am-event-ticket' );
			return $types;
		}


		/**
		 * Yith_am_event_ticket_product_tab
		 *
		 * @param  mixed $tabs comment.
		 * @return $tabs
		 */
		public function yith_am_event_ticket_product_tab( $tabs ) {

			$tabs['Event ticket'] = array(
				'label'  => __( 'Fields Event Ticket', 'yith-am-event-ticket' ),
				'target' => 'event_ticket_product_options',
				'class'  => 'show_if_event_ticket_product',
			);
			return $tabs;
		}


		/**
		 * Demo_product_tab_product_tab_content
		 *
		 * @return void
		 */
		public function demo_product_tab_product_tab_content() {

			?>
			<div id='event_ticket_product_options' class='panel woocommerce_options_panel marketplace-suggestions-container showing-suggestion'>
				<div class='marketplace-suggestion-container-cta'>
				<table class="widefat" >
					<tr>
						<th class = 'yith-amet-header-table-panel'>Name</th>
						<th class = 'yith-amet-header-table-panel'>Placeholder</th>
						<th class = 'yith-amet-header-table-panel'>Type</th>
						<th class = 'yith-amet-header-table-panel'>Required</th>
						<th class = 'yith-amet-header-table-panel'>Actions</th>
					</tr>
					<tr>
						<td>
						<?php
							woocommerce_wp_text_input(
								array(
									'id'          => 'demo_product_info',
									'label'       => '',
									'class'       => 'column-columnname',
									'placeholder' => 'Name',
									'desc_tip'    => 'false',
									'type'        => 'text',
								)
							);
						?>
						</td>
						<td>					
						<?php
							woocommerce_wp_checkbox(
								array(
									'label'             => '', // Text in Label
									'class'             => 'column-columnname',
									'style'             => '',
									'wrapper_class'     => '',
									'value'             => '', // if empty, retrieved from post meta where id is the meta_key
									'id'                => '', // required
									'name'              => '', // name will set from id if empty
									'cbvalue'           => '',
									'desc_tip'          => '',
									'custom_attributes' => '', // array of attributes
									'description'       => '',
								)
							);
						?>
						</td>
						<td>
						<?php
							woocommerce_wp_select(
								array(
									'label'             => '', // Text in Label
									'class'             => '',
									'style'             => '',
									'wrapper_class'     => '',
									'value'             => '', // if empty, retrieved from post meta where id is the meta_key
									'id'                => '', // required
									'name'              => '', // name will set from id if empty
									'options'           => '', // Options for select, array
									'desc_tip'          => '',
									'custom_attributes' => '', // array of attributes
									'description'       => '',
								)
							);
						?>
						</td>
					</tr>

					<tr>
						<td>				
						<?php
							woocommerce_wp_text_input(
								array(
									'id'          => 'demo_product_info',
									'label'       => '',
									'class'       => 'column-columnname',
									'placeholder' => 'Name',
									'desc_tip'    => 'false',
									'type'        => 'text',
								)
							);
						?>
						</td>
						<td>				
						<?php
							woocommerce_wp_checkbox(
								array(
									'label'             => '', // Text in Label
									'class'             => '',
									'style'             => '',
									'wrapper_class'     => '',
									'value'             => '', // if empty, retrieved from post meta where id is the meta_key
									'id'                => '', // required
									'name'              => '', // name will set from id if empty
									'cbvalue'           => '',
									'desc_tip'          => '',
									'custom_attributes' => '', // array of attributes
									'description'       => '',
								)
							);
						?>
						</td>
						<td>
						<?php
							woocommerce_wp_select(
								array(
									'label'             => '', // Text in Label
									'class'             => '',
									'style'             => '',
									'wrapper_class'     => '',
									'value'             => '', // if empty, retrieved from post meta where id is the meta_key
									'id'                => '', // required
									'name'              => '', // name will set from id if empty
									'options'           => '', // Options for select, array
									'desc_tip'          => '',
									'custom_attributes' => '', // array of attributes
									'description'       => '',
								)
							);
						?>
						</td>
					</tr>
				</table>
			</div>
			<?php
		}


		/**
		 * Yith_amet_style_panel_admin
		 *
		 * @return void
		 */
		public function yith_amet_style_panel_admin() {
			wp_register_style( 'yith-amet-panel-style', YITH_AM_EVENT_TICKET_DIR_ASSETS_CSS_URL . '/yith-amet-panel-style-admin.css', false );
			wp_enqueue_style( 'yith-amet-panel-style' );
		}
	}
}
