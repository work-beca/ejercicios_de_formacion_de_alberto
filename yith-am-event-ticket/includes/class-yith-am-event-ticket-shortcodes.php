<?php
/**
 * YITH class shortcodes event ticket
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_EVENT_TICKET_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_EVENT_TICKET_Shortcode' ) ) {
	/**
	 * YITH_AM_EVENT_TICKET_Shortcode
	 */
	class YITH_AM_EVENT_TICKET_Shortcode {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_EVENT_TICKET_Shortcode
		 */
		private static $instance;
		/**
		 * Getinstance
		 *
		 * @return YITH_AM_EVENT_TICKET_Shortcode
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_action( 'init', array( $this, 'yith_amet_ready_for_show_shortcodes' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_amet_frontend_style_search_event_tickets' ) );

		}
		/**
		 * Yith_amet_ready_for_show_shortcodes
		 *
		 * @return void
		 */
		public function yith_amet_ready_for_show_shortcodes() {
			add_shortcode( 'check_in_event', array( $this, 'yith_amet_show_form_users_cb' ) );

		}

		/**
		 * Yith_show_form_users_cb
		 *
		 * @return code
		 */
		public function yith_amet_show_form_users_cb( $atts = array() ) {

			$id_product = $atts['product_id'];

			$args = array(
				'numberposts' => 5,
				'orderby'     => 'date',
				'post_status' => 'any',
				'order'       => 'DESC',
				'meta_key'    => 'yith_amet_event_ticket_product_id', //phpcs:ignore
				'meta_value'  => $id_product, //phpcs:ignore
				'post_type'   => 'yith_am_event_ticket',
			);

			$latest_eventicket = get_posts( $args );

			ob_start();
			yith_am_event_ticket_get_template( '/frontend/tickets.php', $atts, $latest_eventicket );
			return ob_get_clean();

		}


		/**
		 * Yith_amet_frontend_style_search_event_tickets
		 *
		 * @return void
		 */
		public function yith_amet_frontend_style_search_event_tickets() {
            wp_register_style( 'style_frontend_search_event_ticket', YITH_AM_EVENT_TICKET_DIR_ASSETS_CSS_URL . '/yith-amet-style-tickets-pages.css', false ); //phpcs:ignore

		}

	}
}
