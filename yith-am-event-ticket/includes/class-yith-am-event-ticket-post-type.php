<?php
/**
 * File event ticket post type class
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_EVENT_TICKET_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AMTESTIMONIALS_Post_Types' ) ) {

	/**
	 * YITH_AMET_Post_Types
	 */
	class YITH_AMET_Post_Types {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AMET_Post_Types
		 */
		private static $instance;
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AMET_Post_Types
		 */
		public static $post_type = 'yith_am_event_ticket';

		/**
		 * Get_instance
		 *
		 * @return $instance
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_AMET_Post_Types constructor.
		 */
		private function __construct() {

			add_action( 'init', array( $this, 'setup_post_type' ) );
			add_action( 'add_meta_boxes', array( $this, 'adding_custom_meta_boxes' ), 10, 2 );
		}

		/**
		 * Adding_custom_meta_boxes
		 *
		 * @param  mixed $post .
		 * @return void
		 */
		public function adding_custom_meta_boxes( $post_type, $post ) {

			$screens = array( 'yith_am_event_ticket' );
			foreach ( $screens as $screen ) {
				add_meta_box(
					'my-meta-box',
					'Event ' . '#' . $post->ID . ' ' . $post->post_title . ' details',
					'render_my_meta_box',
					$screen
				);
			}
		}


		/**
		 * Setup the custom post type
		 */
		public function setup_post_type() {

			$labels = array(
				'name'                  => __( 'Event ticket', 'yith-am-event-ticket' ),
				'singular_name'         => __( 'Last event tickets', 'yith-am-event-ticket' ),
				'edit_item'             => __( 'Edit event ticket', 'yith-am-event-ticket' ),
				'view_item'             => __( 'View event ticket', 'yith-am-event-ticket' ),
				'search_items'          => __( 'Search event ticket', 'yith-am-event-ticket' ),
				'not_found'             => __( 'Sorry, no event tickets', 'yith-am-event-ticket' ),
				'set_featured_image'    => __( 'Set featured event ticket image', 'yith-am-event-ticket' ),
				'remove_featured_image' => __( 'Remove featured event ticket image', 'yith-am-event-ticket' ),
				'use_featured_image'    => __( 'Choose your image to identify yourself', 'yith-am-event-ticket' ),
			);

			$args = array(

				'labels'           => $labels,
				'description'      => 'Edit and show event tickets',
				'public'           => false,
				'public_queryable' => true,
				'show_ui'          => true,
				'rewrite'          => array( 'slug' => 'yith_am_event_ticket' ),
				'menu_icon'        => 'dashicons-tickets-alt',
				'capability_type'  => 'event_ticket',
				'capabilities'     => array(
					// 'edit_post'          => 'edit_event_ticket',
					// 'edit_posts'         => 'edit_event_tickets',
					// 'edit_others_posts'  => 'edit_other_event_tickets',
					// 'publish_posts'      => 'publish_event_tickets',
					// 'read_post'          => 'read_event_ticket',
					// 'read_private_posts' => 'read_private_event_tickets',
					// 'delete_post'        => 'delete_event_ticket',
					// 'delete_posts'       => 'delete_event_tickets',
					'create_posts' => 'do_not_allow',
				),
				// 'map_meta_cap'     => true,
				'has_archive'      => true,
				'supports'         => array( 'none' ),
			);

			register_post_type( self::$post_type, $args );
		}

	}
}
