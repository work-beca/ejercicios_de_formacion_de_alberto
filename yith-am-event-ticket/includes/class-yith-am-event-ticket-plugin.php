<?php
/**
 * File main class
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_EVENT_TICKET_VERSION' ) ) {
	exit( 'Direct acces forbidden' );
}

if ( ! class_exists( 'YITH_AM_EVENT_TICKET_Plugin' ) ) {

	/**
	 * YITH_AM_EVENT_TICKET_Plugin
	 *
	 * @author Alberto Martin Núñez
	 */
	class YITH_AM_EVENT_TICKET_Plugin {

		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_EVENT_TICKET_Plugin
		 */
		private static $instance;
		/**
		 * Admin
		 *
		 * @var $admin
		 */
		public $admin = null;
		/**
		 * Frontend
		 *
		 * @var $frontend
		 */
		public $frontend = null;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_EVENT_TICKET_Plugin
		 */
		public static function get_instance() {

			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __constructor
		 *
		 * @return void
		 */
		private function __construct() {
			$require = apply_filters(
				'yith_am_product_purchase_note',
				array(
					'common'   => array(
						'includes/class-yith-am-event-ticket-product-type.php',
						'includes/class-yith-am-event-ticket-post-type.php',
						'includes/class-yith-am-event-ticket-shortcodes.php',
						'includes/functions.php',
					),
					'admin'    => array(
						'includes/class-yith-am-event-ticket-admin.php',

					),
					'frontend' => array(
						'includes/class-yith-am-event-ticket-frontend.php',
					),
				)
			);

			$this->require_( $require );
			$this->init_classes();

			/**
			 * Here set any other hoooks(actions o filters you'll use on this class)
			 */
			$this->init();
		}

		/**
		 * Require_
		 *
		 * @param  mixed $main_classes Require diferent sections.
		 * @return void
		 */
		protected function require_( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' === $section && is_admin() ) && file_exists( YITH_AM_EVENT_TICKET_DIR_PATH . $class ) ) {
						require_once YITH_AM_EVENT_TICKET_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {
			YITH_AMET_Product_Type::get_instance();
			YITH_AMET_Post_Types::get_instance();
			YITH_AM_EVENT_TICKET_Shortcode::get_instance();
		}
		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_AM_EVENT_TICKET_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_AM_EVENT_TICKET_Frontend::get_instance();
			}
		}
	}
}

if ( ! function_exists( 'yith_am_event_ticket_plugin' ) ) {
	/**
	 * Yith_am_product_purchase_note_plugin
	 *
	 * @return YITH_AM_EVENT_TICKET_Plugin
	 */
	function yith_am_event_ticket_plugin() {
		return YITH_AM_EVENT_TICKET_Plugin::get_instance();
	}
}
