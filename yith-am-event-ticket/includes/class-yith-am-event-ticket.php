<?php
/**
 *
 * Class Event Ticket
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_EVENT_TICKET_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'WC_Product_Event_Ticket' ) ) {
	/**
	 * WC_Product_Event_Ticket
	 */
	class WC_Product_Event_Ticket extends WC_Product {
		/**
		 * Get_type
		 *
		 * @return string
		 */
		public function get_type() {
			return 'event_ticket';
		}


	}
}
