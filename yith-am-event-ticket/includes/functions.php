<?php
/**
 * YITH functions
 *
 * @package WordPress
 */

if ( ! function_exists( 'yith_am_event_ticket_get_template' ) ) {
	/**
	 * Yith_am_product_purchase_note_get_template
	 *
	 * @param  mixed $file_name comment.
	 * @param  mixed $args comment.
	 * @return void
	 */
	function yith_am_event_ticket_get_template( $file_name, $args = array(), $event_tickets = array() ) {
		$full_path = YITH_AM_EVENT_TICKET_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}


/**
 * Render_my_meta_box
 *
 * @param  mixed $post .
 * @return void
 */
function render_my_meta_box( $post ) {

	$names_ticket = get_post_meta( $post->ID, 'yith_amet_event_ticket_name' );

	$surnames_ticket = get_post_meta( $post->ID, 'yith_amet_event_ticket_surname' );

	echo '<strong>' . esc_html__( 'Field details', 'yith-am-event-ticket' ) . '</strong>';

		echo '
		<p>' . esc_html__( 'Name', 'yith-am-event-ticket' ) . esc_attr( ': ' . $names_ticket[0] ) . '</p>
		<p>' . esc_html__( 'Surname', 'yith-am-event-ticket' ) . esc_attr( ': ' . $surnames_ticket[0] ) . ' </p>
		<hr/>
		';
}


/** CODE for Custom Status */
function my_custom_status_add_in_quick_edit() {
	echo "<script>
    jQuery(document).ready( function() {
        jQuery( 'select[name=\"_status\"]' ).append( '<option value=\"yith_amet_pending_status\">Pending_status</option>' );
        jQuery( 'select[name=\"_status\"]' ).append( '<option value=\"yith_amet_checked_status\">Checked_status</option>' );      
    }); 
    </script>";
}
add_action( 'admin_footer-edit.php', 'my_custom_status_add_in_quick_edit' );
/** CODE for Custom Status */
function my_custom_status_add_in_post_page() {
	echo "<script>
    jQuery(document).ready( function() {        
        jQuery( 'select[name=\"post_status\"]' ).append( '<option value=\"yith_amet_pending_status\">Pending_status</option>' );
        jQuery( 'select[name=\"post_status\"]' ).append( '<option value=\"yith_amet_checked_status\">Checked_status</option>' );
    });
    </script>";
}
add_action( 'admin_footer-post.php', 'my_custom_status_add_in_post_page' );
add_action( 'admin_footer-post-new.php', 'my_custom_status_add_in_post_page' );



/**
 * Yith_amet_custom_post_status
 *
 * @return void
 */
function yith_amet_custom_post_status() {

	register_post_status(
		'yith_amet_pending_status',
		array(
			'label'                     => __( 'Pending Status', 'yith-am-event-ticket' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
		   'label_count'               => _n_noop( 'Pending Status (%s)', 'Pending Status (%s)', 'yith_am_event_ticket' ), //phpcs:ignore
		)
	);
	register_post_status(
		'yith_amet_checked_status',
		array(
			'label'                     => __( 'Checked Status', 'yith-am-event-ticket' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
		   'label_count'               => _n_noop( 'Checked Status (%s)', 'Checked Status (%s)', 'yith_am_event_ticket' ), //phpcs:ignore
		)
	);

}
add_action( 'init', 'yith_amet_custom_post_status' );
