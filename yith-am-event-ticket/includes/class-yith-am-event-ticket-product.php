<?php
/**
 * File class admin
 *
 * @package WordPress
 */

add_action( 'init', 'register_event_ticket_product_type' );
/**
 * Register_demo_product_type
 *
 * @return void
 */
function register_event_ticket_product_type() {

	/**
	 * WC_Product_Demo
	 */
	class WC_Product_Demo extends WC_Product {

		/**
		 * __construct
		 *
		 * @param  mixed $product comment.
		 * @return void
		 */
		public function __construct( $product ) {
			$this->product_type = 'demo';
			parent::__construct( $product );
		}
	}
}


add_filter( 'product_type_selector', 'yith_am_add_event_ticket_product_type' );
/**
 * Add_event_ticket_product_type
 *
 * @param  mixed $types .
 * @return $types
 */
function add_event_ticket_product_type( $types ) {
	$types['Event ticket'] = __( 'Event ticket', 'yith-am-event-ticket' );
	return $types;
}


add_filter( 'woocommerce_product_data_tabs', 'yith_am_event_ticket_product_tab' );
