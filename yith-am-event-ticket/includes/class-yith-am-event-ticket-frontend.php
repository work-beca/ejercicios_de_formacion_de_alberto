<?php
/**
 * File class admin
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_EVENT_TICKET_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_EVENT_TICKET_Frontend' ) ) {
	/**
	 * YITH_AM_EVENT_TICKET_Frontend
	 */
	class YITH_AM_EVENT_TICKET_Frontend {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_EVENT_TICKET_Frontend
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_EVENT_TICKET_Frontend
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_action( 'woocommerce_event_ticket_add_to_cart', array( $this, 'yith_amet_print_add_to_cart_button' ) );
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'yith_amet_add_inputs_event_ticket' ) );
			add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'yith_amet_add_inputs_event_ticket_add_to_cart_validation' ), 10, 4 );

			add_action( 'wp_enqueue_scripts', array( $this, 'yith_amet_script_add_input_event_ticket' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_amet_frontend_style_event_ticket' ) );

			add_filter( 'woocommerce_get_item_data', array( $this, 'yith_amet_get_item_data' ), 10, 2 );
			add_action( 'woocommerce_add_to_cart_handler_event_ticket', array( $this, 'yith_amet_woocommerce_add_to_cart_handler' ) );

			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'yith_amet_checkout_create_order_line_item' ), 10, 4 );

			add_action( 'woocommerce_new_order_item', array( $this, 'yith_amet_create_order_item' ), 20, 3 );

		}

		/**
		 * Yith_amet_create_order_item
		 *
		 * @return void
		 * @param item_id       $item_id comment .
		 * @param values        $values comment .
		 * @param cart_item_key $cart_item_key comment .
		 */
		public function yith_amet_create_order_item( $item_id, $values, $cart_item_key ) {

			$meta_data    = isset( $values->legacy_values ) ? $values->legacy_values : $values;
			$product_type = isset( $meta_data['data'] ) ? $meta_data['data']->get_type() : '';
			if ( 'event_ticket' === $product_type ) {
				wc_add_order_item_meta( $item_id, '_product_type', $product_type );
			}

		}
		/**
		 * Yith_amet_woocommerce_add_to_cart_handler
		 *
		 * @param  mixed $url .
		 * @return bool
		 */
		public function yith_amet_woocommerce_add_to_cart_handler( $url ) {
			if ( isset( $_REQUEST['quantity'] ) ) { //phpcs:ignore
				$quantity =  $_REQUEST['quantity']; //phpcs:ignore
			}
			if ( isset( $_REQUEST['add-to-cart'] ) ) {  //phpcs:ignore
				$product_id = $_REQUEST['add-to-cart']; //phpcs:ignore
			}

			// $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );

			for ( $i = 0; $i < $quantity; $i++ ) {
				$cart_item_data = array(
					'yith_amet_event_ticket_name'    => $_REQUEST['yith-amet-event-ticket-name'][ $i ], //phpcs:ignore
					'yith_amet_event_ticket_surname' => $_REQUEST['yith-amet-event-ticket-surname'][ $i ], //phpcs:ignore
				);

				WC()->cart->add_to_cart( $product_id, 1, 0, array(), $cart_item_data );

			}

			wc_add_to_cart_message( array( $product_id => $quantity ), true );?>

			<?php
			return true;

		}


		/**
		 * Yith_amet_script_add_input_event_ticket
		 *
		 * @return void
		 */
		public function yith_amet_script_add_input_event_ticket() {
			wp_register_script( 'yith-amet-script-event-ticket', YITH_AM_EVENT_TICKET_DIR_ASSETS_JS_URL . '/yith-amet-script-event-ticket.js', array( 'jquery' ), '1', true );
		}


		/**
		 * Yith_mjpa_print_add_to_cart_button
		 *
		 * @return void
		 */
		public function yith_amet_print_add_to_cart_button() {
			wc_get_template( 'single-product/add-to-cart/simple.php' );
		}

		/**
		 * Yith_ampa_add_addons
		 *
		 * @return void
		 */
		public function yith_amet_add_inputs_event_ticket() {

			wp_enqueue_style( 'style_frontend_event_ticket' ); // ¡Incluimos el fichero de estilos
			global $post;
			$current_product_id         = $post->ID;
			$product                    = get_post( $current_product_id );
			$input_name_event_ticket    = get_post_meta( $current_product_id, 'input_name_event_ticket' ) ? get_post_meta( $current_product_id, 'input_name_event_ticket' )[0] : $input_name_event_ticket = 'name';
			$input_surname_event_ticket = get_post_meta( $current_product_id, 'input_surname_event_ticket' ) ? get_post_meta( $current_product_id, 'input_surname_event_ticket' )[0] : $input_surname_event_ticket = 'surname';

			wp_localize_script( 'yith-amet-script-event-ticket', 'product_title', $product->post_title );
			wp_enqueue_script( 'yith-amet-script-event-ticket' );
			?>
			<div class = 'container-event-ticket'>
				<div class = 'inputs-event-ticket'>
				<span class = 'yith-amet-event-ticket-name'><strong></strong></span>
				<input type='hidden' class ='yith-amet-event-ticket-index' name= 'yith-amet-event-ticket[0][index]' value = '0'>
				<?php
				woocommerce_form_field(
					'yith-amet-event-ticket-name[]',
					array(
						'type'        => 'text',
						'class'       => array( 'my-field-class form-row-wide yith_amet_inputs_event' ),
						'input_class' => array( 'yith-amet-input-name' ),
						'label'    => __( $input_name_event_ticket, 'yith-am-event-ticket' ), //phpcs:ignore
						'required'    => true,

					)
				);

				woocommerce_form_field(
					'yith-amet-event-ticket-surname[]',
					array(
						'type'        => 'text',
						'class'       => array( 'my-field-class form-row-wide yith_amet_inputs_event' ),
						'input_class' => array( 'yith-amet-input-surname' ),
						'label'    => __( $input_surname_event_ticket, 'yith-am-event-ticket' ), //phpcs:ignore
						'required'    => true,

					)
				);
				?>
				</div>
			</div>
			<script>
			// Eliminamos el texto "(Opcional)"
			jQuery(document).ready(function($){
				$('.yith_amet_inputs_event > label > .optional').remove();
			});
			</script>
				<?php
		}

		/**
		 * Yith_amet_add_inputs_event_ticket_to_cart_validation
		 *
		 * @param {bool} $passed comment .
		 * @param {int}  $product_id comment .
		 * @param {int}  $quantity comment .
		 * @param {int}  $variation_id comment .
		 * @return $passed
		 */
		public function yith_amet_add_inputs_event_ticket_add_to_cart_validation( $passed, $product_id, $quantity, $variation_id = null ) {

			if ( empty( $quantity ) ) {
				$passed = false;
				wc_add_notice( __( 'Your field is a empty field', 'yith-am-event-ticket' ), 'error' );
			}

			return $passed;

		}

		/**
		 * Yith_amet_get_item_data
		 *
		 * @param  mixed $item_data comment .
		 * @param  mixed $cart_item_data comment .
		 * @return mixed
		 */
		public function yith_amet_get_item_data( $item_data, $cart_item_data ) {

			if ( isset( $cart_item_data['yith_amet_event_ticket_name'] ) && isset( $cart_item_data['yith_amet_event_ticket_surname'] ) ) {
				$event_ticket_inputs_name    = $cart_item_data ['yith_amet_event_ticket_name'];
				$event_ticket_inputs_surname = $cart_item_data ['yith_amet_event_ticket_surname'];

			}

			$item_data[] = array(
				'key'   => __( 'Name', 'yith-am-event-ticket' ),
				'value' => wc_clean( $event_ticket_inputs_name ),
			);

			$item_data[] = array(
				'key'   => __( 'Surname', 'yith-am-event-ticket' ),
				'value' => wc_clean( $event_ticket_inputs_surname ),
			);

			return $item_data;
		}


		/**
		 * Yith_amet_frontend_style_event_ticket
		 *
		 * @return void
		 */
		public function yith_amet_frontend_style_event_ticket() {
			wp_register_style( 'style_frontend_event_ticket', YITH_AM_EVENT_TICKET_DIR_ASSETS_CSS_URL . '/yith-amet-style-frontend-event-ticket.css', false ); //phpcs:ignore
		}


		/**
		 * Yith_amet_checkout_create_order_line_item
		 *
		 * @param  mixed $item comment .
		 * @param  mixed $cart_item_key comment .
		 * @param  mixed $values comment .
		 * @param  mixed $order comment .
		 * @return void
		 */
		public function yith_amet_checkout_create_order_line_item( $item, $cart_item_key, $values, $order ) {

			if ( isset( $values['yith_amet_event_ticket_name'] ) ) {
				$item->update_meta_data(
					__( 'Name', 'yith-am-event-ticket' ),
					$values['yith_amet_event_ticket_name'],
					true
				);
			}
			if ( isset( $values['yith_amet_event_ticket_surname'] ) ) {
				$item->update_meta_data(
					__( 'Surname', 'yith-am-event-ticket' ),
					$values['yith_amet_event_ticket_surname'],
					true
				);
			}
		}
	}
}
