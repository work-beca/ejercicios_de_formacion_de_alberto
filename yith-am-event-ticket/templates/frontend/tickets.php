<?php

/** File tickets template
 *
 * @package WordPress
 */

wp_enqueue_style( 'style_frontend_search_event_ticket' ); // ¡Incluimos el fichero de estilos
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="entry-content"> 
	<div class="yith_wcevti_check_in enhanced" data-product_id="6"> 
		<form name="yith_wcevti_check_in_form" method="post"> 
			<div class="check_in_panel search_panel"> 
				<label for="_search_ticket_number" class="search_panel_label">Check in for YITH Event prueba </label> 
				<input type="text" name="_search_ticket_number" class="search_ticket_number" value="" placeholder="Enter the ticket number here"> 
				<input type="hidden" class="search_ticket_value"> 
				<a href="#" class="myButton"><i class="dashicons dashicons-search button-search"></i></a>
			</div> 
			<div class="check_in_panel dialog_panel" style="position: static; zoom: 1;"> 
				<span class="message">Tickets purchased for "YITH Event prueba" </span> 
				<a href="" class="fa fa-times-circle back_search_button" aria-hidden="true"></a> 
			</div> 
			<div class="check_in_panel list_panel"> 
				<div class="list_header_panel"> 
					<div class="list_colum ticket_status"> 
						<span class="ticket_status_icon_header dashicons dashicons-tickets-alt" title="Ticket Status"></span> 
					</div> 
					<div class="list_colum ticket_info"> 
						<span><b>Ticket info</b></span> 
					</div>
				</div> 
				<div class="list_rows_panel"> 
				<?php
				foreach ( $event_tickets as $ticket ) {
					?>
					<div class="list_row_panel list_row_panel_ticket">
						<div class="row_header">
							<div class="list_colum ticket_status">
								<span class="ticket_status_icon yith-amet-icon dashicons dashicons-update-alt status_pending_check" title="Pending check"></span>
							</div>
							<div class="list_colum ticket_id">
								<span> <b>#<?php echo esc_html( $ticket->ID ); ?></b><small>, by </small><b><?php echo esc_html( get_post_meta( $ticket->ID, 'yith_amet_event_ticket_user_name_order', true ) ); ?></b>  </span>
							</div>
							<div class="list_colum row_actions">
								<span class="item_action dashicons dashicons-arrow-down yith-amet-check-event-ticket-dropdown dropdown-toggle" aria-hidden="true"></span>
								<span class="dashicons dashicons-thumbs-up yith-amet-check-event-ticket-button"></span>
							</div>
						</div>
						<div class="list_row_panel dropdown">
							<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
								<span><strong>Name: </strong><?php echo esc_html( get_post_meta( $ticket->ID, 'yith_amet_event_ticket_name', true ) ); ?></span>
								<span><strong>Surname: </strong><?php echo esc_html( get_post_meta( $ticket->ID, 'yith_amet_event_ticket_surname', true ) ); ?></span>
							</ul>
						</div>
					</div>
					<?php } ?>
				</div>  
			</div>  
		</form> 
	</div> 
	<div class="clear"></div> 
</div>
<script>
	jQuery(document).ready(function ($) {
		$('.dropdown-menu').hide();
		$('body').on('click', '.yith-amet-check-event-ticket-dropdown', function () {
			$dropdown = $(this);
			$(this).closest('.list_row_panel').find('.dropdown-menu').slideToggle("slow", function () {
					if( $dropdown.hasClass('dashicons-arrow-down')){
						$dropdown.removeClass('dashicons-arrow-down') .addClass('dashicons-arrow-up');
					}else{
						$dropdown.removeClass('dashicons-arrow-up') .addClass('dashicons-arrow-down');			
					}
				});
			});


			$(".search_ticket_number").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$(".list_row_panel_ticket").filter(function() {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
  });
	});

	</script>
