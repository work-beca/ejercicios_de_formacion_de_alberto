<?php
/**
 * 
 * CREATING A WIDGET WITH DIFERENT FIELDS
 * 
 */

class Contact_widget extends WP_Widget{

    public function __construct()
    {
        // actual widget
        parent::__construct(
            'data-contact', // Base ID
            'Data Contact' // Name
        );
    }

    public $args = array(
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
        'before_widget' => '<div class="widget-wrap">',
        'after_widget'  => '</div></div>'
    );

    public function widget($args, $instance)
    {
        // outputs the content of the widget

        echo $args['before_widget'];

        /*if(!empty($instance['title'])){
            echo $args['before_title'].apply_filters('widget_title', $instance['title']).$args['after_title'];
        }*/
        
        $name_html = esc_html__($instance['name'], 'text_domain');
        $last_name_html = esc_html__($instance['last_name'], 'text_domain');
        $address_html = esc_html__($instance['address'], 'text_domain');
        $phone_html = esc_html__($instance['phone'], 'text_domain');

        echo '<div class ="textwidget">';

            echo '<p class="title-widgets"> Data Contact: </p>';
            echo '<p> Me llamo <i>'.$name_html. '  '.$last_name_html.'</i></p>';
            echo    '<p> Vivo en <i>'.$address_html.'</i> y mi número de teléfono es </p>'.
                '<p><i>'.$phone_html.'</i></p>';
        echo '</div>';
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        // outputs the options form in the admin

        $name = !empty($instance['name']) ? $instance['name']: esc_html__('', 'text_domain');
        $last_name = !empty($instance['last_name']) ? $instance['last_name'] : esc_html__('','text_domain');
        $address = !empty($instance['address']) ? $instance['address']: esc_html__('', 'text_domain');
        $phone = !empty($instance['phone']) ? $instance['phone']: esc_html__('', 'text_domain');
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('name')); ?>"> <?php echo esc_html__('Name:', 'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'name' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'name' ) ); ?>" type="text" value="<?php echo esc_attr( $name ); ?>" required>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'last_name' ) ); ?>"><?php echo esc_html__( 'Last_name:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'last_name' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'last_name' ) ); ?>" type="text" value="<?php echo esc_attr( $last_name ); ?>" required>
            
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>"><?php echo esc_html__( 'Address:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'address' ) ); ?>" type="text" value="<?php echo esc_attr( $address ); ?>" required>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>"><?php echo esc_html__( 'Phone:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'phone' ) ); ?>" type="tel" value="<?php echo esc_attr( $phone ); ?>" pattern="[0-9]{3}-[0-9]{3}-[0-9]{3}" required placeholder="123-456-789">
        </p>

        <?php
    }

    public function update($new_instance, $old_instance)
    {
        // processes widget options to be saved

        $instance = array();
 
        $instance['name'] = ( !empty( $new_instance['name'] ) ) ? strip_tags( $new_instance['name'] ) : '';
        $instance['last_name'] = ( !empty( $new_instance['last_name'] ) ) ? $new_instance['last_name'] : '';
        $instance['address'] = ( !empty( $new_instance['address'] ) ) ? $new_instance['address'] : '';
        $instance['phone'] = ( !empty( $new_instance['phone'] ) ) ? $new_instance['phone'] : '';

        return $instance;
    }

} // end to class My_Widget

/**
 * Register Contact_widget widget
 */
function yith_register_Contact_widget(){
    register_widget('Contact_widget');
}
add_action('widgets_init', 'yith_register_Contact_widget');





/**
 * 
 * CREATING A WIDGET that list the last x posts
 * 
 */

class Post_widget extends WP_Widget{

    public function __construct()
    {
        // actual widget
        parent::__construct(
            'List-Last-Post', // Base ID
            'List Last Post' // Name
        );
    }

    public $args = array(
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
        'before_widget' => '<div class="widget-wrap">',
        'after_widget'  => '</div></div>'
    );

    public function widget($args, $instance)
    {
        // outputs the content of the widget

        echo $args['before_widget'];

        /*if(!empty($instance['title'])){
            echo $args['before_title'].apply_filters('widget_title', $instance['title']).$args['after_title'];
        }*/
        
        $num_post = esc_html__($instance['num_post'], 'text_domain');
        $posts_recent = wp_get_recent_posts(array('numberposts'=>$num_post));
        
        echo '<div class ="textwidget">';
        echo '<p>'.$instance['title'].': </p>';
        echo '<ul>';
        foreach($posts_recent as $post_){
            echo '<li><a href="'.get_post_permalink($post_['ID']).'">'.$post_['post_title'].'</li>';
        }
    
        echo '</ul></div>';
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        // outputs the options form in the admin

        $title = !empty($instance['title']) ? $instance['title']: esc_html__('', 'text_domain');
        $num_post = !empty($instance['num_post']) ? $instance['num_post']: esc_html__('', 'text_domain');

        ?>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"> <?php echo esc_html__('Title:', 'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('num_post')); ?>"> <?php echo esc_html__('Num_post:', 'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'num_post' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'num_post' ) ); ?>" type="number" min="0" max="5" value="<?php echo esc_attr( $num_post ); ?>">
        </p>          

        <?php
    }

    public function update($new_instance, $old_instance)
    {
        // processes widget options to be saved

        $instance = array();
 
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['num_post'] = ( !empty( $new_instance['num_post'] ) ) ? $new_instance['num_post'] : '';


        return $instance;
    }

} // end to class Post_widget

/**
 * Register Contact_widget widget
 */
function yith_register_Post_widget(){
    register_widget('Post_widget');
}
add_action('widgets_init', 'yith_register_Post_widget');

?>