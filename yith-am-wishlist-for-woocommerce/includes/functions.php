<?php
/**
 * YITH functions
 *
 * @package WordPress
 */

if ( ! function_exists( 'yith_am_wishlist_for_woocommerce_get_template' ) ) {
	/**
	 * Yith_am_wishlist_hide_price_get_template
	 *
	 * @param  mixed $file_name comment.
	 * @param  mixed $args comment.
	 * @return void
	 */
	function yith_am_product_relevance_for_woocommerce_get_template( $file_name, $args = array() ) {
		$full_path = YITH_AM_PRODUCT_RELEVANCE_FOR_WOOCOMMERCE_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

