<?php
/**
 * File class frontend
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_Frontend' ) ) {

	/**
	 * YITH_AM_WISHLIST_FOR_WOOCOMMERCE_Frontend
	 */
	class YITH_AM_WISHLIST_FOR_WOOCOMMERCE_Frontend {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_WISHLIST_FOR_WOOCOMMERCE_Frontend
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_WISHLIST_FOR_WOOCOMMERCE_Frontend
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

		}

	}
}
