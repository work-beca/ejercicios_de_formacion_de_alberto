<?php
/**
 * Plugin Name: yith-am-wishlist-for-woocommerce
 * Plugin URI: https://yith-am-wishlist-for-woocommerce.es
 * Description: The plugin allows to your customers to add products to the wishlist.
 * Version: 1.0.0
 * Author: Alberto Martin
 * Author URI: https://alberto.es
 * License: GPL2
 * Text Domain: yith-am-wishlist-for-woocommerce
 * php version 7.4
 *
 * @category Plugin
 * @package  WordPress
 * @author   Alberto Martin <alberto.mrtn.nu@gmail.com>
 * @license  www.url.es GPL2
 * @link     http://link.com
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_VERSION' ) ) {
	define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_URL' ) ) {
	define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_ASSETS_URL' ) ) {
	define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_ASSETS_URL', YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_ASSETS_CSS_URL', YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_ASSETS_JS_URL', YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_PATH' ) ) {
	define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_INCLUDES_PATH', YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_TEMPLATES_PATH', YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_PATH . 'templates' );
}

if ( ! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_VIEWS_PATH', YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_PATH . '/views' );
}

! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_SLUG' ) && define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_SLUG', 'yith-am-product-message-hide-price' );

! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_FILE' ) && define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_FILE', __FILE__ );

! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_INIT' ) && define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_INIT', plugin_basename( __FILE__ ) );

! defined( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_SECRETKEY' ) && define( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_SECRETKEY', 'zd9egFgFdF1D8Azh2ifK' );
/**
 *
 * Included the scripts
 */


if ( ! function_exists( 'yith_am_wishlist_for_woocommerce_init_classes' ) ) {
	/**
	 * Yith_am_wishlist_for_woocommerce_init_classes
	 *
	 * @return void
	 */
	function yith_am_wishlist_for_woocommerce_init_classes() {

		load_plugin_textdomain( 'yith-am-wishlist-hide-price', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// @require all the files you include on your plugins
		require_once YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_INCLUDES_PATH . '/class-yith-am-product-relevance-for-woocommerce-plugin.php';

		if ( class_exists( 'YITH_AM_WISHLIST_FOR_WOOCOMMERCE_Plugin' ) ) {
			/**
			 * Call the main function
			 */
			yith_am_wishlist_for_woocommerce_plugin();
		}
	}
}

add_action( 'plugins_loaded', 'yith_am_wishlist_for_woocommerce_init_classes' );

if ( ! function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_PATH . 'plugin-fw/init.php' ) ) {
	require_once YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_PATH . 'plugin-fw/init.php';
}
yit_maybe_plugin_fw_loader( YITH_AM_WISHLIST_FOR_WOOCOMMERCE_DIR_PATH );


if ( ! function_exists( 'yith_plugin_registration_hook' ) ) {
	require_once 'plugin-fw/yit-plugin-registration-hook.php';
}

register_activation_hook( __FILE__, 'yith_plugin_registration_hook' );
