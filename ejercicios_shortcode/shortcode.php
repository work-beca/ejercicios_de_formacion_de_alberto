<?php 


/**
 * Creating a shortcode
 * 
 * yith_last_three_post return the last three post published.
 * @return tag three last post
 * 
 */

add_shortcode('yith_last_two_post', 'list_two_post');

function ready_post(){
    function list_two_post(){

        global $wpdb;

        $output = "";
        //wp_get_recent_posts(array('numberposts'=>2))
        $result = $wpdb->get_results("select post_title from `$wpdb->posts` where post_status = 'publish' order by `post_date` desc limit 2");

        foreach($result as $two_last_post){

           
            $title_post_ = $two_last_post->post_title;
            if($title_post_ != ""){
                $output.= '<li>' .$title_post_. '</li>';     
            }
           
        }
	
	    return '<ul>'. $output .'</ul>';
    }
}
add_action('init', 'ready_post');


/**
 * 
 * Creating a shortcode that show details of a user
 * 
 * yith_details_user show details of a user
 * @return tag list of details
 * 
 */

add_shortcode('yith_details_user','yith_handle_details_user');

function ready_init(){
    function yith_handle_details_user( $atts=[] ){

        $user_id = $atts['userid'];        

        $output = "";

        $first_name_user = get_user_meta($user_id, 'first_name');
        $last_name_user = get_user_meta($user_id, 'last_name');
        $nickname_user = get_user_meta($user_id, 'nickname');
        
        $output.= '<p> Details of user: </p>';
        $output.= '<ul><li> Name: '.$first_name_user[0].'</li>';
        $output.= '<li> Last Name: '.$last_name_user[0].'</li>';
        $output.= '<li> Nickname: '.$nickname_user[0].'</li></ul>'; 

        return $output;
    }
}
add_action('init', 'ready_init');


/**
 * 
 * Creating a shortcode that show the content of post
 * 
 * yith_content_post show content of post
 * @return content content of post
 * 
 */

function ready_to_show_content_post(){
    add_shortcode('yith_content_post', 'yith_handler_content_post');
}
add_action('init', 'ready_to_show_content_post');


function yith_handler_content_post($atts = []){

    ob_start();

    $post_id_ = $atts['postid'];
    $post_ = get_post($post_id_);
    
    do_action('yith_before_content_post' );
    echo $post_->post_content;    
    do_action('yith_after_content_post',$post_id_);
    
    return ob_get_clean();
} 


?>