<?php

// Abort if this file is accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

/**
 * TT_Example_List_Table
 */
class TT_Example_List_Table extends WP_List_Table {


	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		// Set parent defaults.
		parent::__construct(
			array(
				'singular' => 'product_message',     // Singular name of the listed records.
				'plural'   => 'products_message',    // Plural name of the listed records.
				'ajax'     => false,       // Does this table support ajax?
			)
		);
	}

	/**
	 * Get_columns
	 *
	 * @return $colums
	 */
	public function get_columns() {
		$columns = array(
			'cb'      => '<input type="checkbox" />',
			'product' => __( 'Product Rule', 'yith-am-product-message-hide-price' ),
			'rule'    => __( 'Apply rule', 'yith-am-product-message-hide-price' ),

		);

		return $columns;
	}
	/**
	 * Get_sortable_columns
	 *
	 * @return array
	 */
	protected function get_sortable_columns() {
		$sortable_columns = array(
			'product' => array( 'product', false ),
			'rule'    => array( 'rule', false ),

		);

		return $sortable_columns;
	}


	/**
	 * Column_default
	 *
	 * @param  mixed $item .
	 * @param  mixed $column_name .
	 * @return message .
	 */
	protected function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			case 'product':
			case 'rule':
				return $item[ $column_name ];
			default:
				return print_r( $item, true ); // Show the whole array for troubleshooting purposes.
		}
	}

	/**
	 * Get value for checkbox column.
	 *
	 * REQUIRED if displaying checkboxes or using bulk actions! The 'cb' column
	 * is given special treatment when columns are processed. It ALWAYS needs to
	 * have it's own method.
	 *
	 * @param object $item A singular item (one full row's worth of data).
	 * @return string Text to be placed inside the column <td>.
	 */
	protected function column_cb( $item ) {

		return sprintf(
			'<input type="checkbox" name="pmhp_ids[]" value ="%s"/>',
			$item['ID']
		);
	}


	/**
	 * Get_bulk_actions
	 *
	 * @return void
	 */
	protected function get_bulk_actions() {

		$actions = array(
			'delete' => __( 'Delete', 'yith-am-product-message-hide-price' ),
		);

		return $actions;
	}

	/**
	 * Handle bulk actions.
	 *
	 * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
	 * For this example package, we will handle it in the class to keep things
	 * clean and organized.
	 *
	 * @see $this->prepare_items()
	 */
	protected function process_bulk_action() {

		global $post;
		if ( 'delete' === $this->current_action() && isset( $_POST['pmhp_ids'] ) ) {
			foreach ( $_POST['pmhp_ids'] as $post_id ) {
				wp_delete_post( $post_id, false );

			}
		}

		// Detect when a bulk action is being triggered.
	}



	/**
	 * Prepare_items
	 *
	 * @return void
	 */
	public function prepare_items() {

		$args                 = array(
			'numberposts'    => -1,
			'post_type'      => 'yith_product_message',
			'posts_per_page' => 5,  // ! modified
		);
		$product_message_cpt  = get_posts( $args );
		$product_message_data = array();

		foreach ( $product_message_cpt as $key => $value ) {
	
			$apply_rule = get_post_meta( $value->ID, '_ypmhp_apply_rule_onoff', true );
			$id_product = get_post_meta( $value->ID, '_ypmhp_product_selected_id', true );

			$product      = wc_get_product( $id_product );
			$product_name = 'No se añadio ningun nombre de producto al crear el mensaje';

			if ( $product instanceof WC_Product ) {
				$product_name = $product->get_name();
			}
			if ( $id_product ) {
				$field = array(
					'type'  => 'onoff',
					'class' => 'yith_am_apply_rule_button',
					'value' => ! empty( $apply_rule ) ? $apply_rule : 'no',

				);

				$product_message_data[ $key ] = array(
					'ID'      => $value->ID,
					'product' => '<a class="row-title" data-value="' . $value->ID . '" href="/wp-admin/post.php?post=' . $value->ID . '&action=edit">' . $product_name . '</a>',
					'rule'    => yith_plugin_fw_get_field( $field, false ),
				);
			}
		}

		$columns  = $this->get_columns();
		$hidden   = array();
		$sortable = $this->get_sortable_columns();

		$this->_column_headers = array( $columns, $hidden, $sortable );

		$this->process_bulk_action();

		/*
		* GET THE DATA!
		*
		*/
		$data = $product_message_data;

		/*
		* REQUIRED. Now we can add our *sorted* data to the items property, where
		* it can be used by the rest of the class.
		*/
		$this->items = $data;

	}

}
