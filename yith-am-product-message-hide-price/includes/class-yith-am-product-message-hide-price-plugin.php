<?php
/**
 * File main class
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_VERSION' ) ) {
	exit( 'Direct acces forbidden' );
}

if ( ! class_exists( 'YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Plugin' ) ) {

	/**
	 * YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Plugin
	 *
	 * @author Alberto Martin Núñez
	 */
	class YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Plugin {

		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Plugin
		 */
		private static $instance;
		/**
		 * Admin
		 *
		 * @var $admin
		 */
		public $admin = null;
		/**
		 * Frontend
		 *
		 * @var $frontend
		 */
		public $frontend = null;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Plugin
		 */
		public static function get_instance() {

			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __constructor
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'plugins_loaded', array( $this, 'plugin_fw_loader' ), 15 );
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );

			$require = apply_filters(
				'yith_am_product_message_hide_price',
				array(
					'common'   => array(
						'includes/functions.php',
						'includes/class-yith-am-product-message-hide-price-post-type.php',
					),
					'admin'    => array(
						'includes/class-yith-am-product-message-hide-price-admin.php',

					),
					'frontend' => array(
						'includes/class-yith-am-product-message-hide-price-frontend.php',
					),
				)
			);

			$this->require_( $require );
			$this->init_classes();

			/**
			 * Here set any other hoooks(actions o filters you'll use on this class)
			 */
			$this->init();
		}

		/**
		 * Require_
		 *
		 * @param  mixed $main_classes Require diferent sections.
		 * @return void
		 */
		protected function require_( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' === $section && is_admin() ) && file_exists( YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_PATH . $class ) ) {
						require_once YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {
			// !init class
			YITH_AM_PRODUCT_MESSAGE_HIDE_Post_Types::get_instance();
		}
		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Frontend::get_instance();
			}
		}

		/**
		 * Plugin_fw_loader
		 *
		 * @return void
		 */
		public function plugin_fw_loader() {
			if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
				global $plugin_fw_data;
				if ( ! empty( $plugin_fw_data ) ) {
					$plugin_fw_file = array_shift( $plugin_fw_data );
					require_once $plugin_fw_file;
				}
			}
		}

		/**
		 * Register plugins for activation tab
		 */
		public function register_plugin_for_activation() {
			if ( function_exists( 'YIT_Plugin_Licence' ) ) {
				YIT_Plugin_Licence()->register( YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_INIT, YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_SECRETKEY, YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_SLUG );
			}
		}

		/**
		 * Register plugins for update tab
		 */
		public function register_plugin_for_updates() {
			if ( function_exists( 'YIT_Upgrade' ) ) {
				YIT_Upgrade()->register( YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_SLUG, YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_INIT );
			}
		}
	}
}

if ( ! function_exists( 'yith_am_product_message_hide_price_plugin' ) ) {
	/**
	 * Yith_am_product_message_hide_price_plugin
	 *
	 * @return YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Plugin
	 */
	function yith_am_product_message_hide_price_plugin() {
		return YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Plugin::get_instance();
	}
}
