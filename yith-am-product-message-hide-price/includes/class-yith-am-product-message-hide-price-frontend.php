<?php
/**
 * File class frontend
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Frontend' ) ) {

	/**
	 * YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Frontend
	 */
	class YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Frontend {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Frontend
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Frontend
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			// ! Ocultamos los precios de la tienda y producto
			add_filter( 'woocommerce_get_price_html', array( $this, 'yith_pmhp_remove_prices' ), 10, 2 );
			add_filter( 'woocommerce_template_single_price', array( $this, 'yith_pmhp_remove_prices' ), 10, 2 );

			add_filter( 'woocommerce_cart_item_subtotal', array( $this, 'filter_woocommerce_cart_item_subtotal_and_cart' ), 10, 3 );
			add_filter( 'woocommerce_cart_item_price', array( $this, 'filter_woocommerce_cart_item_subtotal_and_cart' ), 10, 3 );

			add_action( 'woocommerce_before_add_to_cart_form', array( $this, 'yith_ampmhp_action_woocommerce_add_message_to_product_before_add_to_cart' ), 10, 0 );
			add_action( 'woocommerce_after_add_to_cart_form', array( $this, 'yith_ampmhp_action_woocommerce_add_message_to_product_after_add_to_cart' ), 10, 0 );

			add_action( 'woocommerce_before_single_product_summary', array( $this, 'action_woocommerce_message_to_product' ), 4, 0 );
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_ampmhp_custom_enqueue_js' ) );
		}

		/**
		 * Yith_ampmhp_custom_enqueue_js
		 *
		 * @return void
		 */
		public function yith_ampmhp_custom_enqueue_js() {

			wp_register_style( 'front-end-styles-message', YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_ASSETS_CSS_URL . '/yith-am-front-end-message.css', false ); //phpcs:ignore
			wp_enqueue_style( 'front-end-styles-message' );
		}

		/**
		 * Action_woocommerce_message_to_product
		 *
		 * @return void
		 */
		public function action_woocommerce_message_to_product() {

			global $product;
			/** Se puede solucionar aqui */
			$args                  = array(
				'numberposts' => -1,
				'post_type'   => 'yith_product_message',
			);
			$posts_product_message = get_posts( $args );
			foreach ( $posts_product_message as $cpt ) {

				$apply_rule_onoff = get_post_meta( $cpt->ID, '_ypmhp_apply_rule_onoff', true );
				if ( 'yes' === $apply_rule_onoff ) {
					$value_product_id              = get_post_meta( $cpt->ID, '_ypmhp_product_selected_id', true );
					$value_message                 = '' !== get_post_meta( $cpt->ID, '_ypmhp_product_message', true ) ? get_post_meta( $cpt->ID, '_ypmhp_product_message', true ) : '';
					$value_select_position_message = get_post_meta( $cpt->ID, '_ypmhp_product_selected', true );

					if ( $product->get_id() == $value_product_id ) { //phpcs:ignore
						if ( 'BeforeTitle' === $value_select_position_message ) {

							echo '<spam class ="yith-am-message-before-title">' . esc_html( $value_message ) . '</spam>';
						}
					}
				}
			}

		}

		/**
		 * Yith_ampmhp_action_woocommerce_add_message_to_product_before_add_to_cart
		 *
		 * @return void
		 */
		public function yith_ampmhp_action_woocommerce_add_message_to_product_before_add_to_cart() {

			global $product;
			/** Se puede solucionar aqui */
			$args                  = array(
				'numberposts' => -1,
				'post_type'   => 'yith_product_message',
			);
			$posts_product_message = get_posts( $args );
			foreach ( $posts_product_message as $cpt ) {

				$apply_rule_onoff = get_post_meta( $cpt->ID, '_ypmhp_apply_rule_onoff', true );
				if ( 'yes' === $apply_rule_onoff ) {
					$value_product_id              = get_post_meta( $cpt->ID, '_ypmhp_product_selected_id', true );
					$value_message                 = '' !== get_post_meta( $cpt->ID, '_ypmhp_product_message', true ) ? get_post_meta( $cpt->ID, '_ypmhp_product_message', true ) : '';
					$value_select_position_message = get_post_meta( $cpt->ID, '_ypmhp_product_selected', true );

					if ( $product->get_id() == $value_product_id ) { //phpcs:ignore
						if ( 'BeforeAddToCart' === $value_select_position_message || 'AfterPrice' === $value_select_position_message ) {
							echo '<p><spam>' . esc_html( $value_message ) . '</spam></p>';
						}
					}
				}
			}
		}

		/**
		 * Yith_ampmhp_action_woocommerce_add_message_to_product_before_add_to_cart
		 *
		 * @return void
		 */
		public function yith_ampmhp_action_woocommerce_add_message_to_product_after_add_to_cart() {

			global $product;
			$args                  = array(
				'numberposts' => -1,
				'post_type'   => 'yith_product_message',
			);
			$posts_product_message = get_posts( $args );
			foreach ( $posts_product_message as $cpt ) {

				$apply_rule_onoff = get_post_meta( $cpt->ID, '_ypmhp_apply_rule_onoff', true );
				if ( 'yes' === $apply_rule_onoff ) {
					$value_product_id              = get_post_meta( $cpt->ID, '_ypmhp_product_selected_id', true );
					$value_message                 = '' !== get_post_meta( $cpt->ID, '_ypmhp_product_message', true ) ? get_post_meta( $cpt->ID, '_ypmhp_product_message', true ) : '';
					$value_select_position_message = get_post_meta( $cpt->ID, '_ypmhp_product_selected', true );

					if ( $product->get_id() == $value_product_id ) { //phpcs:ignore
						if ( 'AfterAddToCart' === $value_select_position_message ) {
							echo '</br><p><spam>' . esc_html( $value_message ) . '</spam></p>';
						}
					}
				}
			}
		}

		/**
		 * Filter_woocommerce_cart_item_subtotal_and_cart
		 *
		 * @param  mixed $wc .
		 * @param  mixed $cart_item .
		 * @param  mixed $cart_item_key .
		 * @return $wc
		 */
		public function filter_woocommerce_cart_item_subtotal_and_cart( $wc, $cart_item, $cart_item_key ) {
			$args                  = array(
				'numberposts' => -1,
				'post_type'   => 'yith_product_message',
			);
			$posts_product_message = get_posts( $args );

			if ( count( $posts_product_message ) > 0 ) {
				foreach ( $posts_product_message as $cpt ) {

					$apply_rule_onoff = get_post_meta( $cpt->ID, '_ypmhp_apply_rule_onoff', true );
					if ( 'yes' === $apply_rule_onoff ) {
						$value_product_id   = get_post_meta( $cpt->ID, '_ypmhp_product_selected_id', true );
						$product_hide_onoff = get_post_meta( $cpt->ID, '_ypmhp_product_hide_onoff', true );

						if ( $cart_item['product_id'] == $value_product_id && 'yes' === $product_hide_onoff ) { //phpcs:ignore

							$wc = '';
							return $wc;
						} else {
							return $wc;
						}
					} else {
						return $wc;
					}
				}
			} else {
				return $wc;
			}

		}


		/**
		 * Ayudawp_remove_prices
		 *
		 * @param  mixed $price .
		 * @param  mixed $product .
		 * @return $price .
		 */
		public function yith_pmhp_remove_prices( $price, $product ) {
			$args = array(
				'numberposts' => -1,
				'post_type'   => 'yith_product_message',
			);

			$posts_product_message = get_posts( $args );
			//error_log( print_r( $posts_product_message, true ) );

			foreach ( $posts_product_message as $cpt ) {

				$apply_rule_onoff = get_post_meta( $cpt->ID, '_ypmhp_apply_rule_onoff', true );
				error_log( print_r( $apply_rule_onoff, true ) );
				if ( 'yes' === $apply_rule_onoff ) {
					$value_product_id              = get_post_meta( $cpt->ID, '_ypmhp_product_selected_id', true );
					$product_hide_onoff            = get_post_meta( $cpt->ID, '_ypmhp_product_hide_onoff', true );
					$value_message                 = '' !== get_post_meta( $cpt->ID, '_ypmhp_product_message', true ) ? get_post_meta( $cpt->ID, '_ypmhp_product_message', true ) : '';
					$value_select_position_message = get_post_meta( $cpt->ID, '_ypmhp_product_selected', true );

					if ( 'AfterTitle' === $value_select_position_message || 'BeforePrice' === $value_select_position_message ) {
						echo '<p><spam>' . esc_html( $value_message ) . '</spam></p>'; // ! before price & after title
					}
					error_log( print_r( $value_product_id, true ) );
					error_log( print_r( $apply_rule_onoff, true ) );
					if ( $product->get_id() == $value_product_id && 'yes' === $product_hide_onoff ) {
						error_log( print_r( 'CONCUERDA, EXITO', true ) );
						$price = '';
						return $price;
					} else {
						return $price;
					}
				} else {

					return $price;
				}
			}

		}

	}
}
