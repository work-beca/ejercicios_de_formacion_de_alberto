<?php
/**
 * Class custom post type testimonials
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_PRODUCT_MESSAGE_HIDE_Post_Types' ) ) {

	/**
	 * YITH_AM_PRODUCT_MESSAGE_HIDE_Post_Types
	 */
	class YITH_AM_PRODUCT_MESSAGE_HIDE_Post_Types {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_PRODUCT_MESSAGE_HIDE_Post_Types
		 */
		private static $instance;
		/**
		 * A static variable
		 *
		 * @static
		 * @var $post_type
		 */
		public static $post_type = 'yith_product_message';
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_PRODUCT_ADDONS_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_AM_PRODUCT_MESSAGE_HIDE_Post_Types constructor.
		 */
		private function __construct() {

			add_action( 'init', array( $this, 'setup_post_type' ), 5 );
		}

		/**
		 * Setup the 'Skeleton' custom post type
		 */
		public function setup_post_type() {

			$labels = array(
				'name'                  => __( 'Product Message', 'yith-am-product-message-hide-price' ),
				'singular_name'         => __( 'Last Products', 'yith-am-product-message-hide-price' ),
				'add_new'               => __( 'New Product', 'yith-am-product-message-hide-price' ),
				'edit_item'             => __( 'Edit Product', 'yith-am-product-message-hide-price' ),
				'view_item'             => __( 'View Product', 'yith-am-product-message-hide-price' ),
				'search_items'          => __( 'Search Products', 'yith-am-product-message-hide-price' ),
				'not_found'             => __( 'Sorry, no Products', 'yith-am-product-message-hide-price' ),
				'set_featured_image'    => __( 'Set featured Product image', 'yith-am-product-message-hide-price' ),
				'remove_featured_image' => __( 'Remove featured Product image', 'yith-am-product-message-hide-price' ),
				'use_featured_image'    => __( 'Choose your image to identify yourself', 'yith-am-product-message-hide-price' ),
			);

			$args = array(

				'hierarchical'        => false,

				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'can_export'          => true,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'show_in_rest'        => true,

				'labels'              => $labels,
				'description'         => 'Create, edit and show products',
				'public'              => true,
				'public_queryable'    => true,
				'show_in_menu'        => false, // !param necessary for YITH menu
				'show_ui'             => true,  // !param necessary for YITH menu
				'menu_icon'           => '',
				'capability_type'     => 'post',
				'capabilities'        => array(
					'edit_post'          => 'edit_product',
					'edit_posts'         => 'edit_products',
					'edit_others_posts'  => 'edit_other_products',
					'publish_posts'      => 'publish_products',
					'read_post'          => 'read_product',
					'read_private_posts' => 'read_private_products',
					'delete_post'        => 'delete_product',
					'delete_posts'       => 'delete_products',
				),
				'has_archive'         => true,
				'supports'            => array( '' ),

			);

			register_post_type( self::$post_type, $args );
		}


	}
}
