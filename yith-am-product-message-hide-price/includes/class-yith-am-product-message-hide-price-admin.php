<?php
/**
 * File class admin
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}



if ( ! class_exists( 'YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Admin' ) ) {
	/**
	 * YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Admin
	 */
	class YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Admin {

		/* @var YIT_Plugin_Panel_WooCommerce $_panel the panel */
		private $_panel; //phpcs:ignore



		/* @var Panel page . */
		protected $_panel_page = 'yith-am-product-message-hide-price'; //phpcs:ignore

		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Admin
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_filter( 'plugin_action_links_' . plugin_basename( YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_PATH . '/' . basename( YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );

			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

			add_action( 'wp_ajax_script_ajax_check_apply_rule', array( $this, 'yith_amet_script_ajax_check_apply_rule' ) );
			add_action( 'wp_ajax_nopriv_script_ajax_check_apply_rule', array( $this, 'yith_amet_script_ajax_check_apply_rule' ) );

			add_action( 'yith_test_plugin_print_custom_field', array( $this, 'print_custom_field' ), 10, 1 );

			add_action( 'add_meta_boxes_yith_product_message', array( $this, 'setup_custom_post_type_metaboxes' ) );
			add_action( 'save_post', array( $this, 'save_product_message_cpt' ) );
		}



		/**
		 * Yith_amet_script_ajax_check_apply_rule
		 *
		 * @return void
		 */
		public function yith_amet_script_ajax_check_apply_rule() {

			$post_id = $_POST['id_custom_post_type'];
			if ( ! empty( $post_id ) && 'no' === $_POST['state_on_off_apply_rule'] ) {
				update_post_meta( $post_id, '_ypmhp_apply_rule_onoff', 'yes' );
			}
			if ( ! empty( $post_id ) && 'yes' === $_POST['state_on_off_apply_rule'] ) {
				update_post_meta( $post_id, '_ypmhp_apply_rule_onoff', '' );
			}

		}

		/**
		 * Save_product_message_cpt
		 *
		 * @param  mixed $post_id .
		 * @return $post_id
		 */
		public function save_product_message_cpt( $post_id ) {

			// ! Check permissions
			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return $post_id;
			}

			$defaults = array(
				'numberposts' => -1,
				'meta_key'    => '_ypmhp_product_selected_id',
				'meta_value'  => isset( $_POST['ypmhp_product_selected_id'] ) ? $_POST['ypmhp_product_selected_id'] : '',
				'post_type'   => 'yith_product_message',
			);

			$post = get_post( $post_id );

			$save_fields = array( 'ypmhp_apply_rule_onoff', 'ypmhp_product_hide_onoff', 'ypmhp_product_message', 'ypmhp_product_selected' );

			foreach ( $save_fields as $field ) {
				if ( ! empty( $_POST[$field] ) ) { // phpcs:ignore
					update_post_meta( $post_id, '_' . $field, esc_attr( $_POST[$field] ) ); //phpcs:ignore
				} else {
					update_post_meta( $post_id, '_' . $field, '' );
				}
			}

			if ( ! get_posts( $defaults ) ) {

				if ( isset( $_POST[ 'ypmhp_product_selected_id' ] ) ) { //phpcs:ignore

					update_post_meta( $post_id, '_ypmhp_product_selected_id', esc_attr( $_POST['ypmhp_product_selected_id'] ) );
				}
			}
			return $post_id;
		}
		/**
		 * SetupCustomPostTypeMetaboxes
		 *
		 * @return void
		 */
		public function setup_custom_post_type_metaboxes() {

			add_meta_box(
				'custom_post_type_data_meta_box',
				'Product Message List',
				array( $this, 'custom_post_type_data_meta_box' ),
				'yith_product_message',
				'normal',
				'high'
			);
		}

		/**
		 * Custom_post_type_data_meta_box
		 *
		 * @param  mixed $post .
		 * @return void
		 */
		public function custom_post_type_data_meta_box( $post ) {

			yith_am_product_message_hide_price_get_template( '/product-message-metabox.php' );

		}

		/**
		 * Print_custom_field
		 *
		 * @param  mixed $field .
		 * @return void
		 */
		public function print_custom_field( $field ) {
			wp_enqueue_script( 'yith-ampn-admin-settings-sections' );
			?>
			<script>
				jQuery(document).ready(function ($) {

				var actions = {
						listProducts: function () {			
							$(location).attr('href', '/wp-admin/admin.php?page=yith_test_plugin_panel&tab=list-products');
						}
					//....
				};

				// Delegate click events on the body to those DOM nodes which have a data-action attribute
				$('body').on('click', '[data-action]', function () {
					var action = $(this).data('action');
					if (action in actions) {
						actions[action].apply(this, arguments);
					}
				});
			});
			</script>

			<?php
			$value  = ! ! $field['value'] && is_array( $field['value'] ) ? $field['value'] : array();
			$name   = $field['name'];
			$width  = isset( $value['width'] ) ? $value['width'] : '';
			$height = isset( $value['height'] ) ? $value['height'] : '';

			$html  = "<div class='yith-test-plugin-custom-field-container' >";
			$html .= "<p class='submit' style='float: left;margin: 0 10px 0 -10px;'>";
			$html .= "<input class='yith-ampmhp-button-back-to-product-list button-secondary' type='button' value='BACK TO PRODUCT LIST' data-action='listProducts' />";
			$html .= '</p>';
			$html .= '</div>';
			echo $html;

		}


		/**
		 * Enqueue_scripts
		 *
		 * @return void
		 */
		public function enqueue_scripts() {

			$settings_section_dependencies = array( 'jquery', 'yith-plugin-fw-fields' );

			wp_register_style( 'admin-styles-tab-add-product-menu-yith', YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_ASSETS_CSS_URL . '/yith-am-style-tab-add-products-menu-yith.css', false ); //phpcs:ignore
			wp_enqueue_style( 'admin-styles-tab-add-product-menu-yith' );

			wp_register_style( 'yith-ampn-admin-css-metabox', YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_ASSETS_CSS_URL . '/yith-am-admin-product-message-metabox.css', array( 'yith-plugin-fw-fields' ), YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_VERSION );

			wp_register_script( 'yith-ampn-admin-settings-sections', YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_ASSETS_URL . '/js/yith-am-admin-product-message-metabox.js', $settings_section_dependencies, YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_VERSION, true );
			wp_enqueue_script( 'yith-ampn-admin-settings-sections' );

			wp_register_script( 'yith-ampn-admin-settings-sections', YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_ASSETS_URL . '/js/yith-am-admin-tab-add-products.js', YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_VERSION, true ); //phpcs:ignore

			wp_register_script( 'yith-amet-script-ajax-apply-rule', YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_ASSETS_JS_URL . '/yith-am-admin-ajax-apply-rule-cpt-script.js', array( 'jquery' ), '1', true );
			wp_localize_script(
				'yith-amet-script-ajax-apply-rule',
				'wp_ajax_vars',
				array(
					'ajax_url' => admin_url( 'admin-ajax.php' ),
				)
			);
			wp_enqueue_script( 'yith-amet-script-ajax-apply-rule' );
		}


		/**
		 * Register_panel
		 *
		 * @return void
		 */
		public function register_panel() {
			if ( ! empty( $this->_panel ) ) {
				return;
			}

			$admin_tabs = array(
				'list-products' => __( 'Product Message List', 'yith-am-product-message-hide-price' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH Product Message', // !this text MUST be NOT translatable
				'menu_title'         => 'YITH Product Message', // !this text MUST be NOT translatable
				'plugin_description' => __( 'This plugin will allow an administrator to add messages on the product page and hide the price if desired.', 'yith-am-product-message-hide-price' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => 'yith-am-product-message-hide-price',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => 'yith_test_plugin_panel',
				'admin-tabs'         => $admin_tabs,
				'plugin-url'         => YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_PATH,
				'options-path'       => YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_PATH . 'plugin-options',
			);

			$this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );
		}
		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->_panel_page, true, YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_SLUG );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_SLUG;
				$row_meta_args['is_premium'] = true;
			}

			return $row_meta_args;
		}

	}

}
