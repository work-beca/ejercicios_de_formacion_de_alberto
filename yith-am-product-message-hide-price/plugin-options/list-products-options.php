<?php
/**
 * File class frontend
 *
 * @package WordPress
 */



$settings = array(
	'list-products' => array(
		'general-options'       => array(
			'type' => 'title',
			'desc' => '',
		),
		'list_products_message' => array(
			'type'                 => 'yith-field',
			'yith-type'            => 'list-table',
			'post_type'            => 'yith_product_message',
			'list_table_class'     => 'TT_Example_List_Table',
			'list_table_class_dir' => YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_INCLUDES_PATH . '/class-yith-am-list-table-product-message-hide-price.php',
			'title'                => 'Product Message List',
			'add_new_button'       => 'new products',
			'id'                   => 'yampmhp_list_table',
		),

		'general-options-end'   => array(
			'type' => 'sectionend',
			'id'   => 'yith-pmhp-general-options',
		),

	),

);

return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
