<?php
$settings = array(
	'settings' => array(
		'general-options'              => array(
			'title' => __( 'General Options', 'yith-test-plugin' ),
			'type'  => 'title',
			'desc'  => '',
		),
		'yith-test-plugin-text'        => array(
			'id'        => 'yith-test-plugin-text',
			'name'      => __( 'Text', 'yith-test-plugin' ),
			'type'      => 'yith-field',
			'yith-type' => 'text',
			'default'   => 'Default Text',
			'desc'      => __( 'This is a text field', 'yith-test-plugin' ),
		),
		'yith-test-plugin-colorpicker' => array(
			'id'        => 'yith-test-plugin-colorpicker',
			'name'      => __( 'Color Picker', 'yith-test-plugin' ),
			'type'      => 'yith-field',
			'yith-type' => 'colorpicker',
			'default'   => '#000000',
			'desc'      => __( 'This is a color-picker field', 'yith-test-plugin' ),
		),
		'general-options-end'          => array(
			'type' => 'sectionend',
			'id'   => 'yith-wcbk-general-options',
		),

		'section-2-options'            => array(
			'title' => __( 'Section 2', 'yith-test-plugin' ),
			'type'  => 'title',
		),

		// SECTION 2 Options...

		'section-2-options-end'        => array(
			'type' => 'sectionend',
		),
	),
);

return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
