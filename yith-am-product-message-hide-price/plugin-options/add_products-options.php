<?php


$settings = array(
	'add_products' => array(
		'Adding products'         => array(
			'title' => __( 'Adding products', 'yith-am-product-message-hide-price' ),
			'type'  => 'title',
			'desc'  => '',
		),
		'apply-rule'              => array(
			'id'        => 'yith-ampmhp-apply-rule',
			'name'      => __( 'Apply this rule', 'yith-am-product-message-hide-price' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'std'       => 'no',
			'value'     => 'no',
			'desc'      => __( 'Check to apply this rule', 'yith-am-product-message-hide-price' ),
		),
		'select-products'         => array( // !without variations
			'id'        => 'yith-ampmhp-select-products',
			'name'      => __( 'Select products', 'yith-am-product-message-hide-price' ),
			'type'      => 'yith-field',
			'yith-type' => 'ajax-products',
			'desc'      => __( 'Search products', 'yith-am-product-message-hide-price' ),
		),

		'message'                 => array(
			'id'        => 'yith-ampmhp-message',
			'name'      => __( 'Message', 'yith-am-product-message-hide-price' ),
			'type'      => 'yith-field',
			'yith-type' => 'textarea-editor',
			'desc'      => __( 'Write the product message', 'yith-am-product-message-hide-price' ),
		),

		'select-display'          => array( // !without variations
			'id'        => 'yith-ampmhp-select-display',
			'name'      => __( 'Where to display the message on product page', 'yith-am-product-message-hide-price' ),
			'type'      => 'yith-field',
			'yith-type' => 'select',
			'desc'      => __( 'Select display', 'yith-am-product-message-hide-price' ),
			'options'   => array(
				'BeforeAddToCart' => 'Before add to cart', // !Debería de hacerse por defecto pero no funciona
				'AfterAddToCart'  => 'After add to cart',
				'BeforeTitle'     => 'Before title',
				'AfterTitle'      => 'After title',
				'BeforePrice'     => 'Before price',
				'AfterPrice'      => 'After price',

			),
		),
		'hide-product-price'      => array(
			'id'        => 'yith-ampmhp-hide-product-price',
			'name'      => __( 'Hide product price', 'yith-am-product-message-hide-price' ),
			'type'      => 'yith-field',
			'yith-type' => 'onoff',
			'std'       => 'no',
			'value'     => 'no',
			'desc'      => __( 'Check to hide product price', 'yith-am-product-message-hide-price' ),
		),

		'yith-test-plugin-custom' => array(
			'id'        => 'yith-test-plugin-custom',
			//'name'      => __( 'Custom Field', 'yith-test-plugin' ),
			'type'      => 'yith-field',
			'yith-type' => 'custom',
			'action'    => 'yith_test_plugin_print_custom_field',
		),
		'general-options-end'     => array(
			'type' => 'sectionend',
			'id'   => 'yith-wcbk-general-options',
		),

	),
);


return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
