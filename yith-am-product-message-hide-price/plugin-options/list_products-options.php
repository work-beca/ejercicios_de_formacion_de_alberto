<?php
$settings = array(
	'list_products' => array(
		'general-options'       => array(
			// 'title' => __( 'General Options', 'yith-test-plugin' ),
			'type' => 'title',
			'desc' => '',
		),
		'list_products_message' => array(
			'type'                 => 'yith-field',
			'yith-type'            => 'list-table',
			'post_type'            => 'yith_product_message',
			'list_table_class'     => 'TT_Example_List_Table',
			'list_table_class_dir' => YITH_AM_PRODUCT_MESSAGE_HIDE_PRICE_DIR_INCLUDES_PATH . '/class-yith-am-list-table-product-message-hide-price.php',
			'title'                => 'Product Message List',
			'add_new_button'       => 'new products',
			'id'                   => 'yampmhp_list_table',
		),


		// 'yith-test-plugin-custom' => array(
		// 'id'        => 'yith-test-plugin-custom',
		// 'name'      => __( 'Custom Field', 'yith-test-plugin' ),
		// 'type'      => 'yith-field',
		// 'yith-type' => 'custom',
		// 'action'    => 'yith_boton_save_list_table_custom',
		// ),


		'general-options-end'   => array(
			'type' => 'sectionend',
			'id'   => 'yith-wcbk-general-options',
		),

	),

);

		return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
