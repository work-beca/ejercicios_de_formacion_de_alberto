<?php
/**
 * File class frontend
 *
 * @package WordPress
 */

wp_enqueue_style( 'yith-ampn-admin-css-metabox' );



$screen = get_current_screen();

if ( 'yith_product_message' === $screen->id ) {
	global $post;

	$post_id_current = $post->ID;


	$ypmhp_apply_rule_onoff    = get_post_meta( $post_id_current, '_ypmhp_apply_rule_onoff', true );
	$ypmhp_product_selected_id = get_post_meta( $post_id_current, '_ypmhp_product_selected_id', true );
	$ypmhp_product_message     = get_post_meta( $post_id_current, '_ypmhp_product_message', true );
	$ypmhp_product_selected    = get_post_meta( $post_id_current, '_ypmhp_product_selected', true );
	$ypmhp_product_hide_onoff  = get_post_meta( $post_id_current, '_ypmhp_product_hide_onoff', true );

	yith_pmhp_product_metabox_form_field(
		array(
			'class'  => 'form-field wc_auction_field yith-plugin-ui',
			'desc'   => __( 'Check to apply this rule', 'yith-am-product-message-hide-price' ),
			'title'  => esc_html__( 'Apply this rule', 'yith-am-product-message-hide-price' ),
			'fields' =>
				array(
					'class'   => 'ypmhp-product-metabox-onoff',
					'type'    => 'onoff',
					'value'   => ! empty( $ypmhp_apply_rule_onoff ) ? $ypmhp_apply_rule_onoff : 'no',
					'id'      => 'yith-ampmhp-apply-rule',
					'name'    => 'ypmhp_apply_rule_onoff',
					'default' => 'no',

				),

		)
	);

	yith_pmhp_product_metabox_form_field(
		array(
			'class'  => 'form-field wc_auction_field yith-plugin-ui',
			'desc'   => __( 'Search products', 'yith-am-product-message-hide-price' ),
			'title'  => esc_html__( 'Select products', 'yith-am-product-message-hide-price' ),
			'fields' =>
				array(
					'type'  => 'ajax-products',
					'id'    => 'yith-ampmhp-select-products',
					'name'  => 'ypmhp_product_selected_id',
					'value' => ! empty( $ypmhp_product_selected_id ) ? $ypmhp_product_selected_id : '',

				),

		)
	);

	yith_pmhp_product_metabox_form_field(
		array(
			'class'  => 'form-field wc_auction_field yith-plugin-ui',
			'desc'   => __( 'Write the product message', 'yith-am-product-message-hide-price' ),
			'title'  => esc_html__( 'Message', 'yith-am-product-message-hide-price' ),
			'fields' =>
			array(
				'id'    => 'yith-ampmhp-message',
				'name'  => 'ypmhp_product_message',
				'type'  => 'textarea-editor',
				'value' => ! empty( $ypmhp_product_message ) ? $ypmhp_product_message : '',

			),
		)
	);

	yith_pmhp_product_metabox_form_field(
		array(
			'class'  => 'form-field wc_auction_field yith-plugin-ui',
			'desc'   => __( 'Select display', 'yith-am-product-message-hide-price' ),
			'title'  => esc_html__( 'Where to display the message on product page', 'yith-am-product-message-hide-price' ),
			'fields' =>
			array(
				'id'      => 'yith-ampmhp-select-display',
				'name'    => 'ypmhp_product_selected',
				'type'    => 'select',
				'options' => array(
					'BeforeAddToCart' => __( 'Before add to cart', 'yith-am-product-message-hide-price' ), // !Debería de hacerse por defecto pero no funciona
					'AfterAddToCart'  => __( 'After add to cart', 'yith-am-product-message-hide-price' ),
					'BeforeTitle'     => __( 'Before title', 'yith-am-product-message-hide-price' ),
					'AfterTitle'      => __( 'After title', 'yith-am-product-message-hide-price' ),
					'BeforePrice'     => __( 'Before price', 'yith-am-product-message-hide-price' ),
					'AfterPrice'      => __( 'After price', 'yith-am-product-message-hide-price' ),

				),
				'value'   => ! empty( $ypmhp_product_selected ) ? $ypmhp_product_selected : 'BeforeAddToCart',
			),
		)
	);

	yith_pmhp_product_metabox_form_field(
		array(
			'class'  => 'form-field wc_auction_field yith-plugin-ui',
			'desc'   => __( 'Hide product price', 'yith-am-product-message-hide-price' ),
			'title'  => esc_html__( 'Hide product price', 'yith-am-product-message-hide-price' ),
			'fields' =>
				array(
					'class'   => 'ypmhp-product-metabox-onoff',
					'type'    => 'onoff',
					'value'   => ! empty( $ypmhp_product_hide_onoff ) ? $ypmhp_product_hide_onoff : 'no',
					'id'      => 'ypmhp_product_hide_onoff',
					'name'    => 'ypmhp_product_hide_onoff',
					'default' => 'no',

				),

		)
	);

	yith_pmhp_product_metabox_form_field(
		array(
			'class'  => 'form-field wc_auction_field yith-plugin-ui',
			'desc'   => ' ',
			'fields' =>
				array(
					'id'     => 'yith-test-plugin-custom',
					'type'   => 'custom',
					'action' => 'yith_test_plugin_print_custom_field',

				),

		)
	);
}
