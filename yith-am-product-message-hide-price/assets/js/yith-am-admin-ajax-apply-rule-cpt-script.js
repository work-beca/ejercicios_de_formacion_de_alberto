jQuery(document).ready(function ($) {

    $('.yith_am_apply_rule_button').on('click', function () {

        let item_html = $(this).parent().parent().parent().find('.column-product > a').data("value");
        console.log(item_html);
 
        //La llamada AJAX
        $.ajax({

            type: "post",
            url: wp_ajax_vars.ajax_url, // Pon aquí tu URL
            data: {
                action: "script_ajax_check_apply_rule",
                state_on_off_apply_rule: $(this).parent().find('input').val(),
                id_custom_post_type : item_html
            },
            error: function (response) {
                console.log(response);
            },
            success: function (response) {
                // Actualiza el mensaje con la respuesta
                console.log('Success mmessage ajax');

            }
        })

    });

});