<?php
	/*
	 * 
	 *Añadiendo la funcionalidad de agregar texto al final de cada post
 	*/

function yith_insert_text_end_post($content){
	if(!is_feed() && !is_home()){
		$content.="<div class= 'text-end'>";
		$content.= "<h4>Este post ha sido escrito por Alberto</h4>";
		$content.= "</div>";

		return $content;
	}
}	 
add_filter('the_content', 'yith_insert_text_end_post');

/*******************************************************************************************/
/**
	 * Añadiendo la funcionalidad de agregrar el author al final de cada post
	 */

function yith_insert_author_end_post($content){
	
	if(!is_feed() && !is_home()){
		$author = get_the_author_meta('nickname');
		$content.= "<h4>Este post ha sido escrito por ".$author." </h4>";
		
		return $content;
	}
}	 
add_filter('the_content', 'yith_insert_author_end_post');
/*******************************************************************************************/


/**
 * Añadiendo funcionalidad de intercambiar nombre de autor de post
 */
function yith_return_name_swap($id_author){

	$name_authors = array( 1 => 'admin', 2 => 'alberto' );

	return $name_authors[$id_author];

}
 function yith_swap_name_post($content){

		if(!is_feed() && !is_home()){
			global $post;
			$postID = $post->ID;
			

			if($postID == 5){
				$content.= "<h4>Este post ha sido escrito por ".return_name_swap(2)."</h4>";	
			}elseif($postID == 11){
				$content.= "<h4>Este post ha sido escrito por ".return_name_swap(1)."</h4>";
			}
			
			return $content;
		}
	}	
add_filter('the_content', 'yith_swap_name_post');


/***************************************************************************** */

/**
 * Añadiendo funcionalidad - Creando nuevos campos metadatos
 */
function yith_add_fields(){

	$post_id = 11;
	$meta_key = array('name_mom', 'name_dad');
	$meta_value = array('pepa', 'jose');

	for($i = 0; $i < 2; $i++){
		update_post_meta($post_id, $meta_key[$i], $meta_value[$i], false);
	}
	
}

function yith_add_fields_post($post_id){

	yith_add_fields();

}
add_action('save_post','yith_add_fields_post',10,1);

/**
 * Añadiendo funcionalidad obteniendo metadatos
 */

function yith_show_fields_post($content){

	if(!is_feed() && !is_home()){

		$edad_author = get_post_meta(11, 'Edad');
		$mom_author = get_post_meta(11, 'name_mom');
		$dad_author = get_post_meta(11, 'name_dad');
		error_log(print_r($edad_author));
		$content.= "<h4>La edad del autor es de ".$edad_author[0]." años</h4>";	
		$content.= "<h4>Su madre se llama ".$mom_author[0]." </h4>";	
		$content.= "<h4>Su padre se llama ".$dad_author[0]." </h4>";	
		
		
	}
	return $content;
}
add_filter('the_content', 'yith_show_fields_post');

/********************************************************************************** */

/**
 * Añadiendo funcionalidad modificando titulo
 */
function change_title_filter($title, $id){

	$post_ = get_post($id);
	$author_id = $post_->post_author;
	//error_log(print_r($author_id, true));	
	$name_author = get_the_author_meta('nickname', $author_id);
	
	$title.= " escrito por ".$name_author ;

	return $title;
}
add_filter('the_title', 'change_title_filter', 10, 2);

/******************************************************************************************************** */
/**
 * Añadiendo funcionalidad - Añadiendo fecha del post
 */

function add_date_post($content){

	if(!is_feed() && !is_home()){
		global $post;
		$date_post =  $post->post_date;
	

		$content.= "<h4>La fecha de publicacion del post es ".$date_post."</h4> ";
		return $content;
	}


}
add_filter('the_content', 'add_date_post');

/******************************************************************************************************** */
/**
 * Añadiendo funcionalidad - Eliminando title_post
 */

function change_title_filter_to_empty($title, $id){

	global $post;

	$title_real = $post->post_title;

	return $title_real;
 }
add_filter('the_title', 'change_title_filter_to_empty', 20, 2);

?>