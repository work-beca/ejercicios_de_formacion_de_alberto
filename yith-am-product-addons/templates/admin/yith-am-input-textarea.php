<?php

$addon_name           = $args['name'];
$input_textarea_value = empty( $addon['description'] ) ? $args['value'] : $addon['description'];

if ( $addon ) {
	$addon_index = $addon['index'];
	$addon_name  = 'yith-ampa-addon[' . $addon['index'] . '][description]';
}
?>

<hr>
<p class='form-field <?php echo esc_html__( $args['div_class'] ); ?>'>
		<label> <?php echo esc_html__( $args['label'], 'yith-am-product-purchase-note' ); ?></label>
		<textarea  
			rows='3'
			name ='<?php echo esc_html( $addon_name ); ?>'
			class ='<?php echo esc_html( $args['class'] ); ?>'
			placeholder = '<?php echo esc_html( $args['placeholder'] ); ?>'	
		><?php echo esc_html( $input_textarea_value ); ?></textarea>
</p>
