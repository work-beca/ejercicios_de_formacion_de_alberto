<?php
/** File options
 *
 * @author Alberto Martín Núñez
 * @package WordPress
 */

$addon_name_text    = 'yith-ampa-addon[{{INDEX}}][inputs][text][]';
$addon_name_price   = 'yith-ampa-addon[{{INDEX}}][inputs][price][]';
$array_text_size    = 0;
$array_inputs_text  = array();
$array_inputs_price = array();

?>

<p class='yith-ampa-options-base'>
	<span>Options</span>
	<div class='yith-ampa-container'> 
	<?php
	if ( isset( $addon['inputs'] ) ) {

		$array_inputs_text  = $addon['inputs']['text'];
		$array_inputs_price = $addon['inputs']['price'];
		$array_text_size    = count( $array_inputs_text );

		$addon_index      = $addon['index'];
		$addon_name_text  = 'yith-ampa-addon[' . $addon['index'] . '][inputs][text][]';
		$addon_name_price = 'yith-ampa-addon[' . $addon['index'] . '][inputs][price][]';

		for ( $i = 0; $i < $array_text_size; $i++ ) {
			?>
 
		<div class='yith-ampa-struct-options'>
			<input 
				class='yith-option-name' 
				name ='<?php echo esc_html( $addon_name_text ); ?>'
				type ='text' 
				placeholder='Write name'
				value = '<?php
				if ( isset( $array_inputs_text[ $i ] ) ) {
					echo esc_html( $array_inputs_text[ $i ] );
				}
				?>'>


			<input 
				class='yith-option-price' 
				name ='<?php echo esc_html( $addon_name_price ); ?>'
				type='text' 
				placeholder='Write price'
				value = '<?php if ( isset( $array_inputs_price[ $i ] ) && 'fixed' === $addon['price_setting'] ) {
					echo esc_html( $array_inputs_price[ $i ] );
				}
				?>'>

			<span class='dashicons dashicons-trash yith-ampa-icon-trash' id='yith-ampa-icon-trash-{{INDEX}}'></span>
		</div>
			<?php
		}
	} else {
	?>
		<div class='yith-ampa-struct-options'>
		<input 
			class='yith-option-name' 
			name ='<?php echo esc_html( $addon_name_text ); ?>'
			type ='text' 
			placeholder='Write name'
			>


		<input 
			class='yith-option-price' 
			name ='<?php echo esc_html( $addon_name_price ); ?>'
			type='text' 
			placeholder='Write price'
		    >

		<span class='dashicons dashicons-trash yith-ampa-icon-trash' id='yith-ampa-icon-trash-{{INDEX}}'></span>
	</div>
	<?php } ?>
	</div>
</p>

<a class= 'yith-ampa-new-option'>Add new option</a>
