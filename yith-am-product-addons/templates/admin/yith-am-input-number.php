<?php
/** File number input
 *
 * @author Alberto Martín Núñez
 * @package WordPress
 */

$addon_name         = $args['name'];
$value_input_number = $args['value'];

if ( $addon ) {
	$addon_index        = $addon['index'];
	$value_input_number = $addon['free_chars'];

	$addon_name = 'yith-ampa-addon[' . $addon['index'] . '][free_chars]';
}

?>

<p class='form-field <?php echo esc_html( $args['div_class'] ); ?>'>
		<label><?php echo esc_html__( $args['label'] ); ?> </label>
		<input 
			type='number' 
			name='<?php echo esc_html( $addon_name ); ?>'
			min='<?php echo esc_html( $args['min'] ); ?>'
			value='<?php echo esc_html( $value_input_number ); ?>'
			>
</p>
