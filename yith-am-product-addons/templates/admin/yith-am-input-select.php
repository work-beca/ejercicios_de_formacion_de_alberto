<?php
/** File select
 *
 * @author Alberto Martín Núñez
 * @package WordPress
 */

$addon_name         = $args['name'];
$value_input_select = empty( $addon['field_type'] ) ? $args['value'] : $addon['field_type'];

if ( $addon ) {
	$addon_index = $addon['index'];
	$addon_name  = 'yith-ampa-addon[' . $addon['index'] . '][field_type]';
}
?>

<hr>
<p class='form-field'>
	<label>
		<?php echo esc_html__( $args['label'], 'yith-am-product-addons' ); ?>
	</label>
	<select 
	class = '<?php echo esc_html( $args['class'] ); ?>'
	name='<?php echo esc_html( $addon_name ); ?>'>
		<?php foreach ( $args['option'] as $option ) { ?>
			<option 
				<?php
				$selected = $option === $value_input_select ? ' selected' : ' ';
				echo esc_html( $selected );
				?>
				> 
				<?php
				echo esc_html( $option );
				?>
			</option>
			<?php
		}
		?>
	</select>
</p>
<hr>
