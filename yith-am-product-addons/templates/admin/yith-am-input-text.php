<?php
/**
 * File input text
 *
 * @package WordPress
 */

$addon_name       = $args['name'];
$input_text_value = $args['value'];
if ( $addon ) {
	$addon_index = $addon['index'];

	if ( 'yith-ampa-change-text' === $args['class'] ) {

		$input_text_value = $addon['name'];
		$addon_name       = 'yith-ampa-addon[' . $addon['index'] . '][name]';
	}
	if ( 'yith-ampa-text-price' === $args['div_class'] ) {
		$input_text_value = $addon['price'];
		$addon_name       = 'yith-ampa-addon[' . $addon['index'] . '][price]';
	}
}

?>
<hr>
<p class='form-field <?php echo esc_html( $args['div_class'] ); ?>'>
	<label> <?php echo esc_html( $args['label'] ); ?> </label>
	<input 
		type='text' 
		name='<?php echo esc_html( $addon_name ); ?>'
		placeholder='<?php echo esc_html( $args['placeholder'] ); ?>'
		value = '<?php echo esc_html( $input_text_value ); ?>'
		class= '<?php echo esc_html( $args['class'] ); ?>'

		>
</p>
