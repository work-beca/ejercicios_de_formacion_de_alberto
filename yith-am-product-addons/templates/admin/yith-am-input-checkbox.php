<?php
/**
 * File input checkbox
 *
 * @package WordPress
 */
$addon_name = $args['name'];
$checked    = isset( $addon['enable'] ) ? 'checked' : '';

if ( $addon ) {

	$addon_index = $addon['index'];
	$addon_name  = 'yith-ampa-addon[' . $addon['index'] . '][enable]';
}
?>

<p class='form-field'>
<label class="switch-new">
	<input 
		type='checkbox' 
		name='<?php echo esc_html( $addon_name ); ?>'
		<?php echo esc_attr( $checked ); ?>>
	<span class="round-new"></span>
</label>
</p>
