<?php
/** File colorpicker input
 *
 * @author Alberto Martín Núñez
 * @package WordPress
 */

global $post;

if ( is_object( $post ) ) {

	$value_get_post_meta = get_post_meta( $post->ID, $args['name'] );
	$value_input_color   = empty( $value_get_post_meta[0] ) ? esc_html__( $args['value'] ) : esc_html__( $value_get_post_meta[0] );
} else {
	$value_get_options = get_option( 'border_note_box_name' );
	$value_input_color = empty( $value_get_options['color'] ) ? esc_html__( $args['value'] ) : esc_html__( $value_get_options['color'] );
}

?>

<p class='form-field <?php echo esc_html__( $args['div_class'] ); ?>'>
	<label> <?php echo esc_html__( $args['label'] ); ?> </label>
	<input 
		type='text' 
		name='<?php echo esc_html__( $args['name'] ); ?>'
		id='<?php echo esc_html__( $args['id'] ); ?>'
		class ='<?php echo esc_html__( $args['class'] ); ?>'
		placeholder='<?php echo esc_html__( $args['placeholder'] ); ?>'
		value ='<?php echo esc_html__( $value_input_color ); ?>'
		>
</p>
