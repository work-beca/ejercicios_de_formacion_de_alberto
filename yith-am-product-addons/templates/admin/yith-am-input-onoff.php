<?php
/**
 * File input onoff
 *
 * @package WordPress
 */
$addon_name = $args['name'];
$checked    = isset( $addon['enable_default'] ) ? 'checked' : '';

if ( $addon ) {

	$addon_index = $addon['index'];
	$addon_name  = 'yith-ampa-addon[' . $addon['index'] . '][enable_default]';
}
?>

<div class= 'yith-ampa-enable-default-base'>
	<p class= 'yith-ampa-enable-default-span'>Enable default</p>
	<p class= 'yith-ampa-enable-default'>
		<span>
			<label class='switch-new'>
				<input 
					type='checkbox' 
					name='<?php echo esc_html( $addon_name ); ?>'
					<?php echo esc_attr( $checked ); ?>>
				<span class='round-new'></span>
			</label>
		</span>
	</p>
</div>
