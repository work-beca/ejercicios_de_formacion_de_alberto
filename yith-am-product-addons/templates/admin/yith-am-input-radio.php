<?php
/** File number input
 *
 * @author Alberto Martín Núñez
 * @package WordPress
 */

$addon_name = $args['name'];

if ( $addon ) {
	$addon_index = $addon['index'];
	$addon_name  = 'yith-ampa-addon[' . $addon['index'] . '][price_setting]';
	$checked     = ! empty( $addon['price_setting'] ) && $addon['price_setting'] === $args['value'] ? 'checked' : '';

} else {
	$checked = 'free' === $args['value'] ? 'checked' : '';
}

?>
<p class='form-field <?php echo esc_html( $args['div_class'] ); ?>'>
	<label>
		<?php echo esc_html__( $args['label'], 'yith-am-product-addons' ); ?>
	</label>	
	<input 
		type='radio' 
		name='<?php echo esc_html( $addon_name ); ?>'  
		value='<?php echo esc_html( $args['value'] ); ?>' 
		<?php echo esc_html( $checked ); ?>
	>
</p>
