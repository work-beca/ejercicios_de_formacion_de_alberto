<?php
/**
 * File show addons
 *
 * @package WordPress
 */

?>

<div id = 'yith-ampa-container-order-addons' class ='container-addons'>
	<div class = 'yith-ampa-addons'>
	<?php
	// ¡Si hay addons guardados en la bbdd los mostramos primero con la misma estructura que  los que se añaden al hacer click en el boton
	if ( $addons ) {
		foreach ( $addons[0] as $addon ) {
			?>

	<div class = 'yith-ampa-addons-template-border'>

		<input type='hidden' class ='yith-ampa-addons-index' name= 'yith-ampa-addon[<?php echo esc_attr( $addon['index'] ); ?>][index]' value = '<?php echo intval( isset( $addon['index'] ) ? $addon['index'] : -1 ); ?>'>
		<!-- Head of Addons -->
		<div class = 'yith-ampa-addons-head'>		
			<span class='dashicons dashicons-arrow-right'></span>
			<div class='yith-ampa-title-addons'><?php echo esc_html( $addon['name'] ); ?></div>
			<div class = 'yith-ampa-checkbox'>
			<?php
			foreach ( $args['fields'] as $field ) {
				if ( 'checkbox' === $field['type_input'] ) {
					YITH_AM_PRODUCT_Inputs::show_input( $field, $addon );
				}
			}
			?>
			</div>
		</div>
		<!-- Body of Addons -->
		<div class = 'yith-ampa-addons-body'>
			<div class='options_group'>
			<?php
			foreach ( $args['fields'] as $field ) {
				if ( 'checkbox' !== $field['type_input'] ) {
					YITH_AM_PRODUCT_Inputs::show_input( $field, $addon );

				}
			}
			?>
			<div class= 'yith-ampa-remove-button-class'>
				<button type='button' class='button panel button-primary yith-ampa-button-remove-addon'> REMOVE ADDON </button>
			</div>
			</div>
		</div>

	</div>

			<?php
		}
	}
	?>
	</div>
	<!-- Addons template -->
	<div class = 'yith-ampa-addons-template hidden-addons-default'>
		<div class= 'yith-ampa-addons-template-border'>
		<input type='hidden' class ='yith-ampa-addons-index' name= 'yith-ampa-addon[{{INDEX}}][index]' value = '<?php echo intval( isset( $args['index'] ) ? $args['index'] : -1 ); ?>'>
			<!-- Head of Addons -->
			<div class = 'yith-ampa-addons-head'>		
				<span class='dashicons dashicons-arrow-right'></span>
				<div class='yith-ampa-title-addons'><?php echo esc_html( $args['title'] ); ?></div>
				<div class = 'yith-ampa-checkbox'>
				<?php
				foreach ( $args['fields'] as $field ) {
					if ( 'checkbox' === $field['type_input'] ) {
						YITH_AM_PRODUCT_Inputs::show_input( $field, array() );
					}
				}
				?>
				</div>
			</div>
			<!-- Body of Addons -->
			<div class = 'yith-ampa-addons-body'>
				<div class='options_group'>
					<?php
					foreach ( $args['fields'] as $field ) {
						if ( 'checkbox' !== $field['type_input'] ) {
							YITH_AM_PRODUCT_Inputs::show_input( $field, array() );
						}
					}
					?>
					<div class= 'yith-ampa-remove-button-class'>
						<button type='button' class='button panel button-primary yith-ampa-button-remove-addon'> REMOVE ADDON </button>
					</div>
				</div>		
			</div>

		</div>
	</div>	
</div>
