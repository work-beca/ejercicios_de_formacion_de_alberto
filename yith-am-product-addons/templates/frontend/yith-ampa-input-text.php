<?php
/**
 * File textarea input
 *
 * @package WordPress
 * @author Alberto Martín Núñez
 */

if ( ! empty( $addon['price'] ) ) { ?>
		<label class='yith-ampa-price-addon-text-label yith-ampa-value-label'></label>
<?php	} ?>
<input type='hidden' class= 'yith-ampa-SelectText-name-addon' name='yith-ampa-SelectText-name-addon' value = '<?php echo esc_html( $addon['name'] ); ?>'>
<input type='hidden' class= 'yith-ampa-SelectText-text-price' name='yith-ampa-SelectText-text-price' value = '<?php echo esc_html( $addon['price'] ); ?>'>
<div class='yith-ampa-addon-field-type'>
	<input 
		type='text' 
		name='yith-ampa-input-text'
		placeholder='<?php echo esc_html__( 'Introduzca el texto', 'yith-am-product-addons' ); ?>'
		class = 'yith-ampa-input-text-frontend'
		>
</div>
