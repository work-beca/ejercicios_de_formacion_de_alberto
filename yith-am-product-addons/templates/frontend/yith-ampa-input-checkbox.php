<?php
/**
 * File textarea input
 *
 * @package WordPress
 * @author Alberto Martín Núñez
 */

?>
<?php if ( ! empty( $addon['price'] ) ) { ?>
		<label class='yith-ampa-price-addon-checkbox-label yith-ampa-value-label'></label>
		<input type='hidden' class='yith-ampa-SelectCheckbox-text-price' name='yith-ampa-SelectCheckbox-text-price' value='<?php echo esc_html( $addon['price'] ); ?>'>
<?php	} ?>
<input type='hidden' class='yith-ampa-SelectCheckbox-name-addon' name='yith-ampa-SelectCheckbox-name-addon' value='<?php echo esc_html( $addon['name'] ); ?>'>
<div class='yith-ampa-addon-field-type'>
	<input
		type = 'checkbox'
		name = 'yith-ampa-input-checkbox'
		class = 'yith-ampa-input-checkbox-frontend'
		<?php
		if ( isset( $addon['enable_default'] ) ) {
			echo 'checked';
		}
		?>
		>   
</div>
