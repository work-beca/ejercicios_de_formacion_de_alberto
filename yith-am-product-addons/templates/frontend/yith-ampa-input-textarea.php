<?php
/**
 * File textarea input
 *
 * @package WordPress
 * @author Alberto Martín Núñez
 */

?>

<?php if ( ! empty( $addon['price'] ) ) { ?>
		<label class='yith-ampa-price-addon-textarea-label yith-ampa-value-label'></label>
		
<?php } ?>
<input type='hidden' class='yith-ampa-SelectTextarea-name-addon' name='yith-ampa-SelectTextarea-name-addon' value='<?php echo esc_html( $addon['name'] ); ?>'>
<input type='hidden' class='yith-ampa-SelectTextarea-text-price' name='yith-ampa-SelectTextarea-text-price' value='<?php echo esc_html( $addon['price'] ); ?>'>

<div class='yith-ampa-addon-field-type'>
		<textarea  
			rows='2'
			name ='yith-ampa-input-textarea'
			class ='yith-ampa-input-textarea-frontend'
			placeholder = '<?php echo esc_html__( 'Write here', 'yith-am-product-addons' ); ?>'	
		></textarea>
</div>
