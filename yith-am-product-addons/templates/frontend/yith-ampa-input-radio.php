<?php
/**
 * File textarea input
 *
 * @package WordPress
 * @author Alberto Martín Núñez
 */

?>

<label class = 'yith-ampa-price-addon-radio-label yith-ampa-value-label' > </label>
<input type='hidden' class='yith-ampa-SelectOptionsRadio-text-price' name='yith-ampa-SelectOptionsRadio-text-price' value=''>
<input type='hidden' class='yith-ampa-SelectOptionsRadio-name-addon' name='yith-ampa-SelectOptionsRadio-name-addon' value='<?php echo esc_html( $addon['name'] ); ?>'>
<div class='yith-ampa-addon-field-type yith-ampa-radio-addons-frontend'>
<?php
$text_array = count( $addon['inputs']['text'] );
for ( $i = 0; $i < $text_array;$i++ ) {
	if ( ! empty( $addon['inputs']['price'][ $i ] ) && ( 'fixed' === $addon['price_setting'] ) ) {
		$value_input_radio = $addon['inputs']['text'][ $i ] . ' + ' . $addon['inputs']['price'][ $i ];
	} else {
		$value_input_radio = $addon['inputs']['text'][ $i ];
	}

	?>
	<div class='yith-ampa-radio-input-base'>
	<span class='yiht-ampa-selector-radio'><?php echo esc_html( $value_input_radio ); ?></span>
	<input 
		type='radio' 
		name='yith-ampa-input-radio'
		class='yith-ampa-input-radio-frontend'  
		value='<?php echo esc_html( $value_input_radio ); ?>' 
	>
	</div>
	

<?php } ?>
</div>
