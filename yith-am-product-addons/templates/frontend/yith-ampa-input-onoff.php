<?php
/**
 * File textarea input
 *
 * @package WordPress
 * @author Alberto Martín Núñez
 */

?>
<?php if ( ! empty( $addon['price'] ) ) { ?>
		<label class='yith-ampa-price-addon-onoff-label yith-ampa-value-label'></label>
		<input type='hidden' class='yith-ampa-SelectOnoff-text-price' name='yith-ampa-SelectOnoff-text-price' value='<?php echo esc_html( $addon['price'] ); ?>'>
<?php	} ?>
<input type='hidden' class='yith-ampa-SelectOnoff-name-addon' name='yith-ampa-SelectOnoff-name-addon' value='<?php echo esc_html( $addon['name'] ); ?>'>
<div class='yith-ampa-addon-field-type'>
	<label class="switch-new">
		<input 
			type='checkbox' 
			name='yith-ampa-input-onoff'
			class= 'yith-ampa-input-onoff-frontend'
			<?php
			if ( isset( $addon['enable_default'] ) ) {
				echo 'checked';
			}
			?>
		>
		<span class='round-new'></span>
	</label>
</div>
