<?php
/**
 * File textarea input
 *
 * @package WordPress
 * @author Alberto Martín Núñez
 */

?>
<label class = 'yith-ampa-price-addon-select-label yith-ampa-value-label' > </label>
<input type='hidden' class='yith-ampa-SelectOptionsSelect-text-price' name='yith-ampa-SelectOptionsSelect-text-price' value=''>
<input type='hidden' class='yith-ampa-SelectOptionsSelect-name-addon' name='yith-ampa-SelectOptionsSelect-name-addon' value='<?php echo esc_html( $addon['name'] ); ?>'>
<div class='yith-ampa-addon-field-type'>
	<select 
		class='yith-ampa-input-select-frontend'
		name='yith-ampa-input-select'>
		<?php
		$text_array = count( $addon['inputs']['text'] );
		for ( $i = 0; $i < $text_array;$i++ ) {

			?>
			<option >
				<?php
				if ( ! empty( $addon['inputs']['price'][ $i ] ) && ( 'fixed' === $addon['price_setting'] ) ) {
					echo esc_html( $addon['inputs']['text'][ $i ] . ' + ' . $addon['inputs']['price'][ $i ] );
				} else {
					$value_input_radio = $addon['inputs']['text'][ $i ];
					echo esc_html( $value_input_radio );
				}
				?>
			</option>
			<?php
		}
		?>
	</select>
</div>
