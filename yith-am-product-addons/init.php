<?php
/**
 * Plugin Name: yith-am-product-addons
 * Plugin URI: https://yith-am-product-addons.es
 * Description:The plugin allows to add add-on fields to products on frontend, so the customer can set some additional fields to products; these fields could influence the product product price and will be visible in the order (on frontend and backend)
 * Version: 1.0.0
 * Author: Alberto Martin
 * Author URI: https://alberto.es
 * License: GPL2
 * Text Domain: yith-am-product-addons
 * php version 7.4
 *
 * @category Plugin
 * @package  WordPress
 * @author   Alberto Martin <alberto.mrtn.nu@gmail.com>
 * @license  www.url.es GPL2
 * @link     http://link.com
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! defined( 'YITH_AM_PRODUCT_ADDONS_VERSION' ) ) {
	define( 'YITH_AM_PRODUCT_ADDONS_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_AM_PRODUCT_ADDONS_DIR_URL' ) ) {
	define( 'YITH_AM_PRODUCT_ADDONS_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_URL' ) ) {
	define( 'YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_URL', YITH_AM_PRODUCT_ADDONS_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_CSS_URL', YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_JS_URL', YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_AM_PRODUCT_ADDONS_DIR_PATH' ) ) {
	define( 'YITH_AM_PRODUCT_ADDONS_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_AM_PRODUCT_ADDONS_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_AM_PRODUCT_ADDONS_DIR_INCLUDES_PATH', YITH_AM_PRODUCT_ADDONS_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_AM_PRODUCT_ADDONS_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_AM_PRODUCT_ADDONS_DIR_TEMPLATES_PATH', YITH_AM_PRODUCT_ADDONS_DIR_PATH . 'templates' );
}

if ( ! defined( 'YITH_AM_PRODUCT_ADDONS_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_AM_PRODUCT_ADDONS_DIR_VIEWS_PATH', YITH_AM_PRODUCT_ADDONS_DIR_PATH . '/views' );
}



/**
 *
 * Included the scripts
 */


if ( ! function_exists( 'yith_am_product_addons_init_classes' ) ) {
	/**
	 * Yith_am_product_addons_init_classes
	 *
	 * @return void
	 */
	function yith_am_product_addons_init_classes() {

		load_plugin_textdomain( 'yith-am-product-addons', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// @require all the files you include on your plugins
		require_once YITH_AM_PRODUCT_ADDONS_DIR_INCLUDES_PATH . '/class-yith-am-product-addons-plugin.php';

		if ( class_exists( 'YITH_AM_PRODUCT_ADDONS_Plugin' ) ) {
			/**
			 * Call the main function
			 */
			yith_am_product_addons_plugin();
		}
	}
}

add_action( 'plugins_loaded', 'yith_am_product_addons_init_classes' );
