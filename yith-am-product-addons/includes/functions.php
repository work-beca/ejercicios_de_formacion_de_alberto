<?php
/**
 * YITH functions
 *
 * @package WordPress
 */

if ( ! function_exists( 'yith_am_product_addons_get_template' ) ) {
	/**
	 * Yith_am_product_addons_get_template
	 *
	 * @param  mixed $file_name comment.
	 * @param  mixed $args comment.
	 * @return void
	 */
	function yith_am_product_addons_get_template( $file_name, $args = array(), $addons ) {
		$full_path = YITH_AM_PRODUCT_ADDONS_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_am_product_get_inputs' ) ) {
	/**
	 * Yith_am_product_purchase_note_get_template
	 *
	 * @param  mixed $file_name comment.
	 * @param  mixed $args comment.
	 * @return void
	 */
	function yith_am_product_get_inputs( $file_name, $args = array(), $addon ) {
		$full_path = YITH_AM_PRODUCT_ADDONS_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_ampa_addons_inputs' ) ) {
	/**
	 * Yith_am_product_purchase_note_get_template
	 *
	 * @param  mixed $file_name comment.
	 * @param  mixed $args comment.
	 * @return void
	 */
	function yith_ampa_addons_inputs( $file_name, $addon ) {
		$full_path = YITH_AM_PRODUCT_ADDONS_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}


add_action( 'wp_ajax_nopriv_notify_button_click', 'notify_button_click' );
add_action( 'wp_ajax_notify_button_click', 'notify_button_click' );
/**
 * Notify_button_click
 *
 * @return void
 */
function notify_button_click() {

	$message = isset( $_POST['message'] ) ? $_POST['message'] : false;
	if ( ! $message ) {
		wp_send_json( array( 'message' => __( 'Message not received :(', 'wpduf' ) ) );
	} else {
		wp_send_json( array( 'message' => __( 'Message received, greetings from server!', 'wpduf' ) ) );
	}
}
