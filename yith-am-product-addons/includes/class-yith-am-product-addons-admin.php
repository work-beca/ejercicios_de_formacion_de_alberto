<?php
/**
 * File class admin
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_PRODUCT_ADDONS_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_PRODUCT_ADDONS_Admin' ) ) {
	/**
	 * YITH_AM_PRODUCT_ADDONS_Admin
	 */
	class YITH_AM_PRODUCT_ADDONS_Admin {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_PRODUCT_ADDONS_Admin
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_PRODUCT_ADDONS_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yith_ampa_add_tab_product_addons' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'yith_ampa_addons_options_style_icon' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'yith_ampa_addons_options' ) );
			add_action( 'woocommerce_admin_process_product_object', array( $this, 'yith_ampa_save_product_addons' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'yith_am_style_addons_admin' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'yith_ampa_script_enqueue_dropdown_style' ) );
		}

		/**
		 * Script_enqueue_options
		 *
		 * @return void
		 */
		public function yith_ampa_script_enqueue_dropdown_style() {

			wp_register_script( 'yith-ampa-script-panel', YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_JS_URL . '/yith-ampa-script-panel.js', array( 'jquery' ), '1', true );
			wp_enqueue_script( 'yith-ampa-script-panel' );
		}

		/**
		 * Yith_am_style_addons_admin
		 *
		 * @return void
		 */
		public function yith_am_style_addons_admin() {
			wp_register_style( 'admin-styles', YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_CSS_URL . '/yith-am-style-addons-admin.css', false );
			wp_enqueue_style( 'admin-styles' );
		}
		/**
		 * Yith_am_add_tab_product_addons
		 *
		 * @return array
		 * @param array $tabs tabs of options product.
		 */
		public function yith_ampa_add_tab_product_addons( $tabs ) {

			$tabs['Add_ons'] = array(
				'label'  => __( 'Add ons', 'yith-ampa-product-addons' ),
				'class'  => array( 'show_if_simple', 'show_if_variable' ),
				'target' => 'Add_ons_panel',
			);
			return $tabs;

		}

		/**
		 * Add_ons_options_style_icon
		 *
		 * @return void
		 */
		public function yith_ampa_addons_options_style_icon() {
			?>
			<style>
				#woocommerce-product-data ul.wc-tabs li.Add_ons_options a:before { font-family: dashicons; content: '\f119'; }
			</style>
			<?php
		}

		/**
		 * Yith_am_addons_options
		 */
		public function yith_ampa_addons_options() {

			global $post;
			$struct_addons = array(
				'title'  => 'Untitled',
				'id'     => 'yith-ampa-total-addons-id',
				'name'   => 'yith-ampa-total-addons-name',
				'index'  => '',
				'fields' => array(
					array(
						'type_input' => 'checkbox',
						'name'       => 'yith-ampa-addon[{{INDEX}}][enable]',
						'class'      => 'display-check-add-on',
						'check'      => 'checked',

					),
					array(

						'type_input'  => 'text',
						'name'        => 'yith-ampa-addon[{{INDEX}}][name]',
						'class'       => 'yith-ampa-change-text',
						'label'       => __( 'Name', 'yith-am-product-addons' ),
						'placeholder' => __( 'Write the name' ),
						'value'       => __( 'Untitled' ),
					),
					array(
						'type_input'  => 'textarea',
						'name'        => 'yith-ampa-addon[{{INDEX}}][description]',
						'label'       => __( 'Note description', 'yith-am-product-addons' ),
						'placeholder' => __( 'Write the note description', 'yith-am-product-addons' ),

					),
					array(
						'type_input' => 'select',
						'name'       => 'yith-ampa-addon[{{INDEX}}][field_type]',
						'class'      => 'yith-ampa-select-type-field',
						'option'     => array(
							'text',
							'textarea',
							'select',
							'radio',
							'checkbox',
							'onoff',
						),
						'label'      => __( 'Select field type', 'yith-am-product-addons' ),

					),

					array(
						'type_input' => 'radio',
						'id'         => 'yith-ampa-radio-free-Id',
						'name'       => 'yith-ampa-addon[{{INDEX}}][price_setting]',
						'div_class'  => 'yith-ampa-radio-free',
						'label'      => __( 'Free', 'yith-am-product-addons' ),
						'value'      => 'free',
						'check'      => 'checked',
					),
					array(
						'type_input' => 'radio',
						'id'         => 'yith-ampa-radio-Fixed-Id',
						'name'       => 'yith-ampa-addon[{{INDEX}}][price_setting]',
						'div_class'  => 'yith-ampa-radio-fixed',
						'label'      => __( 'Fixed', 'yith-am-product-addons' ),
						'value'      => 'fixed',

					),
					array(
						'type_input' => 'radio',
						'id'         => 'yith-ampa-radio-Character-Id',
						'name'       => 'yith-ampa-addon[{{INDEX}}][price_setting]',
						'div_class'  => 'yith-ampa-radio-char',
						'label'      => __( 'Per Character', 'yith-am-product-addons' ),
						'value'      => 'character',

					),
					array(
						'type_input' => 'onoff',
						'name'       => 'yith-ampa-addon[{{INDEX}}][enable_default]',

					),
					array(
						'type_input' => 'options',
					),

					array(
						'type_input'  => 'text',
						'id'          => 'yith-ampa-textPriceId',
						'name'        => 'yith-ampa-addon[{{INDEX}}][price]',
						'div_class'   => 'yith-ampa-text-price',
						'label'       => __( 'Price', 'yith-am-product-addons' ),
						'placeholder' => __( 'Write the price', 'yith-am-product-addons' ),
						'value'       => 0,
					),
					array(
						'type_input' => 'number',
						'id'         => 'yith-ampa-input-number-free-characterId',
						'name'       => 'yith-ampa-addon[{{INDEX}}][free_chars]',
						'div_class'  => 'yith-ampa-character-number',
						'label'      => __( 'Number of free characters', 'yith-am-product-addons' ),
						'min'        => 0,
						'value'      => 0,

					),

				),

			);

			$array_addons = get_post_meta( $post->ID, 'yith_ampa_addons' );

			?>
			<div id ='Add_ons_panel' class= 'panel woocommerce_options_panel'>
				<div class ='div-button-addons'>
					<button type='button' class='button panel button-primary yith-ampa-button-addon'>Add New ADD-ON</button>        
				</div> 
				<?php
				yith_am_product_addons_get_template( '/admin/yith-am-show-addons.php', $struct_addons, $array_addons );
				?>
			</div>
			<?php
		}

		/**
		 * Yith_ampa_save_product_addons
		 *
		 * @param {} $product Producto.
		 * @return void
		 */
		public function yith_ampa_save_product_addons( $product ) {

			if ( isset( $_POST['yith-ampa-addon'] ) ) {
				$array_addons = wp_unslash( $_POST['yith-ampa-addon'] );
				if ( array_key_exists( '{{INDEX}}', $array_addons ) ) {
					unset( $array_addons['{{INDEX}}'] );
				}
				error_log( print_r( $array_addons, true ) );
				$product->update_meta_data( 'yith_ampa_addons', $array_addons );

			}

		}
	}
}
