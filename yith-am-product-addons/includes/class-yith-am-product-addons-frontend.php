<?php
/**
 * File class admin
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_PRODUCT_ADDONS_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AM_PRODUCT_ADDONS_Frontend' ) ) {
	/**
	 * YITH_AM_PRODUCT_ADDONS_Frontend
	 */
	class YITH_AM_PRODUCT_ADDONS_Frontend {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_PRODUCT_ADDONS_Frontend
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_PRODUCT_ADDONS_Frontend
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_action( 'wp_enqueue_scripts', array( $this, 'yith_ampa_frontend_style' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_ampa_frontend_summary_product_price' ) );
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'yith_ampa_add_addons' ) );
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'yith_ampa_summary' ) );
			add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'yith_ampa_add_addons_to_cart_validation' ), 10, 4 );
			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'yith_ampa_add_addon_cart_item_data' ), 10, 3 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'yith_ampa_get_item_data' ), 10, 2 );
			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'yith_ampa_checkout_create_order_line_item' ), 10, 4 );
			add_action( 'woocommerce_before_calculate_totals', array( $this, 'yith_ampa_add_price' ), 99 );
			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'yith_ampa_checkout_create_order_line_item' ), 10, 4 );

			// Para ajax
			add_action( 'wp_ajax_nopriv_notify_button_click', array( $this, 'notify_button_click' ) );

			add_action( 'wp_ajax_notify_button_click', array( $this, 'notify_button_click' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'ajax_test_enqueue_scripts' ) );
			add_action( 'init', array( $this, 'init_ready' ) );

		}

		/**
		 * Ajax_test_enqueue_scripts
		 *
		 * @return void
		 */
		function ajax_test_enqueue_scripts() {
			wp_register_script( 'yith-ampa-script-message-ajax', YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_JS_URL . '/yith-am-message-ajax.js', array( 'jquery' ), 1, false );
			wp_enqueue_script( 'yith-ampa-script-message-ajax' );
			wp_localize_script( 'yith-ampa-script-message-ajax', 'ajax_var', array( 'url' => admin_url( 'admin-ajax.php' ) ) );
		}
		/**
		 * Ready_init
		 */
		function init_ready() {
			/**
			 * Yith_handle_details_user
			 *
			 * @return $output
			 */
			add_shortcode( 'yith_message_ajax', array( $this, 'yith_handle_message_ajax' ) );

		}

		function yith_handle_message_ajax() {

			$output = '<div><button id="my-button">Click me</button><hr>
						<p id="txtMessage">Nothing yet</p></div><hr>';

			return $output;
		}

		// Función que procesa la llamada AJAX
		/**
		 * Notify_button_click
		 *
		 * @return void
		 */
		function notify_button_click() {

			$message = isset( $_POST['message'] ) ? $_POST['message'] : false;
			if ( ! $message ) {
				wp_send_json( array( 'message' => __( 'Message not received :(', 'wpduf' ) ) );
			} else {
				wp_send_json( array( 'message' => __( 'Message received, greetings from server!', 'wpduf' ) ) );
			}
		}

		/**
		 * Yith_ampa_frontend_style_addons
		 *
		 * @return void
		 */
		public function yith_ampa_frontend_style() {
			wp_register_style( 'style_product_addons', YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_CSS_URL . '/yith-ampa-style-frontend-addons.css', false );
			wp_register_style( 'style_product_summary', YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_CSS_URL . '/yith-ampa-style-frontend-summary.css', false );
		}

		/**
		 * Yith_ampa_frontend_summary_product_price
		 *
		 * @return void
		 */
		public function yith_ampa_frontend_summary_product_price() {
			wp_register_script( 'yith_ampa_summary_price', YITH_AM_PRODUCT_ADDONS_DIR_ASSETS_JS_URL . '/yith-ampa-summary-price.js', array( 'jquery' ), false, true );
		}

		/**
		 * Yith_ampa_add_addons
		 *
		 * @return void
		 */
		public function yith_ampa_add_addons() {

			wp_enqueue_style( 'style_product_addons' );
			global $product;
			$id_product_current = $product->get_id();

			$addons = $product->get_meta( 'yith_ampa_addons' );
			// error_log(print_r($addons, true));
			$num_addons = 0;
			if ( $addons ) {
				$num_addons = count( $addons );

			}
			$data_addon = array();
			for ( $i = 1; $i <= $num_addons + 1;$i++ ) {

				if ( isset( $addons[ $i ]['enable'] ) && 'on' === $addons[ $i ]['enable'] ) {
					?>
					<div class = 'yith-ampa-total-addons'>
						<div class = 'yith-ampa-addon-frontend'>
							<input type='hidden' class='yith-ampa-addon-index-frontend' name='yith-ampa-addon-index-frontend' value='<?php echo esc_html( $addons[ $i ]['index'] ); ?>'>
							<div class = 'yith-ampa-addon-name'><strong><?php echo esc_html( $addons[ $i ]['name'] ); ?></strong></div>
							<div class = 'yith-ampa-addon-description'><?php echo esc_html( $addons[ $i ]['description'] ); ?> </div>
						<?php

						yith_ampa_addons_inputs( '/frontend/yith-ampa-input-' . $addons[ $i ]['field_type'] . '.php', $addons[ $i ] );

						?>
						</div>
					</div> 
					<?php
				}
			}

		}

		/**
		 * Yith_ampa_summary
		 *
		 * @return void
		 */
		public function yith_ampa_summary() {
			wp_enqueue_style( 'style_product_summary' );

			global $product;
			$addons = $product->get_meta( 'yith_ampa_addons' );

			$id_product_current    = $product->get_id();
			$product_price_current = $product->get_price();
			error_log( print_r( $addons, true ) );
			wp_localize_script( 'yith_ampa_summary_price', 'addons', $addons );
			wp_enqueue_script( 'yith_ampa_summary_price' );

			?>
			<div class = 'price-totals-summary'>
				<div class='title-summary'><strong>Price totals:</strong></div>
				<div class='product-price'>
					<div class='product-price-title title-price-style'><?php echo __( 'Product price:', 'yith-am-product-addons' ); ?></div>
					<div class='product-price-number number-price-style'>$<?php echo esc_attr( $product_price_current ); ?></div>
				</div>
				<div class='additional-options-price'>
					<div class='additional-options-price-title title-price-style'><?php echo __( 'Additional options total:', 'yith-am-product-addons' ); ?></div>
					<div class='additional-options-price-number number-price-style'></div>
				</div>
				<div class='total-price'>
					<div class='total-price-title title-price-style'><?php echo __( 'Total:', 'yith-am-product-addons' ); ?></div>
					<div class='total-price-number number-price-style'></div>
				</div>
			</div>
			<?php
		}

		/**
		 * yith_ampa_add_addons_to_cart_validation
		 *
		 * @return bool
		 */
		public function yith_ampa_add_addons_to_cart_validation( $passed, $product_id, $quantity, $variation_id = null ) {

			return $passed;

		}

		/**
		 * Yith_ampa_get_item_data
		 *
		 * @param  mixed $item_data
		 * @param  mixed $cart_item_data
		 * @return void
		 */
		public function yith_ampa_get_item_data( $item_data, $cart_item_data ) {

			// error_log( print_r( $cart_item_data, true ) );
			if ( isset( $cart_item_data['yith_ampa_input_text'] ) ) {
				$item_data[] = array(
					'key'   => $cart_item_data ['yith_ampa_SelectText_name_addon'] . ' ( + ' . $cart_item_data['yith_ampa_SelectText_text_price'] . ')',
					'value' => wc_clean( $cart_item_data ['yith_ampa_input_text'] ),
				);
			}
			if ( isset( $cart_item_data['yith_ampa_input_textarea'] ) ) {
				$item_data[] = array(
					'key'   => $cart_item_data ['yith_ampa_SelectTextarea_name_addon'] . ' ( + ' . $cart_item_data['yith_ampa_SelectTextarea_text_price'] . ')',
					'value' => wc_clean( $cart_item_data ['yith_ampa_input_textarea'] ),
				);

			}

			if ( isset( $cart_item_data['yith_ampa_input_select'] ) ) {
				$options_selected_split = explode( '+', $cart_item_data ['yith_ampa_input_select'] );
				$options_selected       = $options_selected_split[0];
				$item_data[]            = array(
					'key'   => $cart_item_data ['yith_ampa_SelectOptionsSelect_name_addon'] . ' (' . $cart_item_data['yith_ampa_SelectOptionsSelect_text_price'] . ')',
					'value' => wc_clean( $options_selected ),
				);

			}

			if ( isset( $cart_item_data['yith_ampa_input_radio'] ) ) {
				$options_selected_split = explode( '+', $cart_item_data ['yith_ampa_input_radio'] );
				$options_selected       = $options_selected_split[0];
				$item_data[]            = array(
					'key'   => $cart_item_data ['yith_ampa_SelectOptionsRadio_name_addon'] . ' (' . $cart_item_data['yith_ampa_SelectOptionsRadio_text_price'] . ')',
					'value' => wc_clean( $options_selected ),
				);

			}

			if ( isset( $cart_item_data['yith_ampa_input_onoff'] ) ) {
				$item_data[] = array(
					'key'   => $cart_item_data ['yith_ampa_SelectOnoff_name_addon'] . ' ( + ' . $cart_item_data['yith_ampa_SelectOnoff_text_price'] . ')',
					'value' => wc_clean( $cart_item_data ['yith_ampa_input_onoff'] ),
				);

			}

			if ( isset( $cart_item_data['yith_ampa_input_checkbox'] ) ) {
				$item_data[] = array(
					'key'   => $cart_item_data ['yith_ampa_SelectCheckbox_name_addon'] . ' ( + ' . $cart_item_data['yith_ampa_SelectCheckbox_text_price'] . ')',
					'value' => wc_clean( $cart_item_data ['yith_ampa_input_checkbox'] ),
				);

			}

			return $item_data;
		}

		/**
		 * yith_ampa_add_addon_cart_item_data
		 *
		 * @param  mixed $cart_item_data
		 * @param  mixed $product_id
		 * @param  mixed $variaciacion_id
		 * @return void
		 */
		public function yith_ampa_add_addon_cart_item_data( $cart_item_data, $product_id, $variación_id ) {
			error_log( print_r( $_POST, true ) );
			if ( isset( $_POST['yith-ampa-input-text'] ) && ( ! empty( $_POST['yith-ampa-input-text'] ) ) ) {
				$cart_item_data ['yith_ampa_input_text']            = sanitize_text_field( $_POST['yith-ampa-input-text'] );
				$cart_item_data ['yith_ampa_SelectText_name_addon'] = sanitize_text_field( $_POST ['yith-ampa-SelectText-name-addon'] );
				if ( isset( $_POST['yith-ampa-SelectText-text-price'] ) ) {
					$cart_item_data ['yith_ampa_SelectText_text_price'] = sanitize_text_field( $_POST ['yith-ampa-SelectText-text-price'] );
				}
			}

			if ( isset( $_POST['yith-ampa-input-textarea'] ) && ( ! empty( $_POST['yith-ampa-input-textarea'] ) ) ) {
				$cart_item_data ['yith_ampa_input_textarea']            = sanitize_text_field( $_POST ['yith-ampa-input-textarea'] );
				$cart_item_data ['yith_ampa_SelectTextarea_name_addon'] = sanitize_text_field( $_POST ['yith-ampa-SelectTextarea-name-addon'] );
				if ( isset( $_POST['yith-ampa-SelectTextarea-text-price'] ) ) {
					$cart_item_data ['yith_ampa_SelectTextarea_text_price'] = sanitize_text_field( $_POST ['yith-ampa-SelectTextarea-text-price'] );
				}
			}

			if ( isset( $_POST['yith-ampa-input-checkbox'] ) ) {
				$cart_item_data ['yith_ampa_input_checkbox']            = sanitize_text_field( $_POST ['yith-ampa-input-checkbox'] );
				$cart_item_data ['yith_ampa_SelectCheckbox_name_addon'] = sanitize_text_field( $_POST ['yith-ampa-SelectCheckbox-name-addon'] );
				if ( isset( $_POST['yith-ampa-SelectCheckbox-text-price'] ) ) {
					$cart_item_data ['yith_ampa_SelectCheckbox_text_price'] = sanitize_text_field( $_POST ['yith-ampa-SelectCheckbox-text-price'] );
				}
			}

			if ( isset( $_POST['yith-ampa-input-onoff'] ) ) {
				$cart_item_data ['yith_ampa_input_onoff']            = sanitize_text_field( $_POST ['yith-ampa-input-onoff'] );
				$cart_item_data ['yith_ampa_SelectOnoff_name_addon'] = sanitize_text_field( $_POST ['yith-ampa-SelectOnoff-name-addon'] );
				if ( isset( $_POST['yith-ampa-SelectOnoff-text-price'] ) ) {
					$cart_item_data ['yith_ampa_SelectOnoff_text_price'] = sanitize_text_field( $_POST ['yith-ampa-SelectOnoff-text-price'] );
				}
			}

			if ( isset( $_POST['yith-ampa-input-select'] ) ) {
				$cart_item_data ['yith_ampa_input_select']                   = sanitize_text_field( $_POST ['yith-ampa-input-select'] );
				$cart_item_data ['yith_ampa_SelectOptionsSelect_name_addon'] = sanitize_text_field( $_POST ['yith-ampa-SelectOptionsSelect-name-addon'] );
				if ( isset( $_POST['yith-ampa-SelectOptionsSelect-text-price'] ) ) {
					$cart_item_data ['yith_ampa_SelectOptionsSelect_text_price'] = sanitize_text_field( $_POST ['yith-ampa-SelectOptionsSelect-text-price'] );
				}
			}

			if ( isset( $_POST['yith-ampa-input-radio'] ) ) {
				$cart_item_data ['yith_ampa_input_radio']                   = sanitize_text_field( $_POST ['yith-ampa-input-radio'] );
				$cart_item_data ['yith_ampa_SelectOptionsRadio_name_addon'] = sanitize_text_field( $_POST ['yith-ampa-SelectOptionsRadio-name-addon'] );
				if ( isset( $_POST['yith-ampa-SelectOptionsRadio-text-price'] ) ) {
					$cart_item_data ['yith_ampa_SelectOptionsRadio_text_price'] = sanitize_text_field( $_POST ['yith-ampa-SelectOptionsRadio-text-price'] );
				}
			}

			return $cart_item_data;

		}

		/**
		 * Yith_ampa_add_price
		 *
		 * @return void
		 */
		public function yith_ampa_add_price( $cart_object ) {
			// error_log( print_r( $cart_object->cart_contents, true ) );
			foreach ( $cart_object->cart_contents as $key => $value ) {

				if ( isset( $value['yith_ampa_SelectCheckbox_text_price'] ) ) {

					$additionalPrice      = $value['yith_ampa_SelectCheckbox_text_price'];
					$additionalPriceSplit = explode( ' ', $additionalPrice );

					$orgPrice = floatval( $value['data']->get_price() );

					$discPrice = $orgPrice + $additionalPriceSplit[0];
					$value['data']->set_price( $discPrice );
				}

				if ( isset( $value['yith_ampa_SelectOnoff_text_price'] ) ) {

					$additionalPrice      = $value['yith_ampa_SelectOnoff_text_price'];
					$additionalPriceSplit = explode( ' ', $additionalPrice );

					$orgPrice = floatval( $value['data']->get_price() );

					$discPrice = $orgPrice + $additionalPriceSplit[0];
					$value['data']->set_price( $discPrice );
				}

				if ( isset( $value['yith_ampa_SelectText_text_price'] ) ) {

					$additionalPrice      = $value['yith_ampa_SelectText_text_price'];
					$additionalPriceSplit = explode( ' ', $additionalPrice );

					$orgPrice = floatval( $value['data']->get_price() );

					$discPrice = $orgPrice + $additionalPriceSplit[0];
					$value['data']->set_price( $discPrice );
				}

				if ( isset( $value['yith_ampa_SelectTextarea_text_price'] ) ) {

					$additionalPrice      = $value['yith_ampa_SelectTextarea_text_price'];
					$additionalPriceSplit = explode( ' ', $additionalPrice );

					$orgPrice = floatval( $value['data']->get_price() );

					$discPrice = $orgPrice + $additionalPriceSplit[0];
					$value['data']->set_price( $discPrice );
				}

				if ( isset( $value['yith_ampa_SelectOptionsRadio_text_price'] ) ) {

					$additionalPrice      = $value['yith_ampa_SelectOptionsRadio_text_price'];
					$additionalPriceSplit = explode( ' ', $additionalPrice );

					$orgPrice = floatval( $value['data']->get_price() );

					$discPrice = $orgPrice + $additionalPriceSplit[1];
					$value['data']->set_price( $discPrice );
				}

				if ( isset( $value['yith_ampa_SelectOptionsSelect_text_price'] ) ) {

					$additionalPrice      = $value['yith_ampa_SelectOptionsSelect_text_price'];
					$additionalPriceSplit = explode( ' ', $additionalPrice );

					$orgPrice = floatval( $value['data']->get_price() );

					$discPrice = $orgPrice + $additionalPriceSplit[1];
					$value['data']->set_price( $discPrice );
				}
			}
		}

		/**
		 * Yith_ampa_checkout_create_order_line_item
		 *
		 * @return void
		 */
		public function yith_ampa_checkout_create_order_line_item( $item, $cart_item_key, $values, $order ) {

			if ( isset( $values['yith_ampa_SelectText_name_addon'] ) ) {
				$item->update_meta_data(
					__( 'Input text', 'yith-am-product-addons' ),
					$values['yith_ampa_SelectText_name_addon'],
					true
				);
			}

			if ( isset( $values['yith_ampa_SelectTextarea_name_addon'] ) ) {
				$item->update_meta_data(
					__( 'Input textarea', 'yith-am-product-addons' ),
					$values['yith_ampa_SelectTextarea_name_addon'],
					true
				);
			}

			if ( isset( $values['yith_ampa_SelectOnoff_name_addon'] ) ) {
				$item->update_meta_data(
					__( 'Input onoff', 'yith-am-product-addons' ),
					$values['yith_ampa_SelectOnoff_name_addon'],
					true
				);
			}

			if ( isset( $values['yith_ampa_SelectCheckbox_name_addon'] ) ) {
				$item->update_meta_data(
					__( 'Input on/off', 'yith-am-product-addons' ),
					$values['yith_ampa_SelectCheckbox_name_addon'],
					true
				);
			}

			if ( isset( $values['yith_ampa_SelectOptionsRadio_name_addon'] ) ) {
				$item->update_meta_data(
					__( 'Input options Radio', 'yith-am-product-addons' ),
					$values['yith_ampa_SelectOptionsRadio_name_addon'],
					true
				);
			}

			if ( isset( $values['yith_ampa_SelectOptionsSelect_name_addon'] ) ) {
				$item->update_meta_data(
					__( 'Input options Select', 'yith-am-product-addons' ),
					$values['yith_ampa_SelectOptionsSelect_name_addon'],
					true
				);
			}
		}
	}
}
