jQuery(document).ready(function ($) {

  //Iniciallizamoss algunas variables
  var index = 0;

  //ocultamos el body de los addons
  $('.yith-ampa-addons-body').hide();


// Añadimos el contenido de las opciones cuando el select es "Radio" or "Select"
// Lo hago así ya que no existe un template que sea "Options" con dos inputs
  $(".yith-ampa-text-price").before(
    "<p class='yith-ampa-options-base'></p>");
  $('.yith-ampa-options-base').append("<span>Options</span>");
  $('.yith-ampa-options-base').append("<div class='yith-ampa-container'> </div>");
  $('.yith-ampa-container').append("<div class='yith-ampa-struct-options'> </div>");
  $('.yith-ampa-struct-options').append("<input class='yith-option-name' name='yith-ampa-addon[{{INDEX}}][inputs][text][]' type ='text' placeholder='Write name'>");
  $('.yith-ampa-struct-options').append("<input class='yith-option-price' name='yith-ampa-addon[{{INDEX}}][inputs][price][]' type='text' placeholder='Write price'>");
  $('.yith-ampa-struct-options').append("<span class='dashicons dashicons-trash yith-ampa-icon-trash' id='yith-ampa-icon-trash-{{INDEX}}'></span>");
  $('.yith-ampa-options-base').after("<a class= 'yith-ampa-new-option'>Add new option</a>");


  $('.yith-ampa-options-base').before("<div class= 'yith-ampa-enable-default-base'></div>");
  $('.yith-ampa-enable-default-base').append("<p class= 'yith-ampa-enable-default-span'></p>");
  $('.yith-ampa-enable-default-span').append('<span>Enable default</span>');
  $('.yith-ampa-enable-default-base').append("<p class= 'yith-ampa-enable-default'></p>");
  $('.yith-ampa-enable-default').append("<span><label class='switch-new'><input type='checkbox'><span class='round-new'></span></label></span> ");

  
  addNewAddon = function () {
    //cogemos el valor del index mayor de todos los addons para si creamos más addons no se pisen y continuen por el valor que le correspondría
    var index_mayor = 0;
    $('.yith-ampa-addons').find('.yith-ampa-addons-template-border').each(function(index, element){
     var value_index = $(element).find('.yith-ampa-addons-index').val();
      if(value_index >= index_mayor){
        index_mayor = value_index;
      } 
    });
    index = parseInt(index_mayor) + 1;

    
    var totalContainer = $('.container-addons');
    var addOnsContainer = totalContainer.find('.yith-ampa-addons');
    var addonTemplate = totalContainer.find('.yith-ampa-addons-template');
    var newAddon = $(addonTemplate.html().replaceAll('{{INDEX}}', index));
    var newAddonIndex = newAddon.find('.yith-ampa-addons-index');
    newAddonIndex.attr('value', index);
    addOnsContainer.append( newAddon );
    newAddon.removeClass('hidden-addons-default');

    index++;
  }
  $(document).on('click', '.yith-ampa-button-addon', addNewAddon);


  // Modificamos el titulo de cada Addon cuando se cambie el valor del input name
  $('body').on('input', '.yith-ampa-change-text', function () {
    var text_content = $(this).val();
    if (text_content == '') {
      text_content = 'Untitled'
    }
    var body_element = ($(this).closest('.yith-ampa-addons-body'));
    var head_element = body_element.siblings('.yith-ampa-addons-head');
    head_element.find('.yith-ampa-title-addons').text(text_content);

  });

  /***************************************************************************************** */
  //Hacemos el dropdown al addon
  $(document).on('click', '.yith-ampa-title-addons', function () {
    var parent_click = $(this).parent();
    var boddy_toggle = parent_click.parent().find('.yith-ampa-addons-body');


    //Cuando queramos que se desplace el listado de opciones solo se verás ciertas opciones dependiendo de los elementosmarcados, por ello, se hace lo siguiente


    /** Se llama a la función por si los valores recogidos por la bbdd son diferentes o bien, se estan puestos los valores por defecto sin cambiarse */

    /**Vemos en primara instancia cuando se hace el dropdown */
    var radio_free_input = boddy_toggle.find('.yith-ampa-radio-free input');
    var radio_fixed_input = boddy_toggle.find('.yith-ampa-radio-fixed input');
    var radio_char_input = boddy_toggle.find('.yith-ampa-radio-char input');

    var radio_char_input_class = boddy_toggle.find('.yith-ampa-radio-char');
    var select_type_field_class = boddy_toggle.find('.yith-ampa-select-type-field');
    var options_addons_class = boddy_toggle.find('.yith-ampa-options-base');
    var options_addons_price_class = boddy_toggle.find('.yith-option-price');
    var number_character_class = boddy_toggle.find('.yith-ampa-character-number');
    var price_text_class = boddy_toggle.find('.yith-ampa-text-price');

    if(radio_free_input.attr('checked')){
      radio_free_input.prop('checked', true);
    }
    if(radio_fixed_input.attr('checked')){
      radio_fixed_input.prop('checked', true);
    }
    if(radio_char_input.attr('checked')){
      radio_char_input.prop('checked', true);
    }

    //comprobamos el select para mostrar u ocultar el radio per character
    if (select_type_field_class.val() == 'text' || select_type_field_class.val() == 'textarea') {

      radio_char_input_class.show();
    } else {
      radio_char_input_class.hide();
    }

    // Comprobamos el select para mostrar u ocultar las opciones
    if (select_type_field_class.val() == 'radio' || select_type_field_class.val() == 'select') {

      options_addons_class.show();
      boddy_toggle.find('.yith-ampa-container').show();
      boddy_toggle.find('.yith-ampa-new-option').show();
      price_text_class.hide();
    } else {

      options_addons_class.hide();
      boddy_toggle.find('.yith-ampa-container').hide();
      boddy_toggle.find('.yith-ampa-new-option').hide();
      price_text_class.show();
    }

    // Comprobamos si el input radio "free" esta activo para mostrar u ocultar el campo price text
    if (radio_free_input.prop('checked')) {
      options_addons_price_class.show();
      price_text_class.hide();
      number_character_class.hide();
    } else {
      options_addons_price_class.hide();
     // price_text_class.show();
      // Si no esta "free" acitvo comprobamos el valor del select para mostrar u ocultar el campo name de las opciones
      if (select_type_field_class.val() == 'text' || select_type_field_class.val() == 'textarea') {
        number_character_class.show();
      } else {
        number_character_class.hide();
      }
    }

    if (select_type_field_class.val() == 'checkbox' || select_type_field_class.val() == 'onoff') {

      boddy_toggle.find('.yith-ampa-enable-default-base').show();
    } else {
      boddy_toggle.find('.yith-ampa-enable-default-base').hide();
    }

    /** Fin de las comprobaciones sin que existan cambios en las opciones */

    /**Ejecutamos la opcion slideToggle */
    boddy_toggle.slideToggle("slow", function () {

    });

  });


  /** Comprobación cuando se cambia de SELECT */
  $('body').on('change', '.yith-ampa-select-type-field', function () {

    var type_field = $('option:selected', this).val();


    var options_groups_element = ($(this).closest('.options_group'));
    var radio_character_element = options_groups_element.find('.yith-ampa-radio-char');
    var radio_fixed_input = options_groups_element.find('.yith-ampa-radio-fixed input');
    var radio_free_input = options_groups_element.find('.yith-ampa-radio-free input');

    if (type_field == 'text' || type_field == 'textarea') {
      radio_character_element.show();

      if (radio_free_input.prop('checked')) {

        options_groups_element.find('.yith-ampa-character-number').hide();
      } else {

        options_groups_element.find('.yith-ampa-character-number').show();
      }

    } else {
      radio_character_element.hide();
    }

    if (type_field == 'radio' || type_field == 'select') {

      options_groups_element.find('.yith-ampa-text-price').hide();
      options_groups_element.find('.yith-ampa-character-number').hide();

      options_groups_element.find('.yith-ampa-options-base').show();
      options_groups_element.find('.yith-ampa-new-option').show();
      options_groups_element.find('.yith-ampa-container').show();// no sñe por qué

      if (radio_fixed_input.prop('checked')) {
        options_groups_element.find('.yith-option-price').show();
      } else {
        options_groups_element.find('.yith-option-price').hide();
      }

    } else {

      options_groups_element.find('.yith-ampa-options-base').hide();
      options_groups_element.find('.yith-ampa-new-option').hide();
      options_groups_element.find('.yith-ampa-container').hide(); // no sñe por qué

      if (radio_free_input.prop('checked')) {
        options_groups_element.find('.yith-ampa-text-price').hide();
      } else {
        options_groups_element.find('.yith-ampa-text-price').show();
      }
    }

    if (type_field == 'checkbox' || type_field == 'onoff') {

      options_groups_element.find('.yith-ampa-enable-default-base').show();
    } else {
      options_groups_element.find('.yith-ampa-enable-default-base').hide();
    }

  });

  /****************************************************************** */

  /**Comprobaciónes cuando se cambia de radio */

  $('body').on('change', '.yith-ampa-radio-free', function () {
    // Cuando el valor cambie a FREE. se tiene que ocultar el input texto del precio de las opciones y el text precio normal y tambien el input number

    var options_group = $(this).parent();
    options_group.find('.yith-ampa-text-price').hide();  //ocultamos el text price 
    options_group.find('.yith-ampa-character-number').hide(); // ocultamos input number
    options_group.find('.yith-option-price').hide(); // ocultamos la opcion precio
  });


  $('body').on('change', '.yith-ampa-radio-char', function () {
    var options_group = $(this).parent();
    var select_type_field_class = options_group.find('.yith-ampa-select-type-field option:selected').val();

    options_group.find('.yith-option-price').hide(); // ocultamos la opcion precio

    // Si es fixed pero el select es radio o select se oculta el text price
    if (select_type_field_class == 'select' || select_type_field_class == 'radio') {
      options_group.find('.yith-ampa-text-price').hide();
    } else {
      options_group.find('.yith-ampa-text-price').show();
    }

    // Si es fixed pero el select es text o textarea se muestra el input number character sino se oculta
    if (select_type_field_class == 'text' || select_type_field_class == 'textarea') {

      options_group.find('.yith-ampa-character-number').show();
    } else {
      options_group.find('.yith-ampa-character-number').hide();
    }

  });


  $('body').on('change', '.yith-ampa-radio-fixed', function () {

    var options_group = $(this).parent();
    var select_type_field_class = options_group.find('.yith-ampa-select-type-field option:selected').val();
    //options_group.find('.yith-option-price').show(); // mostramos la opcion precio

    // Si es fixed pero el select es radio o select se oculta el text price
    if (select_type_field_class == 'select' || select_type_field_class == 'radio') {

      //options_group.find('.yith-ampa-text-price').show();
      options_group.find('.yith-option-price').show();
      options_group.find('.yith-ampa-character-number').hide();
      options_group.find('.yith-ampa-text-price').hide();
    } 

    // Si es fixed pero el select es text o textarea se muestra el input number character sino se oculta
    if (select_type_field_class == 'text' || select_type_field_class == 'textarea') {

      options_group.find('.yith-ampa-character-number').show();
      options_group.find('.yith-ampa-text-price').show();
      options_group.find('.yith-option-price').hide();
    }

    // Si es fixed pero el select es checkbox o onoff se muestra el input number character sino se oculta
    if (select_type_field_class == 'checkbox' || select_type_field_class == 'onoff') {

      options_group.find('.yith-ampa-character-number').hide();
      options_group.find('.yith-ampa-text-price').show();
      options_group.find('.yith-option-price').hide()
    } 
  });



  $('body').on('click', '.yith-ampa-new-option', function () {

    var options_group = $(this).parent();
    var cloneStructOptions = options_group.find('.yith-ampa-struct-options:first').clone();

    options_group.find('.yith-ampa-container').append(cloneStructOptions);

  });



  // Eliminamos la opcion que queremos eliminar
  $('body').on('click', '.yith-ampa-icon-trash', function () {
    var struct_option = $(this).parent();
    var num_icon_trash = struct_option.parent().find('.yith-ampa-icon-trash').length

    if (num_icon_trash > 1) {
      $(this).parents('.yith-ampa-struct-options').remove();
    }

  });


  // Sorteable Addons
  $(function () {
    $('.yith-ampa-addons').sortable();
    $('.yith-ampa-addons').disableSelection();
  });


  // Remove Addon
  $('body').on('click', '.yith-ampa-button-remove-addon', function () {
    var yith_ampa_addons = $(this).parent().parent().parent().parent();
    var value_index_remove = yith_ampa_addons.find('.yith-ampa-addons-index').val();
console.log(value_index_remove);
    yith_ampa_addons.remove();

    $('.yith-ampa-addons').find('.yith-ampa-addons-template-border').each(function(index, element){
      var value_index = $(element).find('.yith-ampa-addons-index').val();
       if(value_index > value_index_remove){
        $(element).find('.yith-ampa-addons-index').val(parseInt(value_index) - 1);
       
        var value_index_new = value_index - 1;
        $(element).html().replaceAll('yith-ampa-addon['+ value_index +']', 'yith-ampa-addon['+ value_index_new +']');
       // $(element).html($(element).html().replaceAll('yith-ampa-addon['+ value_index +']', 'yith-ampa-addon['+ value_index_new +']'));
       // $(element).html($(element).html().replaceAll('{{INDEX}}', value_index_new));
      } 
     });    
  });

});


