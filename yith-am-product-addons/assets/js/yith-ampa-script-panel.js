jQuery(document).ready(function ($) {

  //Iniciallizamoss algunas variables
  var index = 0;

  //ocultamos el body de los addons
  $('.yith-ampa-addons-body').hide();


  addNewAddon = function () {
    //Cogemos el valor del index mayor de todos los addons para si creamos más addons no se pisen y continuen por el valor que le correspondría
    var index_mayor = 0;
    $('.yith-ampa-addons').find('.yith-ampa-addons-template-border').each(function(index, element){
     var value_index = $(element).find('.yith-ampa-addons-index').val();
      if(value_index >= index_mayor){
        index_mayor = value_index;
      } 
    });
    index = parseInt(index_mayor) + 1;

    
    var totalContainer = $('.container-addons');
    var addOnsContainer = totalContainer.find('.yith-ampa-addons');
    var addonTemplate = totalContainer.find('.yith-ampa-addons-template');
    var newAddon = $(addonTemplate.html().replaceAll('{{INDEX}}', index));
    var newAddonIndex = newAddon.find('.yith-ampa-addons-index');
    newAddonIndex.attr('value', index);
    addOnsContainer.append( newAddon );
    newAddon.removeClass('hidden-addons-default');

    index++;
  }
  $(document).on('click', '.yith-ampa-button-addon', addNewAddon);


  // Modificamos el titulo de cada Addon cuando se cambie el valor del input name
  $('body').on('input', '.yith-ampa-change-text', function () {
    var text_content = $(this).val();
    if (text_content == '') {
      text_content = 'Untitled'
    }
    var body_element = ($(this).closest('.yith-ampa-addons-body'));
    var head_element = body_element.siblings('.yith-ampa-addons-head');
    head_element.find('.yith-ampa-title-addons').text(text_content);
  });


  // Hacemos el dropdown al addon xuando hacemos click en el titulo del addon
  $(document).on('click', '.yith-ampa-title-addons', function () {
    var parent_click = $(this).parent();
    var boddy_toggle = parent_click.parent().find('.yith-ampa-addons-body');


    /** Se hace lo siguiente por si los valores recogidos por la bbdd son diferentes o bien, se estan puestos los valores por defecto sin cambiarse */

    /**Vemos en primara instancia cuando se hace el dropdown */
    var radio_free_input = boddy_toggle.find('.yith-ampa-radio-free input');
    var radio_fixed_input = boddy_toggle.find('.yith-ampa-radio-fixed input');
    var radio_char_input = boddy_toggle.find('.yith-ampa-radio-char input');

    var radio_char_input_class = boddy_toggle.find('.yith-ampa-radio-char');
    var select_type_field_class = boddy_toggle.find('.yith-ampa-select-type-field');
    var options_addons_class = boddy_toggle.find('.yith-ampa-options-base');
    var options_addons_price_class = boddy_toggle.find('.yith-option-price');
    var number_character_class = boddy_toggle.find('.yith-ampa-character-number');
    var price_text_class = boddy_toggle.find('.yith-ampa-text-price');

    if(radio_free_input.attr('checked')){
      radio_free_input.prop('checked', true);
    
    }
    if(radio_fixed_input.attr('checked')){
      radio_fixed_input.prop('checked', true);
    }
    if(radio_char_input.attr('checked')){
      radio_char_input.prop('checked', true);
    }

    // Comprobamos el select para los valores TEXT OR TEXTAREA para mostrar u ocultar las opciones
    if (select_type_field_class.val() == 'text' || select_type_field_class.val() == 'textarea') {

      radio_char_input_class.show(); // Mostramos el radio per character
      options_addons_class.hide(); // Ocultamos las opciones de los addons
      boddy_toggle.find('.yith-ampa-container').hide(); // Ocultamos el container de las opciones
      boddy_toggle.find('.yith-ampa-new-option').hide(); // Ocultamos el texto de añadir una nueva opcion
      price_text_class.show(); // Mostramos el input price a priori, luego se comprueba si el radio es free
      boddy_toggle.find('.yith-ampa-enable-default-base').hide(); // Ocultamos el input de los checkbox/onoff

      if(radio_free_input.prop('checked')){ // Si el radio input esta en check
        number_character_class.hide(); // Ocultamos el input free_character
        price_text_class.hide(); // Ocultamos el input price
      }
    }

    // Comprobamos el select para los valores RADIO OR SELECT mostrar u ocultar las opciones
    if (select_type_field_class.val() == 'radio' || select_type_field_class.val() == 'select') {

      radio_char_input_class.hide(); // Ocultamos el radio per character
      options_addons_class.show(); // Mostramos las opciones de los addons
      boddy_toggle.find('.yith-ampa-container').show();  // Mostramos el container de las opciones
      boddy_toggle.find('.yith-ampa-new-option').show(); // Mostramos el texto de añadir una nueva opcion
      boddy_toggle.find('.yith-ampa-enable-default-base').hide(); // Ocultamos el input de los checkbox/onoff
      number_character_class.hide(); // Ocultamos el input free_character
      price_text_class.hide(); // Ocultamos el input del price text

      if(radio_free_input.prop('checked')){ // Si el radio input esta en check
        boddy_toggle.find('.yith-option-price').hide(); //Si esta a free ocultamos el input price de la opcion
        price_text_class.hide(); // Ocultamos el input price
      }
    
    
    } 

    // Comprobamos el select para los valores CHECKBOX OR ONOFF mostrar u ocultar las opciones
    if (select_type_field_class.val() == 'checkbox' || select_type_field_class.val() == 'onoff') {

      radio_char_input_class.hide(); // Ocultamos el radio per character
      options_addons_class.hide(); // Ocultamos las opciones de los addons
      price_text_class.show(); // Mostramos el input del price text
      boddy_toggle.find('.yith-ampa-enable-default-base').show(); // Mostramos el input de los checkbox/onoff
      boddy_toggle.find('.yith-ampa-new-option').hide(); // Ocultamos el texto de añadir una nueva opcion
      number_character_class.hide();

      
      if(radio_free_input.prop('checked')){
        price_text_class.hide(); // Mostramos el input del price text
      }
    }   

    /** Fin de las comprobaciones sin que existan cambios en las opciones */

    /**Ejecutamos la opcion slideToggle */
    boddy_toggle.slideToggle("slow", function () {

    });

  });

/******************************************************************************************** */
  /** Comprobación cuando se cambia de SELECT */
  $('body').on('change', '.yith-ampa-select-type-field', function () {

    var type_field = $('option:selected', this).val();


    var options_groups_element = ($(this).closest('.options_group'));
    var radio_character_element = options_groups_element.find('.yith-ampa-radio-char');
    var radio_fixed_input = options_groups_element.find('.yith-ampa-radio-fixed input');
    var radio_free_input = options_groups_element.find('.yith-ampa-radio-free input');

    if (type_field == 'text' || type_field == 'textarea') {
      radio_character_element.show();

      if (radio_free_input.prop('checked')) {

        options_groups_element.find('.yith-ampa-character-number').hide();
      } else {

        options_groups_element.find('.yith-ampa-character-number').show();
      }

    } else {
      radio_character_element.hide();
    }

    if (type_field == 'radio' || type_field == 'select') {

      options_groups_element.find('.yith-ampa-text-price').hide();
      options_groups_element.find('.yith-ampa-character-number').hide();

      options_groups_element.find('.yith-ampa-options-base').show();
      options_groups_element.find('.yith-ampa-new-option').show();
      options_groups_element.find('.yith-ampa-container').show();

      if (radio_fixed_input.prop('checked')) {
        options_groups_element.find('.yith-option-price').show();
      } else {
        options_groups_element.find('.yith-option-price').hide();
      }

    } else {

      options_groups_element.find('.yith-ampa-options-base').hide();
      options_groups_element.find('.yith-ampa-new-option').hide();
      options_groups_element.find('.yith-ampa-container').hide();

      if (radio_free_input.prop('checked')) {
        options_groups_element.find('.yith-ampa-text-price').hide();
      } else {
        options_groups_element.find('.yith-ampa-text-price').show();
      }
    }

    if (type_field == 'checkbox' || type_field == 'onoff') {

      options_groups_element.find('.yith-ampa-enable-default-base').show();
    } else {
      options_groups_element.find('.yith-ampa-enable-default-base').hide();
    }

  });

  /****************************************************************** */

  /**Comprobaciónes cuando se cambia de radio */

  $('body').on('change', '.yith-ampa-radio-free', function () {
    // Cuando el valor cambie a FREE. se tiene que ocultar el input texto del precio de las opciones y el text precio normal y tambien el input number

    var options_group = $(this).parent();
    options_group.find('.yith-ampa-text-price').hide();  //ocultamos el text price 
    options_group.find('.yith-ampa-character-number').hide(); // ocultamos input number
    options_group.find('.yith-option-price').hide(); // ocultamos la opcion precio
  });


  $('body').on('change', '.yith-ampa-radio-char', function () {
    var options_group = $(this).parent();
    var select_type_field_class = options_group.find('.yith-ampa-select-type-field option:selected').val();

    options_group.find('.yith-option-price').hide(); // ocultamos la opcion precio

    // Si es fixed pero el select es radio o select se oculta el text price
    if (select_type_field_class == 'select' || select_type_field_class == 'radio') {
      options_group.find('.yith-ampa-text-price').hide();
    } else {
      options_group.find('.yith-ampa-text-price').show();
    }

    // Si es fixed pero el select es text o textarea se muestra el input number character sino se oculta
    if (select_type_field_class == 'text' || select_type_field_class == 'textarea') {

      options_group.find('.yith-ampa-character-number').show();
    } else {
      options_group.find('.yith-ampa-character-number').hide();
    }

  });


  $('body').on('change', '.yith-ampa-radio-fixed', function () {

    var options_group = $(this).parent();
    var select_type_field_class = options_group.find('.yith-ampa-select-type-field option:selected').val();
    //options_group.find('.yith-option-price').show(); // mostramos la opcion precio

    // Si es fixed pero el select es radio o select se oculta el text price
    if (select_type_field_class == 'select' || select_type_field_class == 'radio') {

      //options_group.find('.yith-ampa-text-price').show();
      options_group.find('.yith-option-price').show();
      options_group.find('.yith-ampa-character-number').hide();
      options_group.find('.yith-ampa-text-price').hide();
    } 

    // Si es fixed pero el select es text o textarea se muestra el input number character sino se oculta
    if (select_type_field_class == 'text' || select_type_field_class == 'textarea') {

      options_group.find('.yith-ampa-character-number').show();
      options_group.find('.yith-ampa-text-price').show();
      options_group.find('.yith-option-price').hide();
    }

    // Si es fixed pero el select es checkbox o onoff se muestra el input number character sino se oculta
    if (select_type_field_class == 'checkbox' || select_type_field_class == 'onoff') {

      options_group.find('.yith-ampa-character-number').hide();
      options_group.find('.yith-ampa-text-price').show();
      options_group.find('.yith-option-price').hide()
    } 
  });



  $('body').on('click', '.yith-ampa-new-option', function () {

    var options_group = $(this).parent();
    var cloneStructOptions = options_group.find('.yith-ampa-struct-options:first').clone();

    options_group.find('.yith-ampa-container').append(cloneStructOptions);

  });



  // Eliminamos la opcion que queremos eliminar
  $('body').on('click', '.yith-ampa-icon-trash', function () {
    var struct_option = $(this).parent();
    var num_icon_trash = struct_option.parent().find('.yith-ampa-icon-trash').length

    if (num_icon_trash > 1) {
      $(this).parents('.yith-ampa-struct-options').remove();
    }

  });


  // Sorteable Addons
  $(function () {
    $('.yith-ampa-addons').sortable();
    $('.yith-ampa-addons').disableSelection();
  });


  // Remove Addon
  $('body').on('click', '.yith-ampa-button-remove-addon', function () {
    var yith_ampa_addons = $(this).parent().parent().parent().parent();
    var value_index_remove = yith_ampa_addons.find('.yith-ampa-addons-index').val();

    yith_ampa_addons.remove();

    // Modificamos a los addons con el value superior al addons que se borra y le restamos 1 para tener los indices ordenados
    $('.yith-ampa-addons').find('.yith-ampa-addons-template-border').each(function(index, element){
      var value_index = $(element).find('.yith-ampa-addons-index').val();
       if(value_index > value_index_remove){
        $(element).find('.yith-ampa-addons-index').val(parseInt(value_index) - 1);
       
        var value_index_new = value_index - 1;
        $(element).html().replaceAll('yith-ampa-addon['+ value_index +']', 'yith-ampa-addon['+ value_index_new +']');
      } 
     });    
  });

});


