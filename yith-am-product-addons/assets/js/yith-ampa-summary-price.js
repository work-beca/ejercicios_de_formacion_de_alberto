jQuery(document).ready(function ($) {



  var value_total_org = 0;
  var value_price_org = $('.product-price-number').text();

  $('.total-price-number').text(value_price_org);

  // Comprobamos si el checkbox esta a checked cuando se cargue la pagina
  if ($('.yith-ampa-input-checkbox-frontend').attr('checked')) {

    var value_price_add = $('.yith-ampa-input-checkbox-frontend').parent().parent().find('.yith-ampa-SelectCheckbox-text-price').val();

    $('.yith-ampa-price-addon-checkbox-label').text('+ ' + value_price_add);

  } else {
    $('.yith-ampa-price-addon-checkbox-label').text('+ 0 euros');
  }

  // Comprobamos si el onoff esta a checked cuando se cargue la pagina
  if ($('.yith-ampa-input-onoff-frontend').attr('checked')) {

    var value_price_add = $('.yith-ampa-input-onoff-frontend').parent().parent().parent().find('.yith-ampa-SelectOnoff-text-price').val();

    $('.yith-ampa-price-addon-onoff-label').text('+ ' + value_price_add);

  } else {
    $('.yith-ampa-price-addon-onoff-label').text('+ 0 euros');
  }

  // Cuando se modifique un addon del tipo checkbox se añadirá el precio de ese addon al total de los addons en el summary

  $('body').on('change', '.yith-ampa-input-checkbox-frontend', function () {

    var yith_ampa_addon_field_type = $(this).parent();
    var value_price_add = yith_ampa_addon_field_type.parent().find('.yith-ampa-SelectCheckbox-text-price').val();


    if ($(this).prop('checked')) {
      yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-checkbox-label').text('+ ' + value_price_add);

    } else {
      yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-checkbox-label').text('+ 0 euros');

    }

    total_price_addons();
  });


  $('body').on('change', '.yith-ampa-input-onoff-frontend', function () {

    var switch_new = $(this).parent();
    var yith_ampa_addon_field_type = switch_new.parent();
    var value_price_add = yith_ampa_addon_field_type.parent().find('.yith-ampa-SelectOnoff-text-price').val();

    if ($(this).prop('checked')) {

      yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-onoff-label').text('+ ' + value_price_add);
    } else {

      yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-onoff-label').text('+ 0 euros');
    }
    total_price_addons();
  });


  /** Cuando el campo text se modifica */

  $('.yith-ampa-price-addon-text-label').text('+ 0 euros');
  $('.yith-ampa-input-text-frontend').on('input', function () {

    var yith_ampa_addon_field_type = $(this).parent();
    var index_addon = yith_ampa_addon_field_type.parent().find('.yith-ampa-addon-index-frontend').val();

    if (addons[index_addon]['price_setting'] == 'fixed') {

      tam_word = $('.yith-ampa-input-text-frontend').val().length;

      if (tam_word > parseInt(addons[index_addon]['free_chars'])) {

        yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-text-label').text('+ ' + addons[index_addon]['price']);
      } else {
        yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-text-label').text('+ 0 euros');
      }
    }

    if (addons[index_addon]['price_setting'] == 'character') {
      tam_word = $('.yith-ampa-input-text-frontend').val().length;

      if (tam_word > parseInt(addons[index_addon]['free_chars'])) {
        var diff_input_word = tam_word - parseInt(addons[index_addon]['free_chars']);
        var price_text_total = diff_input_word * parseInt(addons[index_addon]['price'].split(" ", 1));
        yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-text-label').text('+ ' + price_text_total + ' ' + addons[index_addon]['price'].substr(2));
        yith_ampa_addon_field_type.parent().find('.yith-ampa-SelectText-text-price').val(price_text_total + ' euros');
      } else {
        yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-text-label').text('+ 0 euros');
      }
    }

    total_price_addons();
  });



  /** Cuando el campo textarea se modifica */

  $('.yith-ampa-price-addon-textarea-label').text('+ 0 euros');
  $('.yith-ampa-input-textarea-frontend').on('input', function () {
    var yith_ampa_addon_field_type = $(this).parent();
    var index_addon = yith_ampa_addon_field_type.parent().find('.yith-ampa-addon-index-frontend').val();


    if (addons[index_addon]['price_setting'] == 'fixed') {
      tam_word = $('.yith-ampa-input-textarea-frontend').val().length;
      if (tam_word > parseInt(addons[index_addon]['free_chars'])) {

        yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-textarea-label').text('+ ' + addons[index_addon]['price']);
      } else {
        yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-textarea-label').text('+ 0 euros');
      }
    }

    if (addons[index_addon]['price_setting'] == 'character') {
      tam_word = $('.yith-ampa-input-textarea-frontend').val().length;

      if (tam_word > parseInt(addons[index_addon]['free_chars'])) {
        var diff_input_word = tam_word - parseInt(addons[index_addon]['free_chars']);
        var price_text_total = diff_input_word * parseInt(addons[index_addon]['price'].split(" ", 1));
        yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-textarea-label').text('+ ' + price_text_total + ' ' + addons[index_addon]['price'].substr(2));
        yith_ampa_addon_field_type.parent().find('.yith-ampa-SelectTextarea-text-price').val(price_text_total + ' euros');
      } else {
        yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-textarea-label').text('+ 0 euros');
      }
    }

    total_price_addons();
  });




  $("body").on('DOMSubtreeModified', ".additional-options-price-number", function () {

    var value_price_product = $('.product-price-number').text();

    value_price_product = parseInt(value_price_product.substr(1));
    var additional_price_text = $(this).text().split(' ');
    value_total_org = value_price_product + parseInt(additional_price_text[0]);
    $('.total-price-number').text(value_total_org + ' ' + additional_price_text[1]);
  });





  function total_price_addons() {

    var total_price_addons = 0;
    var moneda;
    var value_label_text;
    $('.yith-ampa-value-label').each(function (indice, elemento) {

      value_label_text = $(elemento).text().split(' ');
     
      coin_value = value_label_text[2];
      total_price_addons += parseInt(value_label_text[1]);

    });

    $('.additional-options-price-number').text(total_price_addons + ' ' + coin_value);
  }



  // Cuando se modifique un addon del tipo radio se añadirá el precio de ese addon al total de los addons en el summary
  $('.yith-ampa-price-addon-radio-label').text('+ 0 euros');
  $('body').on('change', '.yith-ampa-input-radio-frontend', function () {


    var yith_ampa_addon_field_type = $(this).parent().parent();
    var index_addon = yith_ampa_addon_field_type.parent().find('.yith-ampa-addon-index-frontend').val();

    if (addons[index_addon]['price_setting'] == 'free') {
      yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-radio-label').text('+ 0 euros');
    } else {

      var value_selected_split = $(this).val().split('+');
      var price_selected = value_selected_split[1];

      yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-radio-label').text('+' + price_selected);
    }
    yith_ampa_addon_field_type.parent().find('.yith-ampa-SelectOptionsRadio-text-price').val('+' + price_selected);

    total_price_addons();
  });



  
 // Comprobamos el valor del select sin que se haya cambiado
  if ($('.yith-ampa-input-select-frontend').val().split('+')[1]){
    $('.yith-ampa-price-addon-select-label').text('+' + $('.yith-ampa-input-select-frontend').val().split('+')[1]);
    $('.yith-ampa-SelectOptionsSelect-text-price').val('+' + $('.yith-ampa-input-select-frontend').val().split('+')[1]);
  }else {
    $('.yith-ampa-price-addon-select-label').text('+ 0 euros');
    $('.yith-ampa-SelectOptionsSelect-text-price').val('+ ' + 0 + ' euros');
  }
// Cuando se modifique un addon del tipo select se añadirá el precio de ese addon al total de los addons en el summary
$('body').on('change', '.yith-ampa-input-select-frontend', function () {


  var yith_ampa_addon_field_type = $(this).parent();
  var index_addon = yith_ampa_addon_field_type.parent().find('.yith-ampa-addon-index-frontend').val();

  if (addons[index_addon]['price_setting'] == 'fixed') {

    var value_selected_split = $(this).val().split('+');
    var price_selected = value_selected_split[1];

    yith_ampa_addon_field_type.parent().find('.yith-ampa-price-addon-select-label').text('+' + price_selected);
  }

  yith_ampa_addon_field_type.parent().find('.yith-ampa-SelectOptionsSelect-text-price').val('+' + price_selected);

  total_price_addons();
});


// Valores pord efecto del Addtional options total y el Total
$('.additional-options-price-number').text(0 + ' euros');
$('.total-price-number').text(0 + ' euros');

total_price_addons(); 


});