jQuery(document).ready(function ($) {

    $('#my-button').on('click', function () {

        //La llamada AJAX
        $.ajax({
            type: "post",
            url: ajax_var.url, // Pon aquí tu URL
            data: {
                action: 'notify_button_click',
                message: 'Button has been clicked!'
            },
            error: function (response) {
                console.log(response);
            },
            success: function (response) {
                // Actualiza el mensaje con la respuesta
                $('#txtMessage').text(response.message);
            }
        })

    });
});