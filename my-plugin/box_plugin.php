<?php

/*
Plugin Name: box-plugin
Plugin URI: https://boxplugin.es
Description: Plugin que añade un box con informacion de un post
Version: 1.0
Author: Alberto
Author URI: https://boxplugin.es
License: GPL2
*/

function my_front_end_scripts(){

    
    wp_register_style('style', plugin_dir_url(__FILE__)."assets/css/style.css" , false);
    wp_enqueue_style('style');

}
add_action('wp_enqueue_scripts', 'my_front_end_scripts');



function yith_create_box($content){

    
    global $post;
    $author_post = get_the_author_meta('nickname');
    $date_post = $post->post_date;

   if(!is_home() && !is_feed() && is_single()){
    $content.="<div class = 'div-button-box'> 
                <a href='#modalInfo'><p class = 'link-pop'> Show Post Info</p> </a>
                </div>  
                <div id='modalInfo' class='modal'>
                    <div class='modal-contenido'>
                        <a href='#'><p id = 'cross'>x</p></a>
                        <h2>Post information</h2>
                        <p>The author of post is ".$author_post." </p>
                        <p> The date of post is ".$date_post."</p>
                    </div>  
                </div>";
   }
    

    return $content;
}
add_filter('the_content', 'yith_create_box');



/**
 * *************************************
 * CREATING SHORTCODES
 * *************************************
 */


/**
 * 
 * Creating a shortcode
 * 
 * [current_year] returns the Current Year as a 4-digit string.
 * @return string Current Year
 * 
 */

    add_shortcode('yith_current_year', 'salcodes_year');

    function salcodes_init(){
        function salcodes_year(){
           //error_log (print_r(getdate()['year']));
            return getdate()['year'];
        }
    }
    add_action('init', 'salcodes_init');


/**
 * Creating a shortcode
 * 
 * yith_last_three_post return the last three post published.
 * @return tag three last post
 * 
 */

add_shortcode('yith_last_two_post', 'list_two_post');

function ready_post(){
    function list_two_post(){

        global $wpdb;

        $output = "";
        $result = wp_get_recent_posts(array('numberposts'=>2));
        //$result = $wpdb->get_results("select post_title from `$wpdb->posts` where post_status = 'publish' order by `post_date` desc limit 2");

        foreach($result as $two_last_post){

           
            $title_post_ = $two_last_post['post_title'];
            if($title_post_ != ""){
                $output.= '<li>' .$title_post_. '</li>';     
            }
           
        }
	
	    return '<ul>'. $output .'</ul>';
    }
}
add_action('init', 'ready_post');


/**
 * 
 * Creating a shortcode that show details of a user
 * 
 * yith_details_user show details of a user
 * @return tag list of details
 * 
 */

//do_action('yith_before_details_user', $name_title);
add_shortcode('yith_details_user','yith_handle_details_user');
//do_action('yith_after_details_user');

function ready_init(){
    function yith_handle_details_user( $atts=[] ){

        $user_id = $atts['userid'];        

        $output = "";

        $first_name_user = get_user_meta($user_id, 'first_name');
        $last_name_user = get_user_meta($user_id, 'last_name');
        $nickname_user = get_user_meta($user_id, 'nickname');
        
       

        $output.= '<p> Details of user: </p>';
        $output.= '<ul><li> Name: '.$first_name_user[0].'</li>';
        $output.= '<li> Last Name: '.$last_name_user[0].'</li>';
        $output.= '<li> Nickname: '.$nickname_user[0].'</li></ul>'; 

        return $output;
    }
}
add_action('init', 'ready_init');


/**
 * 
 * Creating a shortcode that show the content of post
 * 
 * yith_content_post show content of post
 * @return content content of post
 * 
 */

function ready_to_show_content_post(){
    add_shortcode('yith_content_post', 'yith_handler_content_post');
}
add_action('init', 'ready_to_show_content_post');


function yith_handler_content_post($atts = []){

    ob_start();

    //$post_id_ = $atts['postid'];

    $post_id_ = apply_filters( 'yith_alberto_post_id', $atts['postid'] );
    $post_ = get_post($post_id_);
    
    do_action('yith_before_content_post' );
    if($post_){
        echo $post_->post_content;    
    }
    else{
        echo '<p class="error-post"> Ha habido un error al cargar el post </p>';
    }
    do_action('yith_after_content_post',$post_id_);
    
    return ob_get_clean();
}

function yith_new_id_post($postid){

    $postid = 16;
    return $postid;
}
add_filter('yith_alberto_post_id', 'yith_new_id_post');

/**
 * *************************************
 * CREATING WIDGETS
 * *************************************
 */




/**
 * 
 * CREATING A WIDGET WITH DIFERENT FIELDS
 * 
 */

class Contact_widget extends WP_Widget{

    public function __construct()
    {
        // actual widget
        parent::__construct(
            'data-contact', // Base ID
            'Data Contact' // Name
        );
    }

    public $args = array(
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
        'before_widget' => '<div class="widget-wrap">',
        'after_widget'  => '</div></div>'
    );

    public function widget($args, $instance)
    {
        // outputs the content of the widget

        echo $args['before_widget'];

        /*if(!empty($instance['title'])){
            echo $args['before_title'].apply_filters('widget_title', $instance['title']).$args['after_title'];
        }*/
        
        $name_html = esc_html__($instance['name'], 'text_domain');
        $last_name_html = esc_html__($instance['last_name'], 'text_domain');
        $address_html = esc_html__($instance['address'], 'text_domain');
        $phone_html = esc_html__($instance['phone'], 'text_domain');

        echo '<div class ="textwidget">';

            echo '<p class="title-widgets"> Data Contact: </p>';
            echo '<p> Me llamo <i>'.$name_html. '  '.$last_name_html.'</i></p>';
            echo    '<p> Vivo en <i>'.$address_html.'</i> y mi número de teléfono es </p>'.
                '<p><i>'.$phone_html.'</i></p>';
        echo '</div>';
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        // outputs the options form in the admin

        $name = !empty($instance['name']) ? $instance['name']: esc_html__('', 'text_domain');
        $last_name = !empty($instance['last_name']) ? $instance['last_name'] : esc_html__('','text_domain');
        $address = !empty($instance['address']) ? $instance['address']: esc_html__('', 'text_domain');
        $phone = !empty($instance['phone']) ? $instance['phone']: esc_html__('', 'text_domain');
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('name')); ?>"> <?php echo esc_html__('Name:', 'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'name' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'name' ) ); ?>" type="text" value="<?php echo esc_attr( $name ); ?>" required>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'last_name' ) ); ?>"><?php echo esc_html__( 'Last_name:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'last_name' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'last_name' ) ); ?>" type="text" value="<?php echo esc_attr( $last_name ); ?>" required>
            
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>"><?php echo esc_html__( 'Address:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'address' ) ); ?>" type="text" value="<?php echo esc_attr( $address ); ?>" required>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>"><?php echo esc_html__( 'Phone:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'phone' ) ); ?>" type="tel" value="<?php echo esc_attr( $phone ); ?>" pattern="[0-9]{3}-[0-9]{3}-[0-9]{3}" required placeholder="123-456-789">
        </p>

        <?php
    }

    public function update($new_instance, $old_instance)
    {
        // processes widget options to be saved

        $instance = array();
 
        $instance['name'] = ( !empty( $new_instance['name'] ) ) ? strip_tags( $new_instance['name'] ) : '';
        $instance['last_name'] = ( !empty( $new_instance['last_name'] ) ) ? $new_instance['last_name'] : '';
        $instance['address'] = ( !empty( $new_instance['address'] ) ) ? $new_instance['address'] : '';
        $instance['phone'] = ( !empty( $new_instance['phone'] ) ) ? $new_instance['phone'] : '';

        return $instance;
    }

} // end to class My_Widget

/**
 * Register Contact_widget widget
 */
function yith_register_Contact_widget(){
    register_widget('Contact_widget');
}
add_action('widgets_init', 'yith_register_Contact_widget');

/**
 * 
 * CREATING A WIDGET that list the last two posts
 * 
 */

class Post_widget extends WP_Widget{

    public function __construct()
    {
        // actual widget
        parent::__construct(
            'List-Last-Post', // Base ID
            'List Last Post' // Name
        );
    }

    public $args = array(
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
        'before_widget' => '<div class="widget-wrap">',
        'after_widget'  => '</div></div>'
    );

    public function widget($args, $instance)
    {
        // outputs the content of the widget

        echo $args['before_widget'];

        /*if(!empty($instance['title'])){
            echo $args['before_title'].apply_filters('widget_title', $instance['title']).$args['after_title'];
        }*/
        
        $num_post = esc_html__($instance['num_post'], 'text_domain');
        $posts_recent = wp_get_recent_posts(array('numberposts'=>$num_post));
        
        echo '<div class ="textwidget">';
        echo '<p class="title-widgets">'.$instance['title'].': </p>';
        echo '<ul>';
        foreach($posts_recent as $post_){
            echo '<li ><a class="last-post" href="'.get_post_permalink($post_['ID']).'">'.$post_['post_title'].'</li>';
        }
    
        echo '</ul></div>';
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        // outputs the options form in the admin

        $title = !empty($instance['title']) ? $instance['title']: esc_html__('', 'text_domain');
        $num_post = !empty($instance['num_post']) ? $instance['num_post']: esc_html__('', 'text_domain');

        ?>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"> <?php echo esc_html__('Title:', 'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('num_post')); ?>"> <?php echo esc_html__('Num_post:', 'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'num_post' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'num_post' ) ); ?>" type="number" min="0" max="5" value="<?php echo esc_attr( $num_post ); ?>">
        </p>          

        <?php
    }

    public function update($new_instance, $old_instance)
    {
        // processes widget options to be saved

        $instance = array();
 
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['num_post'] = ( !empty( $new_instance['num_post'] ) ) ? $new_instance['num_post'] : '';


        return $instance;
    }

} // end to class Post_widget

/**
 * Register Contact_widget widget
 */
function yith_register_Post_widget(){
    register_widget('Post_widget');
}
add_action('widgets_init', 'yith_register_Post_widget');
?>