<?php
/**
 * File class admin
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AM_PRODUCT_RELEVANCE_FOR_WOOCOMMERCE_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}



if ( ! class_exists( 'YITH_AM_PRODUCT_RELEVANCE_FOR_WOOCOMMERCE_Admin' ) ) {
	/**
	 * YITH_AM_PRODUCT_RELEVANCE_FOR_WOOCOMMERCE_Admin
	 */
	class YITH_AM_PRODUCT_RELEVANCE_FOR_WOOCOMMERCE_Admin {

		/* @var YIT_Plugin_Panel_WooCommerce $_panel the panel */
		private $_panel; //phpcs:ignore



		/* @var Panel page . */
		protected $_panel_page = 'yith-am-product-relevance-for-woocommerce'; //phpcs:ignore

		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AM_PRODUCT_RELEVANCE_FOR_WOOCOMMERCE_Admin
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_PRODUCT_RELEVANCE_FOR_WOOCOMMERCE_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			add_filter( 'plugin_action_links_' . plugin_basename( YITH_AM_PRODUCT_RELEVANCE_FOR_WOOCOMMERCE_DIR_PATH . '/' . basename( YITH_AM_PRODUCT_RELEVANCE_FOR_WOOCOMMERCE_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );
		}
		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->_panel_page, true, YITH_AM_PRODUCT_RELEVANCE_FOR_WOOCOMMERCE_SLUG );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_AM_PRODUCT_RELEVANCE_FOR_WOOCOMMERCE_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_AM_PRODUCT_RELEVANCE_FOR_WOOCOMMERCE_SLUG;
				$row_meta_args['is_premium'] = true;
			}

			return $row_meta_args;
		}

	}

}
