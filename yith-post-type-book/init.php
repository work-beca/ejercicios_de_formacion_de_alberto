<?php

/*
Plugin Name: yith-post-type-book
Plugin URI: https://posttypebook.es
Description: Plugin that add a new custom post type named "book"
Version: 1.0
Author: Alberto
Author URI: https://alberto.es
License: GPL2
Text Domain: yith-post-type-book
Domain Path: /languages/
*/


/**
 * 
 * Add a new custom post type named "book"
 * 
 */


function yith_cpt_book(){


    $labels = array(
        'name' => 'Book',
        'singular_name' => __('Last book','yith-post-type-book'),
        'add_new' => __('New book','yith-post-type-book'),
        'edit_item' => __('Edit book','yith-post-type-book'),
        'view_item' => __('View book','yith-post-type-book'), 
        'search_items' => __('Search books','yith-post-type-book'),
        'not_found' => __('Sorry, there no book available in this moment','yith-post-type-book'),
        'set_featured_image' => __('Set featured book image','yith-post-type-book'),
        'remove_featured_image' => __('Remove featured book image','yith-post-type-book')

    );

    $args = array(

        'labels' => $labels,
        'description' => 'Create, edit and show books',
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'menu_icon' => 'dashicons-book',
        'capability_type' => 'post',
        'has_archive' => true,
        'supports' => array('title', 'editor', 'thumbnail')
    );



    register_post_type('books', $args);

   // flush_rewrite_rules();
}
add_action('init', 'yith_cpt_book');


/**
 * Formacion internationalize plugin
 * 
 */

function yith_load_text_domain_ctp_book(){

    load_plugin_textdomain( 'yith-post-type-book', $deprecated = false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
add_action('plugins_loaded', 'yith_load_text_domain_ctp_book');

?>