# Ejercicios de formación


#### Alberto Martín Núñez

## Índice de contenidos

1. [ Ejercicios hooks ](#id1)
2. [ Ejercicios shortcode ](#id2)
3. [ Ejercicios shortcode ](#id3)




# EJERCICIOS HOOKS 

1. Mostrar al final de cada post un mensaje tipo:

	"Este post ha sido escrito por Christian"

```php

/*
	 * 
	 *Añadiendo la funcionalidad de agregar texto al final de cada post
*/

function yith_insert_text_end_post($content){
	if(!is_feed() && !is_home()){
		$content.="<div class= 'text-end'>";
		$content.= "<h4>Este post ha sido escrito por Alberto</h4>";
		$content.= "</div>";

		return $content;
	}
}	 
add_filter('the_content', 'inserttextendpost');
```

Se usa el **hook** *the_content* para modificar los post. Simplemente tenemos que añadirle a la variable $context lo que queremos insertar y como estamos añadiendo contenido se insertará al final del post.


2. Partiendo del ejercicio anterior, mostrar el nombre del autor que ha hecho ese post

```php

/**
	 * Añadiendo la funcionalidad de agregrar el author al final de cada post
	 */

function yith_insert_author_end_post($content){
	
	if(!is_feed() && !is_home()){
		$author = get_the_author_meta('nickname');
		$content.= "<h4>Este post ha sido escrito por ".$author." </h4>";
		
		return $content;
	}
}	 
add_filter('the_content', 'insertauthorendpost');
```
Se usa el **hook** *the_content* para modificar los post. Simplemente tenemos que añadirle a la variable $context utilizando `get_the_author_meta()` con el parametro *nickname*. Posteriormente añadimos una etiqeuta en donde mostramos el valor de $author.


3. Crear dos post distintos con contenido y al final de cada post, escribir el autor del otro post.

	Ejemplo:
		Post id =5 → author Pepe
		Post id = 6 → author Juan

	Al final del contenido del post 5 debe aparecer el nombre de Juan
	Al final del contenido del post 6 debe aparecer el nombre de Pepe

```php
    /**
 * Añadiendo funcionalidad de intercambiar nombre de autor de post
 */
function yith_return_name_swap($id_author){

	$name_authors = array( 1 => 'admin', 2 => 'alberto' );

	return $name_authors[$id_author];

}
 function yith_swap_name_post($content){

		if(!is_feed() && !is_home()){
			global $post;
			$postID = $post->ID;
			

			if($postID == 5){
				$content.= "<h4>Este post ha sido escrito por ".return_name_swap(2)."</h4>";	
			}elseif($postID == 11){
				$content.= "<h4>Este post ha sido escrito por ".return_name_swap(1)."</h4>";
			}
			
			return $content;
		}
	}	
add_filter('the_content', 'swap_name_post');

```

En este ejercicio damos por sabido que los postID son 5 y 11, entonces simplemente cuando estamos en un post añadiremos el contenido a ese post con el otro autor utilizando otra funcion que tiene un array con los diferentes usuarios que existen y devuelve el nombre. Esto ahora se hace de esta forma porque al principio no sabia la existencia de otras funciones como `get_the_author_meta()`. En anteriores codigos si las utilicé porque los modifiqué pero en este preferí dejarlo así.


4. A la hora de guardar un post, guardar una serie de datos aleatorios como puede ser.

	“Nombre del padre del autor”
	“Nombre de la madre del autor”
	“Edad”
Todos estos datos son inventados, de esa manera podemos trabajar con update_post_meta y get_post_meta

```php
function yith_add_fields(){

	$post_id = 11;
	$meta_key = array('name_mom', 'name_dad');
	$meta_value = array('pepa', 'jose');

	for($i = 0; $i < 2; $i++){
		update_post_meta($post_id, $meta_key[$i], $meta_value[$i], false);
	}
	
}

function yith_add_fields_post($post_id){

	add_fields();

}
add_action('save_post','add_fields_post',10,1);
```

Para añadir los campos a la tabla de la bbdd *post_meta* creamos una funcion con los parámetros necesarios creados como array(los necesarios) con la informacion que queremos insertar. Posteriormente los recorremos y llamamos a `update_post_meta()` que se encarga de actualizar los valores y si no existen crearlos.
Se utiliza el hook 'save_post' que nos permite llamar a la funcion creada cuando se guarde el post.

*Tener en cuenta que el parametro edad, que no aparece, se hizo en una primera ejecucion en solitario como prueba*

Ahora para obtener los datos de la base de datos y mostrarlos hacemos lo siguiente:

```php
/**
 * Añadiendo funcionalidad obteniendo metadatos
 */

function show_fields_post($content){

	if(!is_feed() && !is_home()){

		$edad_author = get_post_meta(11, 'Edad');
		$mom_author = get_post_meta(11, 'name_mom');
		$dad_author = get_post_meta(11, 'name_dad');
		error_log(print_r($edad_author));
		$content.= "<h4>La edad del autor es de ".$edad_author[0]." años</h4>";	
		$content.= "<h4>Su madre se llama ".$mom_author[0]." </h4>";	
		$content.= "<h4>Su padre se llama ".$dad_author[0]." </h4>";	
		
		
	}
	return $content;
}
add_filter('the_content', 'show_fields_post');
```

Para obtenerlo usamos la funcion `get_post_meta() ` indicándole el postID y el campo a retornar. Lo añadimos a la variables $content y aplicamos el filtro con el hook *the_content*.


5. Añadir en los post el título de quién lo ha escrito.
Por ejemplo:
	Lorem fistrum quietooor de la pradera Escrito por CarlosR


```php
// /**
//  * Añadiendo funcionalidad modificando titulo
//  */
function change_title_filter($title, $id){

	$post_ = get_post($id);
	$author_id = $post_->post_author;
	//error_log(print_r($author_id, true));	
	$name_author = get_the_author_meta('nickname', $author_id);
	
	$title.= " escrito por ".$name_author ;

	return $title;
}
add_filter('the_title', 'change_title_filter', 10, 2);

```
Obtenemos el nickname del autor del post y se lo añadimos a la variable $title. Esta variable representa el título del post utilizando el hook *the_title*.

6. Añadir en el mismo post, la fecha de publicación del post


```php

/**
 * Añadiendo funcionalidad - Añadiendo fecha del post
 */

function add_date_post($content){

	if(!is_feed() && !is_home()){
		global $post;
		$date_post =  $post->post_date;
	

		$content.= "<h4>La fecha de publicacion del post es ".$date_post."</h4> ";
		return $content;
	}
}
add_filter('the_content', 'add_date_post');
```

Para añadir la fecha del post simplemente accedo a la variable global **$post** y obtengo la fecha del la publicacion del post haciendo uso de *->*. Esto nos permite obtener diferente valores del post actual. Añadimos el contenido a la variable $content y la retornamos.


7. Del post, borrar el nombre del autor sin borrar nuestro código personalizado que hemos creado previamente.


```php
/**
 * Añadiendo funcionalidad - Eliminando title_post
 */

function change_title_filter_to_empty($title, $id){

	global $post;

	$title_real = $post->post_title;

	return $title_real;
 }
add_filter('the_title', 'change_title_filter_to_empty', 20, 2);
```

Para eliminar el autor del titulo que añadimo en el ejericicio 5 simplemente obtengo el titulo de la varibale global $post con *->post_title*, que es el titulo real del post, y lo retorno. Para que esto suceda posteriormente al ejericio 5 le pongo una prioridad mayor, 20, y asi me aseguro de que se ejcuté después modificando el titulo sin necesidad de eliminar la anterior función.



# EJERCICIOS SHORTCODE 

1. Crear un shortcode que le pase el post_id y me devuelva el content

Para crear el shorcode tenemos que usar la funcion `add_shortcode()` pasándolo por argumento el nombre con el que vamos a usar nuestro shortcode y el manejador que se ejecuta cuando usemos nuestro shortcode.

En este caso nuestro manejador obtiene el atributo que se le pasa a nuestro shortcode, que para este ejercicios es un ID de un post, y obtenemos el post completo con la funcion `get_post(ID)` y retornamos el contenido de ese post con `$post_->post_content`.

Aqui tendríamos el código para este shortcode:

```php
/**
 * 
 * Creating a shortcode that show the content of post
 * 
 * yith_content_post show content of post
 * @return content content of post
 * 
 */

add_shortcode('yith_content_post', 'yith_handler_content_post');

function ready_to_show_content_post(){
    function yith_handler_content_post($atts = []){

        $post_id_ = $atts['postid'];
        $post_ = get_post($post_id_);

        return $post_->post_content;
    }
}
add_action('init', 'ready_to_show_content_post');

```

2. Crear un shortcode que me liste los últimos 2 post que se han creado en la web

Para crear el shorcode tenemos que usar la funcion `add_shortcode()` pasándolo por argumento el nombre con el que vamos a usar nuestro shortcode y el manejador que se ejecuta cuando usemos nuestro shortcode.

En este caso nuestro manejador obtiene de la base de datos los dos últimos post *publicados* haciendo uso de la variable global `$wpdb->get_results()` y haciendo una consulta. Esto es bastante costoso ya que tenemos que realizar una consulta directa a la base de datos otra forma de acceder a los ultimos post es la siguiente:

`wp_get_recent_posts(array('numberposts'=>2))`

Una vez que tenemos el resultado recorremos los diferentes posts y imprimimos por pantalla el título de cada post.

Aqui tendríamos el código para este shortcode:

```php
/**
 * Creating a shortcode
 * 
 * yith_last_three_post return the last three post published.
 * @return tag three last post
 * 
 */

add_shortcode('yith_last_two_post', 'list_two_post');

function ready_post(){
    function list_two_post(){

        global $wpdb;

        $output = "";
        //wp_get_recent_posts(array('numberposts'=>2))
        $result = $wpdb->get_results("select post_title from `$wpdb->posts` where post_status = 'publish' order by `post_date` desc limit 2");

        foreach($result as $two_last_post){

           
            $title_post_ = $two_last_post->post_title;
            if($title_post_ != ""){
                $output.= '<li>' .$title_post_. '</li>';     
            }
           
        }
	
	    return '<ul>'. $output .'</ul>';
    }
}
add_action('init', 'ready_post');
```


3. Crear un shortcode que muestre los datos de un usuario --> nombre, apellidos, nickname

Para crear el shorcode tenemos que usar la funcion `add_shortcode()` pasándolo por argumento el nombre con el que vamos a usar nuestro shortcode y el manejador que se ejecuta cuando usemos nuestro shortcode.

En este caso nuestro manejador obtiene un id de usuario que le pasamos al shortcode cuando lo usamos. Con este identificador podemos acceder a los metadatos que ese usuario utilizando `get_user_meta()` y los añadimos a la variable **$output**. Posteriormente retornamos la variable **$output** que he creado añadiendo todos los parámetros que se piden en el enunciado.


Aqui tendríamos el código para este shortcode:

```php

/**
 * 
 * Creating a shortcode that show details of a user
 * 
 * yith_details_user show details of a user
 * @return tag list of details
 * 
 */

add_shortcode('yith_details_user','yith_handle_details_user');

function ready_init(){
    function yith_handle_details_user( $atts=[] ){

        $user_id = $atts['userid'];        

        $output = "";

        $first_name_user = get_user_meta($user_id, 'first_name');
        $last_name_user = get_user_meta($user_id, 'last_name');
        $nickname_user = get_user_meta($user_id, 'nickname');
        
        $output.= '<p> Details of user: </p>';
        $output.= '<ul><li> Name: '.$first_name_user[0].'</li>';
        $output.= '<li> Last Name: '.$last_name_user[0].'</li>';
        $output.= '<li> Nickname: '.$nickname_user[0].'</li></ul>'; 

        return $output;
    }
}
add_action('init', 'ready_init');
```

# EJERCICIOS WIDGETS

1. Crear un widget que me muestre la información en el frontend y que yo pueda meter:

Nombre
Apellidos
Dirección
Teléfono


Para crear un widget tenemos que crearnos una clase nueva que extienda de la clase ya existente **WP_Widget**.

Dentro de esta clase nos encontramos 4 funciones importantes:

- **__constructor()**

Configuramos nuestro widget, en mi caso con un nombre y un identificador

- **widget()**

Aqui procesamos las opciones del widget y mostramos el HTML en nuestra página. Para este ejercicio mostramos un pequeño párrafo con los datos del usuario que se introducen en la siguiente función.

- **form()**

Aqui configuramos las opciones de nuestro widgets que vamos a rellenar en el backend. En este caso las opciones son una serie de campos: *nombre, apellido, dirección y número de teléfono*

![Form](img/form.png)

- **update()**

Esta función nos permite guardar las opciones que hemos configurado en la base de datos.

Como último paso para crear nuestro widget es registrarlo. Para ello tenemos que hacer lo siguiente:

```php
function yith_register_Contact_widget(){
    register_widget('Contact_widget');
}
add_action('widgets_init', 'yith_register_Contact_widget');

```

Cuando el hook *widgets_init* se ejecute lanzamos el manejador *yith_register_Contact_widget* que llamará a la función `register_widget()` psando como argumento el nombre del widget a registrar, este es el nombre de la clase que hemos creado.

Aqui estaría el código del **Widget** que he implementado:


```php
/**
 * 
 * CREATING A WIDGET WITH DIFERENT FIELDS
 * 
 */


class Contact_widget extends WP_Widget{

    public function __construct()
    {
        // actual widget
        parent::__construct(
            'data-contact', // Base ID
            'Data Contact' // Name
        );
    }

    public $args = array(
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
        'before_widget' => '<div class="widget-wrap">',
        'after_widget'  => '</div></div>'
    );

    public function widget($args, $instance)
    {
        // outputs the content of the widget

        echo $args['before_widget'];

        /*if(!empty($instance['title'])){
            echo $args['before_title'].apply_filters('widget_title', $instance['title']).$args['after_title'];
        }*/
        
        $name_html = esc_html__($instance['name'], 'text_domain');
        $last_name_html = esc_html__($instance['last_name'], 'text_domain');
        $address_html = esc_html__($instance['address'], 'text_domain');
        $phone_html = esc_html__($instance['phone'], 'text_domain');

        echo '<div class ="textwidget">';

            echo '<p class="title-widgets"> Data Contact: </p>';
            echo '<p> Me llamo <i>'.$name_html. '  '.$last_name_html.'</i></p>';
            echo    '<p> Vivo en <i>'.$address_html.'</i> y mi número de teléfono es </p>'.
                '<p><i>'.$phone_html.'</i></p>';
        echo '</div>';
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        // outputs the options form in the admin

        $name = !empty($instance['name']) ? $instance['name']: esc_html__('', 'text_domain');
        $last_name = !empty($instance['last_name']) ? $instance['last_name'] : esc_html__('','text_domain');
        $address = !empty($instance['address']) ? $instance['address']: esc_html__('', 'text_domain');
        $phone = !empty($instance['phone']) ? $instance['phone']: esc_html__('', 'text_domain');
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('name')); ?>"> <?php echo esc_html__('Name:', 'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'name' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'name' ) ); ?>" type="text" value="<?php echo esc_attr( $name ); ?>" required>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'last_name' ) ); ?>"><?php echo esc_html__( 'Last_name:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'last_name' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'last_name' ) ); ?>" type="text" value="<?php echo esc_attr( $last_name ); ?>" required>
            
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>"><?php echo esc_html__( 'Address:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'address' ) ); ?>" type="text" value="<?php echo esc_attr( $address ); ?>" required>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>"><?php echo esc_html__( 'Phone:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'phone' ) ); ?>" type="tel" value="<?php echo esc_attr( $phone ); ?>" pattern="[0-9]{3}-[0-9]{3}-[0-9]{3}" required placeholder="123-456-789">
        </p>

        <?php
    }

    public function update($new_instance, $old_instance)
    {
        // processes widget options to be saved

        $instance = array();
 
        $instance['name'] = ( !empty( $new_instance['name'] ) ) ? strip_tags( $new_instance['name'] ) : '';
        $instance['last_name'] = ( !empty( $new_instance['last_name'] ) ) ? $new_instance['last_name'] : '';
        $instance['address'] = ( !empty( $new_instance['address'] ) ) ? $new_instance['address'] : '';
        $instance['phone'] = ( !empty( $new_instance['phone'] ) ) ? $new_instance['phone'] : '';

        return $instance;
    }

} // end to class My_Widget

/**
 * Register Contact_widget widget
 */
function yith_register_Contact_widget(){
    register_widget('Contact_widget');
}
add_action('widgets_init', 'yith_register_Contact_widget');

```


2. Crear un widget que me liste los últimos post que indiquemos mediante un campo. 



Para crear el widget tenemos que crearnos una clase nueva que extienda de la clase ya existente **WP_Widget**.

Dentro de esta clase nos encontramos 4 funciones importantes:

- **__constructor()**

Configuramos nuestro widget, en mi caso con un nombre y un identificador

- **widget()**

Aqui procesamos las opciones del widget y mostramos el HTML en nuestra página. Para este ejercicio recorreremos los últimos X post que indicaremos en un campo que se crear en la función `form()` y mostraremos los títulos de esos últimos post.

- **form()**

Aqui configuramos las opciones de nuestro widgets que vamos a rellenar en el backend. En este caso solo tendremos dos opciones: *un título y un campo para indicar el número de post que queremos mostrar*

![Form](img/post.png)

- **update()**

Esta función nos permite guardar las opciones que hemos configurado en la base de datos.

Aquin tenemos el código de la implementación de este widget:


```php
/**
 * 
 * CREATING A WIDGET that list the last x posts
 * 
 */

class Post_widget extends WP_Widget{

    public function __construct()
    {
        // actual widget
        parent::__construct(
            'List-Last-Post', // Base ID
            'List Last Post' // Name
        );
    }

    public $args = array(
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
        'before_widget' => '<div class="widget-wrap">',
        'after_widget'  => '</div></div>'
    );

    public function widget($args, $instance)
    {
        // outputs the content of the widget

        echo $args['before_widget'];

        /*if(!empty($instance['title'])){
            echo $args['before_title'].apply_filters('widget_title', $instance['title']).$args['after_title'];
        }*/
        
        $num_post = esc_html__($instance['num_post'], 'text_domain');
        $posts_recent = wp_get_recent_posts(array('numberposts'=>$num_post));
        
        echo '<div class ="textwidget">';
        echo '<p>'.$instance['title'].': </p>';
        echo '<ul>';
        foreach($posts_recent as $post_){
            echo '<li><a href="'.get_post_permalink($post_['ID']).'">'.$post_['post_title'].'</li>';
        }
    
        echo '</ul></div>';
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        // outputs the options form in the admin

        $title = !empty($instance['title']) ? $instance['title']: esc_html__('', 'text_domain');
        $num_post = !empty($instance['num_post']) ? $instance['num_post']: esc_html__('', 'text_domain');

        ?>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"> <?php echo esc_html__('Title:', 'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('num_post')); ?>"> <?php echo esc_html__('Num_post:', 'text_domain'); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'num_post' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'num_post' ) ); ?>" type="number" min="0" max="5" value="<?php echo esc_attr( $num_post ); ?>">
        </p>          

        <?php
    }

    public function update($new_instance, $old_instance)
    {
        // processes widget options to be saved

        $instance = array();
 
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['num_post'] = ( !empty( $new_instance['num_post'] ) ) ? $new_instance['num_post'] : '';


        return $instance;
    }

} // end to class Post_widget

/**
 * Register Contact_widget widget
 */
function yith_register_Post_widget(){
    register_widget('Post_widget');
}
add_action('widgets_init', 'yith_register_Post_widget');
```