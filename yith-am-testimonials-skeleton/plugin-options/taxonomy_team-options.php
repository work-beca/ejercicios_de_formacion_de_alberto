<?php
/**
 * File taxonomy team options
 *
 * @package WordPress
 */

$new_tab = array(
	'taxonomy_team' => array(
		'custom-taxonomy_list_table' => array(
			'type'     => 'taxonomy',
			'taxonomy' => 'Teams',
		),
	),
);

return $new_tab;
