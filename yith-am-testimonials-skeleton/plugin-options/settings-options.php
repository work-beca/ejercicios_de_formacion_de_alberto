<?php
/**
 * File setting options
 *
 * @package WordPress
 */

$settings = array(
	'settings' => array(
		'section-1' => array(
			array(
				'name' => __( 'Testimonials Config', 'yith-am-testimonials-skeleton' ),
				'type' => 'title',
			),
			array(
				'id'   => 'yith-amet-number-post',
				'name' => __( 'Number Posts Default', 'yith-am-testimonials-skeleton' ),
				'type' => 'number',
				'min'  => 0,
				'max'  => 100,
				'step' => 1,
				'std'  => 6,
			),
			array(
				'id'   => 'yith-amet-checkbox-image',
				'type' => 'onoff',
				'name' => __( 'Show Image', 'yith-am-testimonials-skeleton' ),
			),
			array(
				'id'      => 'yith-amet-select-hover-effect',
				'name'    => __( 'Type of hover effect', 'yith-am-testimonials-skeleton' ),
				'type'    => 'select',
				'options' => array(
					'none'      => 'None',
					'highlight' => 'Highlight',
					'zoom'      => 'Zoom',
				),
			),
			array(
				'id'   => 'yith-amet-size-border-radius',
				'name' => __( 'Size border radius', 'yith-am-testimonials-skeleton' ),
				'type' => 'number',
				'min'  => 0,
				'max'  => 100,
				'step' => 1,
				'std'  => 3,
			),
			array(
				'id'   => 'yith-amet-color-picker-links',
				'name' => __( 'Color links', 'yith-am-testimonials-skeleton' ),
				'type' => 'colorpicker',
				'std'  => '#0073aa',
			),
			array(
				'type' => 'close',
			),
		),
	),
);

return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
