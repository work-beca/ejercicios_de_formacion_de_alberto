<?php
/**
 * File taxonomy position
 *
 * @package WordPress
 */

$new_tab = array(
	'taxonomy_position' => array(
		'custom-taxonomy_list_table' => array(
			'type'     => 'taxonomy',
			'taxonomy' => 'Positions',
		),
	),
);

return $new_tab;
