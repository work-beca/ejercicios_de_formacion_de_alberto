<?php
/**
 * Custom testimonials
 * @package WordPress
 */

$new_tab = array(
	'testimonials' => array(
		'custom-post-type_list_table' => array(
			'type'      => 'post_type',
			'post_type' => 'yith_testimonial',
		),
	),
);

return $new_tab;
