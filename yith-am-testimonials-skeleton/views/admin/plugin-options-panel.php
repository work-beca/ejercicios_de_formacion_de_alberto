<div class="wrap">
	<h1><?php esc_html_e( 'Settings', 'yith-am-testimonials-skeleton' ); ?></h1>

	<form method="post" action="options.php">
		<?php
			settings_fields( 'amtestimonials-options-page' );
			do_settings_sections( 'amtestimonials-options-page' );
			submit_button();
		?>

	</form>
</div>
