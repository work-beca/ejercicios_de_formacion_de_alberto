<?php

 $values                 = get_post_custom( $post->ID );
 $role                   = isset( $values['role_testimonials'] ) ? esc_attr( $values['role_testimonials'][0] ) : '';
 $company                = isset( $values['company_testimonials'] ) ? esc_attr( $values['company_testimonials'][0] ) : '';
 $url_company            = isset( $values['url_company_testimonials'] ) ? esc_attr( $values['url_company_testimonials'][0] ) : '';
 $email_company          = isset( $values['email_testimonials'] ) ? esc_attr( $values['email_testimonials'][0] ) : '';
 $star                   = isset( $values['estrellas'] ) ? esc_attr( $values['estrellas'][0] ) : '';
 $on_off_vip             = isset( $values['on_off_testimonials'] ) ? esc_attr( $values['on_off_testimonials'][0] ) : '';
 $on_off_badge           = isset( $values['on_off_badge_testimonials'] ) ? esc_attr( $values['on_off_badge_testimonials'][0] ) : '';
 $title_badge            = isset( $values['title_badge_testimonials'] ) ? esc_attr( $values['title_badge_testimonials'][0] ) : '';
 $background_color_badge = isset( $values['background_color_badge'] ) ? esc_attr( $values['background_color_badge'][0] ) : '';


?>

<div class="ga-tst-form">
<!-- Testimonial role field -->
<div class="ga-tst-form__input ga-tst-form__input-role">

		<p>
			<label for="role_testimonials"><?php echo esc_html__( 'Role:', 'yith-am-testimonials-skeleton' ); ?> </label>
			<input placeholder="add role" type="text" name="role_testimonials" id="role_testimonials" value="<?php echo esc_html( $role ); ?>" />
		</p>
		<p>
			<label for="company_testimonials"><?php echo esc_html__( 'Company:', 'yith-am-testimonials-skeleton' ); ?> </label>
			<input placeholder="add company" type="text" name="company_testimonials" id="company_testimonials" value="<?php echo esc_html( $company ); ?>" />
		</p>
		<p>
			<label for="url_company_testimonials"><?php echo esc_html__( 'URL Company:', 'yith-am-testimonials-skeleton' ); ?> </label>
			<input placeholder="add url of company" type="url" name="url_company_testimonials" id="url_company_testimonials" value="<?php echo esc_html( $url_company ); ?>" />
		</p>
		<p>
			<label for="email_testimonials"><?php echo esc_html__( 'Email Player:', 'yith-am-testimonials-skeleton' ); ?> </label>
			<input placeholder="add email of player" type="email" name="email_testimonials" id="email_testimonials" value="<?php echo esc_html( $email_company ); ?>" />
		</p>
		<p class="clasificacion">
			<input 
			<?php
			if ( $star == '5' ) {
				?>
				 checked<?php } ?> id="radio1" type="radio" name="estrellas" value="5">
			<label for="radio1">★</label>
			<input 
			<?php
			if ( $star == '4' ) {
				?>
				 checked<?php } ?> id="radio2" type="radio" name="estrellas" value="4">
			<label for="radio2">★</label>
			<input 
			<?php
			if ( $star == '3' ) {
				?>
				checked<?php } ?> id="radio3" type="radio" name="estrellas" value="3">
			<label for="radio3">★</label>
			<input 
			<?php
			if ( $star == '2' ) {
				?>
				checked<?php } ?> id="radio4" type="radio" name="estrellas" value="2">
			<label for="radio4">★</label>
			<input 
			<?php
			if ( $star == '1' ) {
				?>
				checked<?php } ?> id="radio5" type="radio" name="estrellas" value="1">
			<label for="radio5">★</label>
		</p>
		<p>
			<!-- Rectangular switch -->
			<p>VIP </p>
			<label class="switch">
				<input 
				<?php
				if ( $on_off_vip == '1' ) {
					?>
					checked<?php } ?> type="checkbox" id="on_off_testimonials" name="on_off_testimonials" value="1">
				<span class="slider"></span>
			</label>
		</p>

		<p>
			<!-- Rectangular switch -->
			<p><?php echo esc_html__( 'Activate badge', 'yith-am-testimonials-skeleton' ); ?> </p>
			<label class="switch">
				<input 
				<?php
				if ( $on_off_badge == '1' ) {
					?>
					 checked<?php } ?> type="checkbox" id="on_off_badge_testimonials" name="on_off_badge_testimonials" value="1">          
				<span class="slider"></span>
			</label>
		</p>

		<?php if ( $on_off_badge == '1' ) { ?>
			<p>
				<label for="title_badge"><?php echo esc_html__( 'Title:', 'yith-am-testimonials-skeleton' ); ?> </label>
				<input type= "text" placeholder= "Add title" name="title_badge_testimonials" id = " title_badge_testimonials " value= "<?php echo esc_html( $title_badge ); ?>" />
			</p>
			<p>
				<label for="title_badge"><?php echo esc_html__( 'Background color:', 'yith-am-testimonials-skeleton' ); ?>  </label>
				<input type="text" id="background_color_badge" value="<?php echo esc_html( $background_color_badge ); ?>" name="background_color_badge" class="my-color-field" data-default-color="#13327a"/>
			</p>
			
		<?php } ?>
			
				
	</div>
</div>
