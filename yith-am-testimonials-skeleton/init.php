<?php

/*
Plugin Name: yith-am-testimonials-skeleton
Plugin URI: https://yith-am-testimonials-skeleton.es
Description: Plugin that add diferent testimonials use a skeleton
Version: 1.0.0
Author: Alberto Martin
Author URI: https://alberto.es
License: GPL2
Text Domain: yith-am-testimonials-skeleton
*/

! defined( 'ABSPATH' ) && exit;

/**
 *
 * Create a constants
 */

if ( ! defined( 'YITH_AMTESTIMONIALS_VERSION' ) ) {
	define( 'YITH_AMTESTIMONIALS_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_AMTESTIMONIALS_DIR_URL' ) ) {
	define( 'YITH_AMTESTIMONIALS_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_AMTESTIMONIALS_DIR_ASSETS_URL' ) ) {
	define( 'YITH_AMTESTIMONIALS_DIR_ASSETS_URL', YITH_AMTESTIMONIALS_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_AMTESTIMONIALS_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_AMTESTIMONIALS_DIR_ASSETS_CSS_URL', YITH_AMTESTIMONIALS_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_AMTESTIMONIALS_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_AMTESTIMONIALS_DIR_ASSETS_JS_URL', YITH_AMTESTIMONIALS_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_AMTESTIMONIALS_DIR_PATH' ) ) {
	define( 'YITH_AMTESTIMONIALS_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_AMTESTIMONIALS_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_AMTESTIMONIALS_DIR_INCLUDES_PATH', YITH_AMTESTIMONIALS_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_AMTESTIMONIALS_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_AMTESTIMONIALS_DIR_TEMPLATES_PATH', YITH_AMTESTIMONIALS_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_AMTESTIMONIALS_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_AMTESTIMONIALS_DIR_VIEWS_PATH', YITH_AMTESTIMONIALS_DIR_PATH . '/views' );
}
! defined( 'YITH_AMTESTIMONIALS_SLUG' ) && define( 'YITH_AMTESTIMONIALS_SLUG', 'yith-am-testimonials-skeleton' );

! defined( 'YITH_AMTESTIMONIALS_FILE' ) && define( 'YITH_AMTESTIMONIALS_FILE', __FILE__ );

! defined( 'YITH_AMTESTIMONIALS_INIT' ) && define( 'YITH_AMTESTIMONIALS_INIT', plugin_basename( __FILE__ ) );

! defined( 'YITH_AMTESTIMONIALS_SECRETKEY' ) && define( 'YITH_AMTESTIMONIALS_SECRETKEY', 'zd9egFgFdF1D8Azh2ifK' );
/**
 *
 * Included the scripts
 */


if ( ! function_exists( 'yith_amtestimonials_init_classes' ) ) {

	/**
	 * Yith_amtestimonials_init_classes
	 *
	 * @return void
	 */
	function yith_amtestimonials_init_classes() {

		load_plugin_textdomain( 'yith-am-testimonials-skeleton', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// require all the files you include on your plugins
		require_once YITH_AMTESTIMONIALS_DIR_INCLUDES_PATH . '/class-yith-amtestimonials-plugin-skeleton.php';

		if ( class_exists( 'YITH_AMTESTIMONIALS_Plugin_Skeleton' ) ) {
			/**
			 * Call the main function
			 */
			yith_amtestimonials_plugin_skeleton();
		}
	}
}


add_action( 'plugins_loaded', 'yith_amtestimonials_init_classes' );


if ( ! function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( YITH_AMTESTIMONIALS_DIR_PATH . 'plugin-fw/init.php' ) ) {
	require_once YITH_AMTESTIMONIALS_DIR_PATH . 'plugin-fw/init.php';
}
yit_maybe_plugin_fw_loader( YITH_AMTESTIMONIALS_DIR_PATH );

if ( ! function_exists( 'yith_plugin_registration_hook' ) ) {
	require_once 'plugin-fw/yit-plugin-registration-hook.php';
}

register_activation_hook( __FILE__, 'yith_plugin_registration_hook' );
