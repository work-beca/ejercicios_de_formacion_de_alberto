<?php


if ( ! defined( 'YITH_AMTESTIMONIALS_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AMTESTIMONIALS_Frontend' ) ) {

	class YITH_AMTESTIMONIALS_Frontend {

		private static $instance;

		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * Construct YITH_AMTESTIMONIALS_Frontend
		 */

		private function __construct() {

			add_action( 'wp_enqueue_scripts', array( $this, 'front_end_scripts_testimonials' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'amtestimonials_add_inline_css_links' ) );

		}


		public function front_end_scripts_testimonials() {

			wp_register_style( 'style_testimonials', YITH_AMTESTIMONIALS_DIR_ASSETS_CSS_URL . '/style.css', false );
			wp_enqueue_style( 'style_testimonials' );

		}

		public function amtestimonials_add_inline_css_links() {

			wp_enqueue_style( 'color_links_css', YITH_AMTESTIMONIALS_DIR_ASSETS_URL . '/style.css' );

			$color_links = '
            
                 .link-company{
                     color: #0073aa;
                 }
            ';

			wp_add_inline_style( 'color_links_css', $color_links );
		}
	}
}
