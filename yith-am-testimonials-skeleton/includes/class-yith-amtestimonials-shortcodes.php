<?php


if ( ! defined( 'YITH_AMTESTIMONIALS_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AMTESTIMONIALS_Shortcode' ) ) {

	/**
	 * YITH_AMTESTIMONIALS_Shortcode
	 */
	class YITH_AMTESTIMONIALS_Shortcode {

		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return void
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * Construct YITH_AMTESTIMONIALS_Shortcode
		 */

		private function __construct() {

			add_action( 'init', array( $this, 'ready_for_show_testimonials' ) );

		}

		/**
		 * Ready_for_show_testimonials
		 *
		 * @return void
		 */
		public function ready_for_show_testimonials() {
			add_shortcode( 'yith_show_testimonials', array( $this, 'yith_show_testimonials_cb' ) );

		}

		/**
		 * Yith_show_testimonials_cb
		 *
		 * @param  mixed $atts .
		 * @return template
		 */
		public function yith_show_testimonials_cb( $atts = array() ) {
			// ¡obtenemos las opciones generales
			//$options = get_option( 'yit_yith-am-testimonials-skeleton_options' )['yith-amet-number-post'];
			$args    = array();

			if ( ! empty( $atts['tax_ids'] ) ) {
				$array_tax_ids = explode( ',', $atts['tax_ids'] );
				$args          = array(
					'post_type'   => 'yith_testimonial',
					'numberposts' => -1,
					'tax_query'   => array(
						array(
							'taxonomy' => 'Teams',
							'field'    => 'term_id',
							'terms'    => $array_tax_ids,

						),
					),

				);

			}
			// ¡Si los atributos del shortcode estan vacíos
			if ( empty( $atts['num_posts'] ) && empty( $atts['ids'] ) && empty( $atts['tax_ids'] ) ) {

				$args = array(
					'post_type'   => 'yith_testimonial',
					'numberposts' => get_option( 'yit_yith-am-testimonials-skeleton_options' )['yith-amet-number-post'],

				);
			}

			if ( ! empty( $atts['num_posts'] ) && ! empty( $atts['ids'] ) ) {
				$array_ids = explode( ',', $atts['ids'] );

				$args = array(
					'post_type' => 'yith_testimonial',
					'post__in'  => $array_ids,
				);
			}

			if ( empty( $atts['num_posts'] ) and ! empty( $atts['ids'] ) ) {
				$array_ids = explode( ',', $atts['ids'] );

				$args = array(
					'post_type' => 'yith_testimonial',
					'post__in'  => $array_ids,
				);
			}

			if ( ! empty( $atts['num_posts'] ) and empty( $atts['ids'] ) ) {

				$num_posts = $atts['num_posts'];
				$args      = array(
					'post_type'   => 'yith_testimonial',
					'numberposts' => $num_posts,
				);
			}

			$posts_ = get_posts( $args );

			ob_start();
			yith_amtestimonials_get_template( '/frontend/testimonials.php', $posts_, $atts );
			return ob_get_clean();

		}

	}
}
