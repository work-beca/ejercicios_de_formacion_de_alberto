<?php
/**
 * File testimonials admin
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AMTESTIMONIALS_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AMTESTIMONIALS_Admin' ) ) {

	/**
	 * YITH_AMTESTIMONIALS_Admin
	 */
	class YITH_AMTESTIMONIALS_Admin {

		/**
		 * @var $panel_page page .
		 */
		protected $panel_page = 'yith_amte_panel';

		/** @var YIT_Plugin_Panel_WooCommerce $_panel the panel */
		private $_panel;

		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return $instance
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_AMTESTIMONIALS_Admin constructor.
		 */
		private function __construct() {

			add_filter( 'plugin_action_links_' . plugin_basename( YITH_AMTESTIMONIALS_DIR_PATH . '/' . basename( YITH_AMTESTIMONIALS_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );

			// ¡Create panel YITH
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
			// ¡Create metabox YITH
			add_action( 'admin_init', array( $this, 'add_metabox' ), 10 );

			add_action( 'admin_head', array( $this, 'check_post_type_and_remove_media_buttons' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'mw_enqueue_color_picker' ) );

			// ¡See definition on class-wp-post-list-table.php  manage_{$post_type}_posts_columns !;
			add_filter( 'manage_yith_testimonial_posts_columns', array( $this, 'add_skeleton_post_type_columns' ) );

			// ¡See definition on class-wp-post-list-table.php manage_{$post->post_type}_posts_custom_column!
			add_action( 'manage_yith_testimonial_posts_custom_column', array( $this, 'display_skeleton_post_type_custom_column' ), 10, 2 );

			// ¡Create role TTMLS Manager!
			add_action( 'admin_init', array( $this, 'amtestimonials_add_new_role' ) );

			// ¡Add capabilities to role TTMLS Manager!
			add_action( 'admin_init', array( $this, 'add_capabilities_testimonials' ) );
		}

		/**
		 * Register_panel
		 *
		 * @return void
		 */
		public function register_panel() {
			if ( ! empty( $this->_panel ) ) {
				return;
			}

			$admin_tabs = array(
				'settings'          => __( 'Settings', 'yith-am-testimonials-skeleton' ),
				'testimonials'      => __( 'Testimonials', 'yith-am-testimonials-skeleton' ),
				'taxonomy_position' => __( 'Taxonomy Position', 'yith-am-testimonials-skeleton' ),
				'taxonomy_team'     => __( 'Taxonomy Team', 'yith-am-testimonials-skeleton' ),
			);

			$args         = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH Testimonials Config', // ¡this text MUST be NOT translatable
				'menu_title'         => 'YITH Testimonials Config', // ¡this text MUST be NOT translatable
				'plugin_description' => __( 'Plugin that add diferent testimonials use a skeleton', 'yith-am-testimonials-skeleton' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => 'yith-am-testimonials-skeleton',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => 'yith_test_plugin_panel',
				'admin-tabs'         => $admin_tabs,
				'plugin-url'         => YITH_AMTESTIMONIALS_DIR_PATH,
				'options-path'       => YITH_AMTESTIMONIALS_DIR_PATH . 'plugin-options',
			);
			$this->_panel = new YIT_Plugin_Panel( $args );
		}

		/**
		 * Add_metabox
		 *
		 * @return void
		 */
		public function add_metabox() {
			$args = array(
				'label'    => __( 'Additional information', 'yith-am-testimonials-skeleton' ),
				'pages'    => 'yith_testimonial',
				'context'  => 'normal',
				'priority' => 'default',
				'tabs'     => array(
					'settings' => array( // ¡tab
						'label'  => __( 'Settings', 'yith-am-testimonials-skeleton' ),
						'fields' => array(
							'yith_amet_meta_text_role'    => array(
								'label'   => __( 'Role', 'yith-am-testimonials-skeleton' ),
								'desc'    => __( 'Player position.', 'yith-am-testimonials-skeleton' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
							'yith_amet_meta_text_company' => array(
								'label'   => __( 'Company', 'yith-am-testimonials-skeleton' ),
								'desc'    => __( 'Team where the player plays.', 'yith-am-testimonials-skeleton' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
							'yith_amet_meta_text_company_url' => array(
								'label'   => __( 'URL Company', 'yith-am-testimonials-skeleton' ),
								'desc'    => __( 'Team website.', 'yith-am-testimonials-skeleton' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
							'yith_amet_meta_text_player_email' => array(
								'label'   => __( 'Player Email', 'yith-am-testimonials-skeleton' ),
								'desc'    => __( 'Player email.', 'yith-am-testimonials-skeleton' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
							'yith_amet_meta_radio_stars'  => array(
								'label'   => __( 'Stars', 'yith-am-testimonials-skeleton' ),
								'desc'    => __( 'Number of stars-skills', 'yith-am-testimonials-skeleton' ),
								'type'    => 'radio',
								'private' => false,
								'options' => array(
									'1' => '&#9733 1 Star',
									'2' => '&#9733 2 Stars',
									'3' => '&#9733 3 stars',
									'4' => '&#9733 4 stars',
									'5' => '&#9733 5 stars',
								),
							),
							'yith_amet_meta_checkbox_vip' => array(
								'label'   => __( 'Player VIP', 'yith-am-testimonials-skeleton' ),
								'desc'    => __( 'Player VIP.', 'yith-am-testimonials-skeleton' ),
								'type'    => 'checkbox',
								'private' => false,
								'std'     => '',
							),
							'yith_amet_meta_checkbox_badge' => array(
								'id'      => 'yith_amet_meta_checkbox_badge',
								'label'   => __( 'Activate badge', 'yith-am-testimonials-skeleton' ),
								'desc'    => __( 'Player badge.', 'yith-am-testimonials-skeleton' ),
								'type'    => 'checkbox',
								'private' => false,
								'std'     => '',

							),
							'yith_amet_meta_text_title_badge' => array(
								'id'      => 'yith_amet_meta_text_title_badge',
								'label'   => __( 'Title Badge', 'yith-am-testimonials-skeleton' ),
								'desc'    => __( 'Title Badge.', 'yith-am-testimonials-skeleton' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
								'deps'    => array(
									'id'    => 'yith_amet_meta_checkbox_badge',
									'value' => 'yes',
									'type'  => 'fadeInOut',
								),
							),
							'yith_amet_meta_colorpicket_badge' => array(
								'label'         => __( 'Background Color', 'yith-am-testimonials-skeleton' ),
								'desc'          => __( 'Background Color Badge.', 'yith-am-testimonials-skeleton' ),
								'type'          => 'colorpicker',
								'private'       => false,
								'alpha_enabled' => false,
								'std'           => '#13327a',
								'deps'          => array(
									'id'    => 'yith_amet_meta_checkbox_badge',
									'value' => 'yes',
									'type'  => 'fadeInOut',
								),
							),

						),
					),
				),
			);

			$metabox1 = YIT_Metabox( 'yith_amet_metabox_testimonials' );
			$metabox1->init( $args );
		}

		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, YITH_AMTESTIMONIALS_SLUG );
		}

		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_AMTESTIMONIALS_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_AMTESTIMONIALS_SLUG;
				$row_meta_args['is_premium'] = true;
			}

			return $row_meta_args;
		}


		/**
		 * Mw_enqueue_color_picker
		 *
		 * @param  mixed $hook_suffix .
		 * @return void
		 */
		public function mw_enqueue_color_picker( $hook_suffix ) {

			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_script( 'my-script-handle', YITH_AMTESTIMONIALS_DIR_ASSETS_JS_URL . '/my-script.js', array( 'wp-color-picker' ), false, true ); //phpcs:ignore
		}


		/**
		 * Check_post_type_and_remove_media_buttons
		 *
		 * @return void
		 */
		public function check_post_type_and_remove_media_buttons() {
			global $current_screen;

			if ( 'yith_testimonial' == $current_screen->post_type ) {
				remove_action( 'media_buttons', 'media_buttons' );
			}
		}

		/**
		 * Viev meta boxes
		 *
		 * @param $post $post comment .
		 */
		public function view_meta_boxes( $post ) {

			yith_amtestimonials_get_view( '/metaboxes/plugin-am-testimonials-skeleton-info-metabox.php', array( 'post' => $post ) );
		}

		/**
		 *
		 * Add column to custom post type
		 */
		public function add_skeleton_post_type_columns( $post_columns ) {
			$new_columns = apply_filters(
				'yith_amtestimonials_skeleton_custom_columns ',
				array(
					'column1' => esc_html__( 'Role', 'yith-am-testimonials-skeleton' ),
					'column2' => esc_html__( 'Company', 'yith-am-testimonials-skeleton' ),
					'column3' => esc_html__( 'Email', 'yith-am-testimonials-skeleton' ),
					'column4' => esc_html__( 'Stars', 'yith-am-testimonials-skeleton' ),
					'column5' => esc_html__( 'VIP', 'yith-am-testimonials-skeleton' ),
				)
			);

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
		}

		/**
		 *
		 * Add content to column custom post type
		 */

		public function display_skeleton_post_type_custom_column( $column_name, $post_id ) {

			switch ( $column_name ) {

				case 'column1':
					$value_role = get_post_meta( $post_id, 'role_testimonials', true );
					echo esc_html__( $value_role, 'yith-am-testimonials-skeleton' );

					break;
				case 'column2':
					$value_company = get_post_meta( $post_id, 'company_testimonials', true );
					echo esc_html__( $value_company, 'yith-am-testimonials-skeleton' );
					break;
				case 'column3':
					$value_email = get_post_meta( $post_id, 'email_testimonials', true );
					echo esc_html__( $value_email, 'yith-am-testimonials-skeleton' );
					break;
				case 'column4':
					$value_stars = get_post_meta( $post_id, 'estrellas', true );
					echo esc_html__( $value_stars . ' stars', 'yith-am-testimonials-skeleton' );
					break;
				case 'column5':
					$value_vip = get_post_meta( $post_id, 'on_off_testimonials', true );
					if ( '1' == $value_vip ) {
						echo esc_html__( 'The player is VIP', 'yith-am-testimonials-skeleton' );
					} else {
						echo esc_html__( 'The player is not VIP ', 'yith-am-testimonials-skeleton' );
					}

					break;
				default:
					do_action( 'yith_ps_skeleton_display_custom_column', $column_name, $post_id );
					break;
			}

		}


		/**
		 * Register_settings
		 *
		 * @return void
		 */
		public function register_settings() {

			$page_name    = 'amtestimonials-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => 'yith_amtestimonials_shortcode_number',
					'title'    => esc_html__( 'Number Posts Default', 'yith-am-testimonials-skeleton' ),
					'callback' => 'print_number_input',
				),
				array(
					'id'       => 'yith_amtestimonials_shortcode_show_image',
					'title'    => esc_html__( 'Show image', 'yith-am-testimonials-skeleton' ),
					'callback' => 'print_show_image',
				),
				array(
					'id'       => 'yith_amtestimonials_shortcode_hover_effect',
					'title'    => esc_html__( 'Type of hover effect ', 'yith-am-testimonials-skeleton' ),
					'callback' => 'print_hover_effect',
				),
			);

			add_settings_section(
				$section_name,
				esc_html__( 'Section', 'yith-am-testimonials-skeleton' ),
				'',
				$page_name
			);

			foreach ( $setting_fields as $field ) {
				extract( $field );

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);

				register_setting( $page_name, $id );
			}

		}


		/**
		 * Amtestimonials_add_new_role
		 *
		 * @return void
		 */
		public function amtestimonials_add_new_role() {

			if ( get_option( 'yith_add_role_testimonials', false ) ) {

				$capabilities = array(
					'read'                      => true,
					'edit_testimonial'          => true,
					'edit_other_testimonials'   => true,
					'read_testimonial'          => true,
					'read_private_testimonials' => true,
					'publish_testimonials'      => true,
					'edit_testimonials'         => true,
					'delete_testimonial'        => true,
					'delete_testimonials'       => true,

				);

				add_role( 'TTMLS Manager', __( 'TTMLS Manager' ), $capabilities );
				update_option( 'yith_add_role_testimonials', true );

			} else {
				return;
			}

		}

		/**
		 * Add_capabilities_testimonials
		 *
		 * @return void
		 */
		public function add_capabilities_testimonials() {

			// 'gets the TTMLS Manager role
			$testimonial_role = get_role( 'TTMLS Manager' );

			if ( $testimonial_role ) {
				$testimonial_caps = array( 'edit_testimonial', 'edit_other_testimonials', 'read_testimonial', 'read_private_testimonials', 'publish_testimonials', 'edit_testimonials', 'delete_testimonial', 'delete_testimonials', 'manage_testimonials', 'upload_files' );

				foreach ( $testimonial_caps as $cap ) {
					if ( ! $testimonial_role->has_cap( $cap ) ) {
						$testimonial_role->add_cap( $cap );
					}
				}
			}

			// 'gets the administrator role
			$admin = get_role( 'administrator' );

			if ( $admin ) {
				$testimonial_caps_admin = array( 'edit_testimonial', 'edit_other_testimonials', 'read_testimonial', 'read_private_testimonials', 'publish_testimonials', 'edit_testimonials', 'delete_testimonial', 'delete_testimonials', 'manage_testimonials', 'upload_files' );

				foreach ( $testimonial_caps_admin as $cap ) {
					if ( ! $admin->has_cap( $cap ) ) {
						$admin->add_cap( $cap );
					}
				}
			}
		}
	} // End Class Admin
}
