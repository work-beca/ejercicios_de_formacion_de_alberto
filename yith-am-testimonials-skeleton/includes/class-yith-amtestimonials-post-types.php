<?php
/**
 * Class custom post type testimonials
 *
 * @package WordPress
 */

if ( ! defined( 'YITH_AMTESTIMONIALS_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AMTESTIMONIALS_Post_Types' ) ) {

	/**
	 * YITH_AMTESTIMONIALS_Post_Types
	 */
	class YITH_AMTESTIMONIALS_Post_Types {
		/**
		 * A static variable
		 *
		 * @static
		 * @var YITH_AMTESTIMONIALS_Post_Types
		 */
		private static $instance;
		/**
		 * A static variable
		 *
		 * @static
		 * @var $post_type
		 */
		public static $post_type = 'yith_testimonial';
		/**
		 * Get_instance
		 *
		 * @return YITH_AM_PRODUCT_ADDONS_Admin
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_AMTESTIMONIALS_Post_Types constructor.
		 */
		private function __construct() {

			add_action( 'init', array( $this, 'setup_post_type' ), 5 );

			// ¡Creating taxonomy no herarchical
			add_action( 'init', array( $this, 'amtestimonials_create_no_herarchical_taxonomy_positions' ) );

			// ¡Creating taxonomy herarchical
			add_action( 'init', array( $this, 'amtestimonials_create_herarchical_taxonomy_teams' ) );

		}

		/**
		 * Setup the 'Skeleton' custom post type
		 */
		public function setup_post_type() {

			$labels = array(
				'name'                  => __( 'Testimonials', 'yith-am-testimonials-skeleton' ),
				'singular_name'         => __( 'Last testimonials', 'yith-am-testimonials-skeleton' ),
				'add_new'               => __( 'New testimonial', 'yith-am-testimonials-skeleton' ),
				'edit_item'             => __( 'Edit testimonial', 'yith-am-testimonials-skeleton' ),
				'view_item'             => __( 'View testimonial', 'yith-am-testimonials-skeleton' ),
				'search_items'          => __( 'Search testimonials', 'yith-am-testimonials-skeleton' ),
				'not_found'             => __( 'Sorry, no testimonials', 'yith-am-testimonials-skeleton' ),
				'set_featured_image'    => __( 'Set featured testimonial image', 'yith-am-testimonials-skeleton' ),
				'remove_featured_image' => __( 'Remove featured testimonial image', 'yith-am-testimonials-skeleton' ),
				'use_featured_image'    => __( 'Choose your image to identify yourself', 'yith-am-testimonials-skeleton' ),
			);

			$args = array(

				'labels'           => $labels,
				'description'      => 'Create, edit and show testimonials',
				'public'           => true,
				'public_queryable' => true,
				'show_in_menu'     => false,
				'show_ui'          => true,
				'menu_icon'        => 'dashicons-testimonial',
				'capability_type'  => 'post',
				'capabilities'     => array(
					'edit_post'          => 'edit_testimonial',
					'edit_posts'         => 'edit_testimonials',
					'edit_others_posts'  => 'edit_other_testimonials',
					'publish_posts'      => 'publish_testimonials',
					'read_post'          => 'read_testimonial',
					'read_private_posts' => 'read_private_testimonials',
					'delete_post'        => 'delete_testimonial',
					'delete_posts'       => 'delete_testimonials',
				),
				'has_archive'      => true,
				'supports'         => array( 'title', 'editor', 'thumbnail' ),
			);

			register_post_type( self::$post_type, $args );
		}

		/**
		 * Amtestimonials_create_no_herarchical_taxonomy_positions
		 *
		 * @return void
		 */
		public function amtestimonials_create_no_herarchical_taxonomy_positions() {

			$tags = array(

				'name'              => __( 'Positions' ),
				'singular_name'     => __( 'Position' ),
				'search_items'      => __( 'Search positions' ),
				'all_items'         => __( 'All the positions' ),
				'parent_item'       => __( 'Position father:' ),
				'parent_item_colon' => __( 'Position father' ),
				'edit_item'         => __( 'Edit position' ),
				'update_item'       => __( 'Update position' ),
				'add_new_item'      => __( 'Add new position' ),
				'menu_name'         => __( 'Positions' ),

			);

			register_taxonomy(
				'Positions',
				array( 'yith_testimonial' ),
				array(
					'hierarchical'      => true,
					'labels'            => $tags,
					'show_ui'           => true,
					'show_admin_column' => true,
					'query_var'         => true,
					'rewrite'           => array( 'slug' => 'positions' ),
				)
			);

		}

		/**
		 * Amtestimonials_create_herarchical_taxonomy_teams
		 *
		 * @return void
		 */
		public function amtestimonials_create_herarchical_taxonomy_teams() {

			$tags = array(

				'name'              => __( 'Teams' ),
				'singular_name'     => __( 'Team' ),
				'search_items'      => __( 'Search teams' ),
				'all_items'         => __( 'All the teams' ),
				'parent_item'       => __( 'Team father:' ),
				'parent_item_colon' => __( 'Team father' ),
				'edit_item'         => __( 'Edit team' ),
				'update_item'       => __( 'Update team' ),
				'add_new_item'      => __( 'Add new team' ),
				'menu_name'         => __( 'Teams' ),

			);

			register_taxonomy(
				'Teams',
				array( 'yith_testimonial' ),
				array(
					'hierarchical'      => false,
					'labels'            => $tags,
					'show_ui'           => true,
					'show_admin_column' => true,
					'query_var'         => true,
					'rewrite'           => array( 'slug' => 'teams' ),
				)
			);

		}

	}
}
