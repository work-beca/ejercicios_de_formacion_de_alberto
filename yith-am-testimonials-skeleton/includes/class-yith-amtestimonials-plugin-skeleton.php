<?php


if ( ! defined( 'YITH_AMTESTIMONIALS_VERSION' ) ) {
	exit( 'Direct access forbidden' );
}

if ( ! class_exists( 'YITH_AMTESTIMONIALS_Plugin_Skeleton' ) ) {

	/**
	 * YITH_AMTESTIMONIALS_Plugin_Skeleton
	 */
	class YITH_AMTESTIMONIALS_Plugin_Skeleton {

		private static $instance;

		public $Admin    = null;
		public $frontend = null;

		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

		}

		/**
		 * YITH_AM_TESTIMONIALS_Plugin_Skeleton constructor.
		 */

		private function __construct() {

			add_action( 'plugins_loaded', array( $this, 'plugin_fw_loader' ), 15 );

			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );

			$require = apply_filters(
				'yith_amtestimonials_require_class',
				array(

					'common'   => array(
						'includes/class-yith-amtestimonials-post-types.php',
						'includes/class-yith-amtestimonials-shortcodes.php',
						'includes/functions.php',
					),
					'admin'    => array(
						'includes/class-yith-amtestimonials-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-amtestimonials-frontend.php',
					),
				)
			);

			$this->_require( $require );
			$this->init_classes();

			/**
			 * here set any other hoooks(actions o filters you'll use on this class)
			 */

			$this->init();
		}

		/**
		 * _require
		 *
		 * @param  mixed $main_classes .
		 * @return void
		 */
		protected function _require( $main_classes ) {

			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_AMTESTIMONIALS_DIR_PATH . $class ) ) {
						require_once YITH_AMTESTIMONIALS_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {

			YITH_AMTESTIMONIALS_Post_Types::get_instance();
			YITH_AMTESTIMONIALS_Shortcode::get_instance();
		}

		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {

			if ( is_admin() ) {
				$this->admin = YITH_AMTESTIMONIALS_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_AMTESTIMONIALS_Frontend::get_instance();
			}
		}

		/**
		 * Plugin_fw_loader
		 *
		 * @return void
		 */
		public function plugin_fw_loader() {
			if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
				global $plugin_fw_data;
				if ( ! empty( $plugin_fw_data ) ) {
					$plugin_fw_file = array_shift( $plugin_fw_data );
					require_once $plugin_fw_file;
				}
			}
		}

		/**
		 * Register plugins for activation tab
		 */
		public function register_plugin_for_activation() {
			if ( function_exists( 'YIT_Plugin_Licence' ) ) {
				YIT_Plugin_Licence()->register( YITH_AMTESTIMONIALS_INIT, YITH_AMTESTIMONIALS_SECRETKEY, YITH_AMTESTIMONIALS_SLUG );
			}
		}

		/**
		 * Register plugins for update tab
		 */
		public function register_plugin_for_updates() {
			if ( function_exists( 'YIT_Upgrade' ) ) {
				YIT_Upgrade()->register( YITH_AMTESTIMONIALS_SLUG, YITH_AMTESTIMONIALS_INIT );
			}
		}
	}

}


/**
 * Get the YITH_AMTESTIMONIALS:Plugin_Skeleton instance
 */

if ( ! function_exists( 'yith_amtestimonials_plugin_skeleton' ) ) {

	/**
	 * Yith_amtestimonials_plugin_skeleton
	 *
	 * @return instance
	 */
	function yith_amtestimonials_plugin_skeleton() {
		return YITH_AMTESTIMONIALS_Plugin_Skeleton::get_instance();
	}
}
