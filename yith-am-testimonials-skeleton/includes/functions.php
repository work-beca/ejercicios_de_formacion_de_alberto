<?php

// HERE SET FUNCTIONS TO USE ON YOUR PROJECT LIKE Template functions

/**
 * INCLUDE TEMPLATES
 */
if ( ! function_exists( 'yith_amtestimonials_get_template' ) ) {
	function yith_amtestimonials_get_template( $file_name, $args = array(), $param_shortcode ) {
		extract( $args );

		$full_path = YITH_AMTESTIMONIALS_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}


// Example

// yith_amtestimonials_get_template('/frontend/testimonials.php', array(
// 'testimonial_ids' => $testimonial_ids,
// 'show_image'      => $args['show_image'],
// 'hover_effect'    => $args['hover_effect'],
// 'posts_number'    => $args['number']
// ) );



/**
 *
 * INCLUDES VIEWS
 */

if ( ! function_exists( 'yith_amtestimonials_get_view' ) ) {
	function yith_amtestimonials_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_AMTESTIMONIALS_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}
