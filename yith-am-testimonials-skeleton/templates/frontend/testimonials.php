<?php
/**
 * File testimonials
 *
 * @package WordPress
 */

?>
<div class="content-testimonials">
	<?php
	foreach ( $args as $post_ ) {

		$vip_post_class = 1 === ( get_post_meta( $post_->ID, 'yith_amet_meta_checkbox_vip', true ) ) ? 'vip_on' : 'vip_off';

		if ( ( ! empty( $param_shortcode['hover_effect'] ) && 'yes' === $param_shortcode['hover_effect'] ) || 'zoom' === get_option( 'yit_yith-am-testimonials-skeleton_options' )['yith-amet-select-hover-effect'] ) {
			?>
			<div class = "testimonials-card hover-effect-testimonials <?php echo esc_html( $vip_post_class ); ?> " >    
		<?php } else { ?>
			<div class = "testimonials-card <?php echo esc_html( $vip_post_class ); ?>  " >
			<?php
		}

		if ( '1' === get_post_meta( $post_->ID, 'yith_amet_meta_checkbox_badge', true ) ) {
			?>
			<div  class = "title-badge-testimonials"><p style="background-color: <?php echo esc_html( get_post_meta( $post_->ID, 'yith_amet_meta_colorpicket_badge', true ) ); ?>" class="title_badge title_background"> <?php echo esc_html( get_post_meta( $post_->ID, 'yith_amet_meta_text_title_badge', true ) ); ?></p></div>
			<style>
					.title_badge{				
						color: white;
						height: 80%;
						border-radius: 4px;
						margin-top: 5px;
						font-size: 15px;
					}
			</style>

		<?php } ?>
		<div class= "group-testimonials">
			<?php if ( ( ! empty( $param_shortcode['img'] ) && 'yes' === $param_shortcode['img'] ) || 'yes' === get_option( 'yit_yith-am-testimonials-skeleton_options' )['yith-amet-checkbox-image'] ) { ?>
				<div id="img_testimonials" class= "img-testimonials"> <?php echo get_the_post_thumbnail( $post_->ID ); ?> </div>             
			<?php } ?>

			<div class = "title-testimonials"> <?php echo esc_html( $post_->post_title ); ?></div>
		</div>

		<div class = "company-testimonials"> <?php __( 'Player of' ); ?> <a target="_blank" class="link-company" href="<?php echo esc_html( get_post_meta( $post_->ID, 'yith_amet_meta_text_company_url', true ) ); ?>"><?php echo esc_html( get_post_meta( $post_->ID, 'yith_amet_meta_text_company', true ) ); ?></a></div>
		<div class = "email-testimonials"><?php echo esc_html( get_post_meta( $post_->ID, 'yith_amet_meta_text_player_email', true ) ); ?></div>

		<?php $star_testimonials = get_post_meta( $post_->ID, 'yith_amet_meta_radio_stars', true ); ?>

		<p class="clasificacion">
			<?php
			$value = 5;
			for ( $i = 0; $i < $value; $i++ ) {
				$checked = $i <= ( intval( $star_testimonials ) - 1 ) ? 'checked' : 'unchecked';
				?>
				<input <?php echo esc_html( $checked ); ?> type="radio" name="estrellas" value="<?php echo esc_html( $i ); ?>" disabled>
				<label for="radio<?php $i; ?>" class=" <?php echo esc_html( $checked ); ?>">★</label>               
			<?php } ?>
		</p> 

		<?php
			$term_list_teams     = get_the_term_list( $post_->ID, 'Teams', '', '|', ' ' );
			$term_list_positions = get_the_term_list( $post_->ID, 'Positions', '', '|', '' );

		if ( $term_list_teams || $term_list_positions ) {
			?>
				<div class="taxonomy-teams">   
					<?php
					echo $term_list_teams;
					echo $term_list_positions;
					?>
				</div>
		<?php } ?>

		<div class="testimonial-text"> <?php echo esc_html( $post_->post_content ); ?> </div>

		</div> <!-- End div class testimonials-card -->          
	<?php } // end foreach ?>

</div> <!-- End div class content-testimonials
